import loader from '../src/loader.js' // unused, but checks for parse errors
import compiler from './compiler.js'

function sourceJson(source) {
  return JSON.parse(source.replace(/^module.exports = /, ''))
}

test('loads the example .ods file', async () => {
  const stats = (await compiler('example.ods')).toJson()
  // console.log(stats)
  expect(stats.errors).toEqual([])
  const json = sourceJson(stats.modules[0].source)
  // console.log(json)
  expect(json.Affixes).not.toBeNull()
  expect(json.Affixes.map(row => row.affixId)).toEqual(expect.arrayContaining(['life:flat01']))
})
test('loads the raw example .ods file', async () => {
  const stats = (await compiler('example.ods', {format: 'raw'})).toJson()
  // console.log(stats)
  expect(stats.errors).toEqual([])
  const json = sourceJson(stats.modules[0].source)
  // console.log(json)
  expect(json.Affixes).toBeUndefined()
  expect(json.Sheets.Affixes).toBeTruthy()
  expect(json.SheetNames).toEqual(expect.arrayContaining(['Weapons','Armors','Affixes']))
})
