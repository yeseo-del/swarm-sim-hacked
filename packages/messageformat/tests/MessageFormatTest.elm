module MessageFormatTest exposing (all)

import Dict exposing (Dict)
import Expect
import MessageFormat
import Test exposing (..)



-- testing by hand: https://format-message.github.io/icu-message-format-for-translators/editor.html


all : Test
all =
    describe "MessageFormat"
        [ test "parse-error-{}" <| \_ -> "hello {name" |> MessageFormat.parse |> Expect.err
        , test "parse-error-''" <| \_ -> "hello 'name" |> MessageFormat.parse |> Expect.err
        , test "parse-error outer-#" <| \_ -> MessageFormat.parse "hello #" |> Expect.err
        , test "format-empty" <| \_ -> MessageFormat.formats "" Dict.empty |> Expect.equal (Ok "")
        , test "format-simple" <| \_ -> MessageFormat.formats "hello world" Dict.empty |> Expect.equal (Ok "hello world")
        , test "format-quote" <| \_ -> MessageFormat.formats "hello 'world'" Dict.empty |> Expect.equal (Ok "hello world")
        , test "format-multiquote" <| \_ -> MessageFormat.formats "hello 'world{}'''" Dict.empty |> Expect.equal (Ok "hello world{}'")

        -- messageText can contain quoted literal strings including syntax characters.
        -- A quoted literal string begins with an ASCII apostrophe and a syntax
        -- character (usually a {curly brace}) and continues until the next single
        -- apostrophe. A double ASCII apostrohpe inside or outside of a quoted
        -- string represents one literal apostrophe.
        -- http://icu-project.org/apiref/icu4j/com/ibm/icu/text/MessageFormat.html
        --
        -- For example, pattern string "'{''}'" is interpreted as [...] "{'}", not "{}".
        -- https://docs.oracle.com/javase/7/docs/api/java/text/MessageFormat.html
        --
        -- "A pair of adjacent apostrophes always results in a single apostrophe
        -- in the output, even when the pair is between two single, text-quoting apostrophes":
        -- http://icu-project.org/apiref/icu4j/com/ibm/icu/text/MessagePattern.ApostropheMode.html
        , test "fancy quoting" <| \_ -> MessageFormat.formats "a '{' b '}' c '|' d '#' e '''' f '' g '{''}' h \" i" Dict.empty |> Expect.equal (Ok "a { b } c | d # e ' f ' g {'} h \" i")
        , test "simple subst" <| \_ -> MessageFormat.formats "{name}" (Dict.singleton "name" "world") |> Expect.equal (Ok "world")
        , test "simple subst with spaces" <| \_ -> MessageFormat.formats "{ name }" (Dict.singleton "name" "world") |> Expect.equal (Ok "world")
        , test "plural with no-subst other" <| \_ -> MessageFormat.formats "{n, plural, other {hello}}" (Dict.singleton "n" "3") |> Expect.equal (Ok "hello")
        , test "plural with subst other" <| \_ -> MessageFormat.formats "{n, plural, other {hello {n}}}" (Dict.singleton "n" "3") |> Expect.equal (Ok "hello 3")
        , test "select with no-subst other" <| \_ -> MessageFormat.formats "{gender, select, other {other?}}" (Dict.singleton "gender" "nonbinary") |> Expect.equal (Ok "other?")
        , test "select with subst other" <| \_ -> MessageFormat.formats "{gender, select, other {other: {gender}}}" (Dict.singleton "gender" "nonbinary") |> Expect.equal (Ok "other: nonbinary")
        , test "select with non-other " <| \_ -> MessageFormat.formats "{gender, select, m {male!} f {female!} other {other: {gender}}}" (Dict.singleton "gender" "f") |> Expect.equal (Ok "female!")
        , let
            message =
                "hello {name}!"
          in
          describe "subst"
            [ test "parse" <| \_ -> MessageFormat.parse message |> Expect.ok
            , test "error" <| \_ -> Dict.fromList [] |> MessageFormat.formats message |> Expect.err
            , test "success" <| \_ -> Dict.fromList [ ( "name", "world" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world!")
            ]
        , let
            message =
                """hello {name}! There {n, plural,
                    =0 {are no lights}
                    one {is one light}
                    =4 {are four lights}
                    other {are # lights}}."""
          in
          describe "plural"
            [ test "parse" <| \_ -> MessageFormat.parse message |> Expect.ok
            , test "error" <| \_ -> Dict.fromList [] |> MessageFormat.formats message |> Expect.err
            , test "zero" <| \_ -> Dict.fromList [ ( "name", "world" ), ( "n", "0" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world! There are no lights.")
            , test "one" <| \_ -> Dict.fromList [ ( "name", "world" ), ( "n", "1" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world! There is one light.")
            , test "two" <| \_ -> Dict.fromList [ ( "name", "world" ), ( "n", "2" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world! There are 2 lights.")
            , test "four" <| \_ -> Dict.fromList [ ( "name", "world" ), ( "n", "4" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world! There are four lights.")
            , test "nan" <| \_ -> Dict.fromList [ ( "name", "world" ), ( "n", "NaN" ) ] |> MessageFormat.formats message |> Expect.equal (Ok "hello world! There are NaN lights.")
            ]
        , describe "codegen"
            [ test "simple/one" <|
                \_ ->
                    [ ( "greeting", "hello \"codegen\" wor\\d" ) ]
                        |> MessageFormat.codegens "CodegenTest"
                        |> Expect.equal (Ok """module CodegenTest exposing (greeting)

greeting : String
greeting = "hello \\"codegen\\" wor\\\\d"
""")
            , test "var" <|
                \_ ->
                    [ ( "greeting", "hello \"codegen\" wor\\d" )
                    , ( "greetName", "hello {name}!" )
                    ]
                        |> MessageFormat.codegens "CodegenTest"
                        |> Expect.equal (Ok """module CodegenTest exposing (greeting, greetName)

greeting : String
greeting = "hello \\"codegen\\" wor\\\\d"

greetName : { a | name : String } -> String
greetName args = "hello " ++ args.name ++ "!"
""")
            , test "plural" <|
                \_ ->
                    [ ( "greeting", "hello \"codegen\" wor\\d" )

                    -- , ( "lights", "There {n, plural, zero {are no lights} one {is one light} =4 {are four lights} other {are # lights}}!" )
                    , ( "lights", "There {n, plural, zero {are no lights} one {is one light} =4 {are four lights} other {are {n} lights}}!" )
                    ]
                        |> MessageFormat.codegens "CodegenTest"
                        |> Expect.equal (Ok """module CodegenTest exposing (greeting, lights)

greeting : String
greeting = "hello \\"codegen\\" wor\\\\d"

lights : { a | n : String } -> String
lights args = "There " ++ (case args.n of
    "0" -> "are no lights"
    "1" -> "is one light"
    "4" -> "are four lights"
    _ -> "are " ++ args.n ++ " lights"
) ++ "!"
""")
            ]
        ]
