module Game.OrderTest exposing (all)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Expect
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Unit as Unit exposing (Unit)
import GameDataFixture exposing (gameData)
import Set exposing (Set)
import Test exposing (..)
import TestUtil exposing (decimals)
import Time exposing (Posix)


model0 =
    Game.empty gameData (Time.millisToPosix 1)


allVisible =
    Set.fromList <| List.map Item.name <| GameData.items gameData


all : Test
all =
    describe "UnitType tests"
        [ describe "FactorCost"
            (let
                t : String -> Result Decimal Int -> Dict String Decimal -> Expect.Expectation
                t unitName result bank =
                    let
                        game =
                            model0 |> Game.updateUnits (UnitsSnapshot.create (Game.snapshotted model0) bank Dict.empty allVisible)
                    in
                    Order.fromPercent game (Game.units game) (Item.Unit <| unitType unitName) 1
                        |> Result.map .count
                        |> Expect.equal (Ok result)

                c : String -> Dict String Decimal -> Dict String Decimal -> Expect.Expectation
                c unitName result bank =
                    let
                        game =
                            model0 |> Game.updateUnits (UnitsSnapshot.create (Game.snapshotted model0) bank Dict.empty allVisible)
                    in
                    Order.fromPercent game (Game.units game) (Item.Unit <| unitType unitName) 1
                        |> Result.map (.cost >> List.map (Tuple.mapFirst Unit.idToString) >> Dict.fromList)
                        |> Expect.equal (Ok result)
             in
             [ test "first hatchery" <| \_ -> t "hatchery" (Ok 1) (decimals "meat:30")

             -- wow, fuck decimal precision
             -- , test "first hatchery: cost" <| \_ -> c "hatchery" (decimals "meat:30") (decimals "meat:30")
             , test "first hatchery: cost" <| \_ -> c "hatchery" (decimals "meat:30.000000000000004") (decimals "meat:30")
             , test "hatchery fail" <| \_ -> t "hatchery" (Ok 0) (decimals "meat:29")
             , test "hatchery fail: cost" <| \_ -> c "hatchery" (decimals "meat:0") (decimals "meat:29")
             , test "2nd hatchery" <| \_ -> t "hatchery" (Ok 1) (decimals "hatchery: 1, meat:330")

             -- , test "2nd hatchery: cost" <| \_ -> c "hatchery" (decimals "meat:300") (decimals "hatchery: 1, meat:330")
             , test "2nd hatchery: cost" <| \_ -> c "hatchery" (decimals "meat:300.00000000000004") (decimals "hatchery: 1, meat:330")
             , test "2nd hatchery fail" <| \_ -> t "hatchery" (Ok 0) (decimals "hatchery: 1, meat:299")
             , test "2 hatcheries" <| \_ -> t "hatchery" (Ok 2) (decimals "meat:3301")

             -- On paper, two are buyable here, but decimal imprecision breaks it.
             -- As long as the costs are close/it doesn't try to pay for both, it's okay -
             -- not *great*, but worst case is that the user has to click twice. No lost resources.
             , test "3rd hatchery precision error" <| \_ -> t "hatchery" (Ok 1) (decimals "hatchery: 2, meat: 3300")
             , test "3rd hatchery precision error: cost" <| \_ -> c "hatchery" (decimals "meat:3000.0000000000004") (decimals "hatchery: 2, meat: 3300")
             , test "3rd hatchery" <| \_ -> t "hatchery" (Ok 1) (decimals "hatchery: 2, meat: 3333")
             , test "3 hatcheries" <| \_ -> t "hatchery" (Ok 3) (decimals "meat: 3333")
             , test "Decimal-sized hatcheries" <| \_ -> t "hatchery" (Ok 998) (decimals "meat: 3e999")
             , test "Decimal-sized hatcheries: cost" <| \_ -> c "hatchery" (decimals "meat: 3.333333333333333e998") (decimals "meat: 3e999")
             , test "Decimal-sized hatcheries 2" <| \_ -> t "hatchery" (Ok 498) (decimals "hatchery: 500, meat: 3e999")
             , test "Decimal-sized hatcheries 2: cost" <| \_ -> c "hatchery" (decimals "meat: 3.333333333333333e998") (decimals "hatchery: 500, meat: 3e999")
             ]
            )
        ]


createBank : List ( Unit.Id, String ) -> Dict String Decimal
createBank =
    List.map (Tuple.mapBoth Unit.idToString (Decimal.fromString >> TestUtil.unsafeJust "createBank couldn't parse a decimal")) >> Dict.fromList


unitType : String -> Unit
unitType name =
    gameData |> GameData.unitByName name |> TestUtil.unsafeJust ("no such unit: " ++ name)


roundToString : Result Decimal Int -> String
roundToString r =
    case r of
        Err d ->
            Decimal.toString d

        Ok n ->
            String.fromInt n
