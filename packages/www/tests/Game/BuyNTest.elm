module Game.BuyNTest exposing (all)

import Decimal exposing (Decimal)
import Expect
import Game.BuyN as BuyN
import Test exposing (..)


all : Test
all =
    describe "BuyN parsing"
        [ test "val" <|
            \_ -> BuyN.parse "123" |> Expect.equal (Ok <| BuyN.Value <| Decimal.fromInt 123)
        , test "negative" <|
            \_ -> BuyN.parse "-123" |> Expect.err
        , test "val-e" <|
            \_ -> BuyN.parse "1.23e2" |> Expect.equal (Ok <| BuyN.Value <| Decimal.fromInt 123)
        , test "percent" <|
            \_ -> BuyN.parse "50%" |> Expect.equal (Ok <| BuyN.Percent 50)
        , test "percent > 100" <|
            \_ -> BuyN.parse "200%" |> Expect.equal (Ok <| BuyN.Percent 200)
        , test "percent < 0" <|
            \_ -> BuyN.parse "-50%" |> Expect.err
        , test "target" <|
            \_ -> BuyN.parse "@123" |> Expect.equal (Ok <| BuyN.Target <| Decimal.fromInt 123)
        , test "twinval" <|
            \_ -> BuyN.parse "=123" |> Expect.equal (Ok <| BuyN.TwinValue <| Decimal.fromInt 123)
        , test "decimal: XeZ" <|
            \_ -> BuyN.parse "1e400" |> Expect.equal (Ok <| BuyN.Value <| Decimal.fromSigExp 1.0 400)
        , test "decimal: X.YeZ" <|
            \_ -> BuyN.parse "1.1e400" |> Expect.equal (Ok <| BuyN.Value <| Decimal.fromSigExp 1.1 400)
        ]
