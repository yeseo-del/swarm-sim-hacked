module RomanizeTest exposing (all)

import Expect
import Romanize exposing (romanize)
import Test exposing (..)


data : List ( Int, String )
data =
    ([ "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX" ]
        |> List.indexedMap Tuple.pair
    )
        ++ [ ( 28, "XXVIII" )
           , ( 29, "XXIX" )
           , ( 39, "XXXIX" )
           , ( 40, "XL" )
           , ( 44, "XLIV" )
           , ( 48, "XLVIII" )
           , ( 49, "XLIX" )
           , ( 50, "L" )
           ]


all : Test
all =
    describe "romanize" <|
        List.map
            (\( i, r ) ->
                test ("romanize " ++ String.fromInt i ++ " -> " ++ r) <|
                    \_ ->
                        romanize i
                            |> Expect.equal r
            )
            data
