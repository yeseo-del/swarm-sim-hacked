module RouteTest exposing (all)

import Expect
import Route exposing (Route)
import Test exposing (..)
import Url exposing (Url)


all : Test
all =
    describe "Routing/url tests"
        [ test "parse simplest case" <|
            \_ ->
                { url0 | path = "/tab/meat/unit/drone" }
                    |> Route.fromUrl
                    |> Expect.equal (Just <| Route.unit "meat" "drone")
        , test "parse lowercase" <|
            \_ ->
                { url0 | path = "/tab/MEAT/unit/DrOnE" }
                    |> Route.fromUrl
                    |> Expect.equal (Just <| Route.unit "meat" "drone")
        , test "parse%20spaces" <|
            \_ ->
                { url0 | path = "/tab/MEAT/unit/GrEaTer%20QuEeN" }
                    |> Route.fromUrl
                    |> Expect.equal (Just <| Route.unit "meat" "greater queen")
        ]


url0 : Url
url0 =
    case Url.fromString "https://example.com" of
        Nothing ->
            Debug.todo "hardcoded url parse won't fail"

        Just val ->
            val
