module TestUtil exposing (buy, buyOk, decimal, decimals, getVar, isVisible, progressDuration, setBuyN, spellId, tick, tick0, tickPlus, unitCount, unitId, units, unsafeJust, unsafeOk, unsafeRemote, updateUnits, upgradeCount, upgradeId)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.BuyN as BuyN
import Game.BuyOrder
import Game.Order as Order exposing (Order)
import Game.Order.Progress as Progress exposing (OrderProgress, Progress)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import GameDataFixture exposing (gameData)
import RemoteData exposing (RemoteData)
import Result.Extra
import Set exposing (Set)
import Time


unsafeJust err m =
    case m of
        Just a ->
            a

        Nothing ->
            Debug.todo <| "unsafeMaybe broke: " ++ err


unsafeOk : Result err ok -> ok
unsafeOk result =
    case result of
        Ok ok ->
            ok

        Err err ->
            Debug.todo <| "unsafeOk crashed: " ++ Debug.toString err


unsafeRemote : RemoteData e a -> a
unsafeRemote remote =
    case remote of
        RemoteData.Success a ->
            a

        _ ->
            Debug.todo <| "unsafeRemote broke: " ++ Debug.toString remote


decimal : String -> Decimal
decimal str =
    str
        |> String.trim
        |> Decimal.fromString
        |> unsafeJust ("invalid decimal-string: " ++ str)


decimals : String -> Dict String Decimal
decimals strs =
    let
        res =
            strs
                |> String.split ","
                |> List.map
                    (\str ->
                        case str |> String.split ":" of
                            [ key, val ] ->
                                val
                                    |> String.trim
                                    |> Decimal.fromString
                                    |> Maybe.map (Tuple.pair (String.trim key))
                                    |> Result.fromMaybe "invalid key/val"

                            _ ->
                                Err <| "invalid entry"
                    )
                |> Result.Extra.combine
                |> Result.map Dict.fromList
    in
    case res of
        Err _ ->
            Debug.todo "invalid decimal-dict" res

        Ok val ->
            val


getVar : Dict String a -> String -> Maybe a
getVar dict =
    \name -> Dict.get name dict


getItem : String -> Game -> Item
getItem name game =
    game |> Game.gameData |> GameData.itemByName name |> unsafeJust ("no such item: " ++ name)


order : Game -> String -> String -> Result String Order
order game name input =
    let
        item =
            getItem name game
    in
    Order.fromBuyN (game |> Game.setBuyN item input) (Game.units game) item


setBuyN : String -> String -> Game -> Game
setBuyN name input game =
    Game.setBuyN (getItem name game) input game


progressDuration : String -> Game -> List ( String, Maybe Duration )
progressDuration name game =
    let
        item =
            getItem name game

        o =
            Order.fromBuyN game (Game.units game) item
                |> unsafeOk

        op : OrderProgress
        op =
            Progress.updates game (Game.units game) [ o ] Dict.empty
                |> Dict.values
                |> List.head
                |> unsafeJust "progressDuration didn't return anything"
    in
    op
        |> List.map
            (\p ->
                ( p.currency |> Unit.idToString
                , p.status |> Progress.duration |> Maybe.map Progress.remaining
                )
            )


buy : String -> String -> Game -> Result String Game
buy name input game =
    order game name input |> Result.andThen (\o -> Game.BuyOrder.buy o game)


buyOk : String -> String -> Game -> Game
buyOk name input game0 =
    case buy name input game0 of
        Ok game ->
            game

        Err err ->
            Debug.todo <| "buyOk failed: " ++ err


tick : Int -> Game -> Game
tick ms =
    Game.snapshotUnits (Time.millisToPosix ms)


tickPlus : Int -> Game -> Game
tickPlus ms game =
    tick (ms + Time.posixToMillis (Game.snapshotted game)) game


tick0 : Game -> Game
tick0 =
    tickPlus 0


{-| Creates a snapshot with all items visible
-}
units : Int -> String -> UnitsSnapshot
units now str =
    let
        decs =
            decimals str
    in
    UnitsSnapshot.create (Time.millisToPosix now) decs Dict.empty (gameData |> GameData.items |> List.map Item.name |> Set.fromList)


updateUnits : String -> Game -> Game
updateUnits str game =
    game
        |> Game.updateUnits (UnitsSnapshot.create (Game.snapshotted game) (decimals str) Dict.empty Set.empty)
        |> Game.refreshSnapshotUnits


unitCount : String -> Game -> Decimal
unitCount name game =
    let
        unit =
            GameData.unitByName name gameData |> unsafeJust ("no such unit: " ++ name)
    in
    Game.unitCount unit.id game


upgradeCount : String -> Game -> Decimal
upgradeCount name game =
    let
        upgrade =
            GameData.upgradeByName name gameData |> unsafeJust "no such upgrade"
    in
    Game.upgradeCount upgrade.id game


isVisible : String -> UnitsSnapshot -> Bool
isVisible name =
    let
        item =
            GameData.itemByName name gameData |> unsafeJust "no such item"
    in
    UnitsSnapshot.isItemVisible (Item.id item)


unitId : String -> Unit.Id
unitId name =
    GameData.unitByName name gameData |> Maybe.map .id |> unsafeJust "no such unit"


upgradeId : String -> Upgrade.Id
upgradeId name =
    GameData.upgradeByName name gameData |> Maybe.map .id |> unsafeJust "no such upgrade"


spellId : String -> Spell.Id
spellId name =
    GameData.spellByName name gameData |> Maybe.map .id |> unsafeJust "no such spell"
