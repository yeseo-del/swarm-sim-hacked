module Parser.TutorialTest exposing (all)

import Dict
import Expect
import Fools exposing (Fools)
import Game.Tutorial
import GameDataFixture exposing (gameData)
import Locale exposing (Locale(..))
import Parser.Tutorial
import Test exposing (..)
import View.Lang as T


lang =
    T.localeToLang gameData Fools.Off Nothing Nothing Locale.En_US


{-| an empty save from old-swarmsim; same one as the empty example in persist.test.js
-}
all : Test
all =
    describe "Tutorial parser gamedata tests" <|
        List.map
            (\step ->
                test ("parse: " ++ Debug.toString step) <|
                    \_ ->
                        lang.rhtml (T.ViewTutorial Dict.empty step)
                            |> Expect.ok
            )
            Game.Tutorial.steps
