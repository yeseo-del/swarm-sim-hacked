module Parser.VisibilityTest exposing (all)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Expect
import Parser
import Parser.Visibility
import Set exposing (Set)
import Test exposing (..)
import TestUtil


{-| an empty save from old-swarmsim; same one as the empty example in persist.test.js
-}
all : Test
all =
    describe "Visibility tests"
        [ test "run a simple rule" <|
            \_ ->
                Parser.Visibility.parse "a > 3"
                    |> Expect.all
                        [ Expect.ok
                        , eval "" (TestUtil.decimals "a: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 2") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 3") >> Expect.equal (Ok False)
                        , eval "" Dict.empty >> Expect.equal (Err "no such var: a")
                        , eval "0" Dict.empty >> Expect.equal (Ok False)
                        , eval "999" Dict.empty >> Expect.equal (Ok True)
                        ]
        , test "run a simple rule with spaces" <|
            \_ ->
                Parser.Visibility.parse "     a > 3  "
                    |> Expect.all
                        [ Expect.ok
                        , eval "" (TestUtil.decimals "a: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 2") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 3") >> Expect.equal (Ok False)
                        , eval "" Dict.empty >> Expect.equal (Err "no such var: a")
                        , eval "0" Dict.empty >> Expect.equal (Ok False)
                        , eval "999" Dict.empty >> Expect.equal (Ok True)
                        ]
        , test "run && rule" <|
            \_ ->
                Parser.Visibility.parse "(a > 3 && b >= 4)"
                    |> Expect.all
                        [ Expect.ok
                        , eval "" (TestUtil.decimals "a: 4") >> Expect.equal (Err "no such var: b")
                        , eval "" (TestUtil.decimals "b: 4") >> Expect.equal (Err "no such var: a")
                        , eval "0" (TestUtil.decimals "a: 4") >> Expect.equal (Ok False)
                        , eval "0" (TestUtil.decimals "b: 4") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 4, b: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 3, b: 4") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 4, b: 3") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 3, b: 3") >> Expect.equal (Ok False)
                        ]
        , test "run || rule" <|
            \_ ->
                Parser.Visibility.parse "a > 3 || b >= 4"
                    |> Expect.all
                        [ Expect.ok
                        , eval "" (TestUtil.decimals "a: 4") >> Expect.equal (Err "no such var: b")
                        , eval "" (TestUtil.decimals "b: 4") >> Expect.equal (Err "no such var: a")
                        , eval "0" (TestUtil.decimals "a: 4") >> Expect.equal (Ok True)
                        , eval "0" (TestUtil.decimals "b: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 4, b: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 3, b: 4") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 4, b: 3") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 3, b: 3") >> Expect.equal (Ok False)
                        ]
        , test "no mixing ||, && without parens for explicit precedence" <|
            \_ -> Parser.Visibility.parse "a > 3 || b >= 4 && c == 5" |> Expect.err
        , test "nested crap is okay" <|
            \_ -> Parser.Visibility.parse "a > 3 || b >= 4 || ((((((c == 5))))) && (d <= 6 || e < 7 || TRUE || FALSE))" |> Expect.ok
        , describe "visible cannot become invisible"
            (case Parser.Visibility.parse "a > 3" of
                Err err ->
                    Debug.todo "visibility.parse failed?" err

                Ok expr ->
                    let
                        rules =
                            [ ( "xyz", expr ) ]

                        visibleVars name =
                            Dict.singleton "a" Decimal.infinity |> Dict.get name

                        invisibleVars name =
                            Dict.singleton "a" Decimal.zero |> Dict.get name
                    in
                    [ test "set visible when expected" <|
                        \_ ->
                            Set.empty
                                |> Parser.Visibility.evalSetWithDefault Decimal.zero visibleVars rules
                                |> Expect.equal (Set.singleton "xyz")
                    , test "set invisible when expected" <|
                        \_ ->
                            Set.empty
                                |> Parser.Visibility.evalSetWithDefault Decimal.zero invisibleVars rules
                                |> Expect.equal Set.empty
                    , test "set visible cannot become invisible" <|
                        \_ ->
                            Set.empty
                                |> Parser.Visibility.evalSetWithDefault Decimal.zero visibleVars rules
                                |> Parser.Visibility.evalSetWithDefault Decimal.zero invisibleVars rules
                                |> Expect.equal (Set.singleton "xyz")
                    ]
            )
        , test "addition support" <|
            \_ ->
                Parser.Visibility.parse "(a + b + c) > 5"
                    |> Expect.all
                        [ Expect.ok
                        , eval "0" (TestUtil.decimals "a: 1, b: 2, c: 3") >> Expect.equal (Ok True)
                        , eval "" (TestUtil.decimals "a: 1, b: 2, c: 3") >> Expect.equal (Ok True)
                        , eval "0" (TestUtil.decimals "a: 1, b: 2, c: 0") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 1, b: 2, c: 0") >> Expect.equal (Ok False)
                        , eval "0" (TestUtil.decimals "a: 1, b: 2") >> Expect.equal (Ok False)
                        , eval "" (TestUtil.decimals "a: 1, b: 2") >> Expect.equal (Err "no such var: c")
                        ]
        , test "progress bars" <|
            \_ ->
                Parser.Visibility.parse "(a + b) > 3 && a >= 3 && b > 3"
                    |> Expect.all
                        [ Expect.ok
                        , progress "a:3, b:2" >> Expect.equal [ { name = "a", percent = 1, val = Decimal.fromFloat 3, max = Decimal.fromFloat 3 } ]
                        , progress "a:4, b:2" >> Expect.equal [ { name = "a", percent = 1, val = Decimal.fromFloat 4, max = Decimal.fromFloat 3 } ]
                        , progress "a:2, b:2" >> Expect.equal [ { name = "a", percent = 2 / 3, val = Decimal.fromFloat 2, max = Decimal.fromFloat 3 } ]
                        , progress "b:2" >> Expect.equal [ { name = "a", percent = 0, val = Decimal.zero, max = Decimal.fromFloat 3 } ]
                        ]
        ]


progress : String -> Result (List Parser.DeadEnd) Parser.Visibility.Expr -> List Parser.Visibility.Progress
progress vars_ parsed =
    case parsed of
        Err err ->
            Debug.todo "Parser.Visibility parse fail" parsed

        Ok expr ->
            Parser.Visibility.progress (vars_ |> TestUtil.decimals |> vars) expr


eval : String -> Dict String Decimal -> Result (List Parser.DeadEnd) Parser.Visibility.Expr -> Result String Bool
eval default vars_ =
    -- when this was written, `eval` and `evalWithDefault` were the same function, with a more complex signature:
    -- `Maybe Decimal -> Dict String Decimal -> Expr -> Result String Bool`
    --
    -- It'd be nice to rewrite these tests to independently test each of the new simplified functions,
    -- but I'm lazy and only updated this one.
    Result.mapError Parser.deadEndsToString
        >> (case Decimal.fromString default of
                Nothing ->
                    Result.andThen (Parser.Visibility.eval (vars vars_))

                Just d ->
                    Result.map (Parser.Visibility.evalWithDefault d (vars vars_))
           )


vars : Dict String Decimal -> String -> Maybe Decimal
vars dict name =
    Dict.get name dict
