module GameTest exposing (all)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Expect
import Game exposing (Game)
import Game.Achievement
import Game.UnitsSnapshot as UnitsSnapshot
import GameData exposing (GameData)
import GameData.Achievement as Achievement exposing (Achievement)
import GameDataFixture exposing (gameData)
import Iso8601
import Json.Decode as D
import Json.Encode as E
import LegacySaveData
import RemoteData exposing (RemoteData)
import Set exposing (Set)
import Test exposing (..)
import TestUtil
import TestUtil.Expect
import Time exposing (Posix)


all : Test
all =
    describe "Game"
        [ describe "deserialize legacy saves"
            (LegacySaveData.list
                |> List.map
                    (\( name, json ) ->
                        test name <|
                            \_ ->
                                Just json
                                    |> Game.load gameData now
                                    |> Expect.all
                                        [ Expect.ok

                                        -- units all decoded correctly
                                        , TestUtil.unsafeOk
                                            >> Game.units
                                            >> UnitsSnapshot.toDict
                                            >> Dict.filter (\k -> Decimal.isFinite >> not)
                                            >> Expect.equalDicts Dict.empty

                                        -- especially hatcheries, since they have their own special import process
                                        , TestUtil.unsafeOk
                                            >> TestUtil.unitCount "hatchery"
                                            >> Expect.notEqual Decimal.zero
                                        , TestUtil.unsafeOk
                                            >> Game.units
                                            >> UnitsSnapshot.velocity (TestUtil.unitId "larva")
                                            >> Expect.notEqual Decimal.zero

                                        -- achievement-dates all decoded correctly, as reported here:
                                        -- https://www.reddit.com/r/swarmsim/comments/aydjol/swarm_simulator_original_v120beta/eidf2qp/
                                        -- old-swarmsim encoded them as a duration after game creation, while this version is just a posix date.
                                        , TestUtil.unsafeOk
                                            >> Game.achievements
                                            >> List.filterMap (\( a, s ) -> Game.Achievement.earnedAt s |> Maybe.map (Tuple.pair a))
                                            >> List.filter (\( a, d ) -> Time.toYear Time.utc d < 2000)
                                            >> List.map (Tuple.mapBoth Achievement.name Iso8601.fromTime)
                                            >> Expect.equalLists []
                                        ]
                    )
            )
        , describe "serialization"
            [ test "loads empty save; new game" <|
                \_ ->
                    Nothing
                        |> Game.load gameData now
                        |> Expect.ok
            , test "loads nonempty broken save" <|
                \_ ->
                    Just brokenExample
                        |> Game.load gameData now
                        |> Expect.err
            , test "encode >> decode is ok" <|
                \_ ->
                    Game.empty gameData now
                        |> Game.encoder now
                        |> D.decodeValue (Game.decoder gameData now)
                        |> Expect.ok
            , describe "unit production"
                [ test "simple" <|
                    \_ ->
                        Game.empty gameData now
                            |> Game.updateUnits (UnitsSnapshot.create now (TestUtil.decimals "queen: 1, hatchery: 1") Dict.empty Set.empty)
                            |> Expect.all
                                [ Game.units
                                    >> UnitsSnapshot.toDict
                                    -- these zeros are filled in during velocity recalculations. Whatever, same difference
                                    -- >> Expect.equal (TestUtil.decimals "queen: 1, hatchery: 1")
                                    >> Expect.equal (TestUtil.decimals "queen: 1, drone: 0, meat: 0, hatchery: 1, larva: 0")
                                , Game.evalUnits (Time.millisToPosix 0)
                                    >> UnitsSnapshot.toDict
                                    >> Expect.equal (TestUtil.decimals "queen: 1, drone: 0, meat: 0, hatchery: 1, larva: 0")
                                , Game.evalUnits (Time.millisToPosix <| 10 * 1000)
                                    >> UnitsSnapshot.toDict
                                    >> Expect.equal (TestUtil.decimals "queen: 1, drone: 20, meat: 100, hatchery: 1, larva: 10")
                                ]
                ]
            , describe "update"
                (let
                    game0 : Game
                    game0 =
                        Game.empty gameData now
                 in
                 [ describe "don't populate unused units"
                    -- these tests are a bit awkard; normally we'd use Game.count-style functions.
                    -- here, though, I'm interested in what gets persisted.
                    [ test "init doesn't populate unused units" <|
                        \_ ->
                            game0
                                |> Game.units
                                |> UnitsSnapshot.toDict
                                |> Dict.get "overmind6"
                                |> Expect.equal Nothing
                    , test "tick doesn't populate unused units" <|
                        \_ ->
                            game0
                                |> TestUtil.tick 0
                                |> Game.units
                                |> UnitsSnapshot.toDict
                                |> Dict.get "overmind6"
                                |> Expect.equal Nothing
                    , test "snapshot doesn't populate unused units" <|
                        \_ ->
                            game0
                                |> TestUtil.buyOk "drone" "1"
                                |> Expect.all
                                    [ Game.units >> UnitsSnapshot.toDict >> Dict.get "overmind6" >> Expect.equal Nothing
                                    , Game.units >> UnitsSnapshot.toDict >> Dict.get "drone" >> Expect.equal (Just Decimal.one)
                                    ]
                    ]
                 , test "tick" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "drone: 10"
                            |> Expect.all
                                [ TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "10"
                                , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.tick 500
                                    >> Expect.all
                                        [ TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "10"
                                        , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "5"
                                        ]
                                ]
                 , test "buy-drone" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 60, larva: 15"
                            |> TestUtil.buyOk "drone" "6"
                            |> Expect.all
                                [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "9"
                                , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "6"
                                ]
                 , test "buy-fail" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 60, larva: 15"
                            |> TestUtil.buy "drone" "7"
                            |> Expect.equal (Err "not enough resources: meat")
                 , test "buy-first-hatchery" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, meat: 301"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "1"
                                , TestUtil.buyOk "hatchery" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "2"
                                        ]
                                ]
                 , test "buy-two-hatcheries" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, meat: 3301"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "1"
                                , TestUtil.buyOk "hatchery" "100%"
                                    >> Expect.all
                                        [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "3"
                                        ]
                                ]
                 , test "buy-first-expansion" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "territory: 10"
                            |> Expect.all
                                [ TestUtil.upgradeCount "expansion" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "expansion" "100%"
                                    >> Expect.all
                                        [ TestUtil.upgradeCount "expansion" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "territory" >> TestUtil.Expect.decimal "0"
                                        ]
                                ]
                 , test "buy nexus" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 4e15"
                            |> Expect.all
                                [ TestUtil.upgradeCount "nexus1" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "nexus1" "1"
                                    >> Expect.all
                                        [ TestUtil.upgradeCount "nexus1" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "2000"
                                        , TestUtil.buyOk "nexus2" "1"
                                            >> Expect.all
                                                [ TestUtil.upgradeCount "nexus2" >> TestUtil.Expect.decimal "1"
                                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "2"
                                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "5334"
                                                ]
                                        ]
                                ]
                 , test "buying updates unit visibility immediately, and units don't go visible -> invisible" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 9e99, queen: 1000, larva: 1, territory: 1"
                            |> Expect.all
                                [ TestUtil.unitCount "queen" >> TestUtil.Expect.decimal "1000"
                                , TestUtil.unitCount "nest" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "greaterqueen" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "hive" >> TestUtil.Expect.decimal "0"
                                , Game.units >> TestUtil.Expect.isVisible "queen"
                                , Game.units >> TestUtil.Expect.isVisible "nest"
                                , Game.units >> TestUtil.Expect.isNotVisible "greaterqueen"
                                , Game.units >> TestUtil.Expect.isNotVisible "hive"
                                , TestUtil.buyOk "nest" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "queen" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "nest" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.unitCount "greaterqueen" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "hive" >> TestUtil.Expect.decimal "0"

                                        -- queen has count == 0! this would be invisible if not for deliberate visible -> invisible prevention.
                                        , Game.units >> TestUtil.Expect.isVisible "queen"
                                        , Game.units >> TestUtil.Expect.isVisible "nest"

                                        -- greaterqueen just became visible because nest > 0. shouldn't have to wait for a tick/snapshot/etc to see that.
                                        , Game.units >> TestUtil.Expect.isVisible "greaterqueen"
                                        , Game.units >> TestUtil.Expect.isNotVisible "hive"
                                        ]
                                ]
                 , test "can't cast non-visible stuff" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "swarmling: 3, nexus: 4, energy: 40000"
                            |> Expect.all
                                [ TestUtil.unitCount "swarmling" >> TestUtil.Expect.decimal "3"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "4"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "40000"
                                , TestUtil.buy "clonearmy" "1" >> Expect.err
                                , TestUtil.buy "clonearmy" "1" >> Expect.equal (Err "item is not visible: clonearmy")
                                ]
                 , test "cast larva-rush" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "larvarush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "102400"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "cast bat larva-rush" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, nexus: 5, energy: 50000, bat: 1000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "larvarush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal (102400 * 1.3 |> floor |> String.fromInt)
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "cast meat-rush" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "drone: 1, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "meatrush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "100000007200"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "cast bat meat-rush" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "drone: 1, nexus: 5, energy: 50000, bat: 1000"
                            |> Expect.all
                                [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "meatrush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal (100000007200 * 1.3 |> floor |> String.fromInt)
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "cast territory-rush" <|
                    \_ ->
                        -- 14 swarmlings is not exactly 1 territory/sec, but close enough for expectDecimal to match
                        game0
                            |> TestUtil.updateUnits "swarmling: 14, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "territory" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "territoryrush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "territory" >> TestUtil.Expect.decimal "10000007200"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "cast uncapped clone larvae" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, larva: 10000, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "clonelarvae" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "20000"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "38000"
                                        ]
                                ]
                 , test "cast multiple clone larvae" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, larva: 10000, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "clonelarvae" "2"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "40000"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "26000"
                                        ]
                                ]
                 , test "cast capped clone larvae" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, larva: 200000, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "200000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "clonelarvae" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "300000"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "38000"
                                        ]
                                ]
                 , test "cast uncapped -> capped clone larvae" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, larva: 75000, nexus: 5, energy: 50000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "75000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "clonelarvae" "2"
                                    >> Expect.all
                                        [ -- cast 1: 75000 + min(75000, 100000) = 150000
                                          -- cast 2: 150000 + min(150000, 100000) = 250000
                                          TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "250000"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "26000"
                                        ]
                                ]
                 , test "cast swarmwarp" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, queen: 1, nexus: 5, energy: 40000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "40000"
                                , TestUtil.buyOk "swarmwarp" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "900"
                                        , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "1800"

                                        -- it's not velocity * sec - swarmwarp uses the proper polynomial formula here
                                        --
                                        -- queens.prod.total * t^2 / 2!
                                        -- = 1 * 2 * (900 ^ 2) / 2
                                        -- = 900 ^ 2
                                        -- = 810000
                                        , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "810000"

                                        -- swarmwarp doesn't generate energy
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "38000"
                                        ]
                                ]
                 , test "cast low-energy swarmwarp" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, queen: 1, nexus: 5, energy: 2000"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "2000"
                                , TestUtil.buyOk "swarmwarp" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "900"
                                        , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "1800"

                                        -- it's not velocity * sec - swarmwarp uses the proper polynomial formula here
                                        --
                                        -- queens.prod.total * t^2 / 2!
                                        -- = 1 * 2 * (900 ^ 2) / 2
                                        -- = 900 ^ 2
                                        -- = 810000
                                        , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "810000"

                                        -- swarmwarp doesn't generate energy, even at low energy
                                        -- where my subtraction shenanigans might temporarily take energy below zero
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "0"

                                        -- we correctly reset skipped-time after another snapshot
                                        , TestUtil.buyOk "drone" "900"
                                            >> Expect.all
                                                [ TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "2700"
                                                , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "801000"
                                                ]
                                        ]
                                ]
                 , test "hatchery premutagen" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 39, meat: 4e40"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "hatchery" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "40"

                                        -- this is a random result. Looks right-ish; compared with swarm1's result by hand
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "8564"
                                        ]
                                ]
                 , test "premutagen seed is based on start time" <|
                    \_ ->
                        -- other than start time and premutagen result, identical to above test
                        Game.empty gameData (Time.millisToPosix <| nowMs + 1)
                            |> TestUtil.updateUnits "hatchery: 39, meat: 4e40"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "hatchery" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "40"

                                        -- this is a random result. Looks right-ish; compared with swarm1's result by hand
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "9270"
                                        ]
                                ]
                 , test "no premutagen below minimum level" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 4e39"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "hatchery" "39"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                        ]
                                ]
                 , test "second hatchery premutagen not guaranteed" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 39, meat: 4e41"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                , TestUtil.buyOk "hatchery" "2"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "41"
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "8564"
                                        ]
                                ]
                 , test "mutation frequency " <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "hatchery: 39, meat: 4e41, mutantfreq: 1e99"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"

                                -- mostly identical to above. with added mutation frequency, both hatcheries grant mutagen
                                , TestUtil.buyOk "hatchery" "2"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "41"
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "18534"
                                        ]
                                ]
                 , test "meta-mutation" <|
                    \_ ->
                        -- 9900 mutantEach happens to be exactly double mutagen gains
                        game0
                            |> TestUtil.updateUnits "hatchery: 39, meat: 4e41, mutanteach: 9900"
                            |> Expect.all
                                [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "39"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"

                                -- mostly identical to above; mutagen gains doubled per the mutation
                                , TestUtil.buyOk "hatchery" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "hatchery" >> TestUtil.Expect.decimal "40"
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal (8564 * 2 |> String.fromInt)
                                        ]
                                ]
                 , test "mutate meat" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "drone: 1, mutantmeat: 9900"
                            |> Expect.all
                                [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.upgradeCount "droneprod" >> TestUtil.Expect.decimal "0"
                                , TestUtil.tick (100 * 1000)
                                    >> Expect.all
                                        [ -- faster than the natural 1 meat/sec
                                          TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "196"
                                        ]
                                ]
                 , test "mutate larva" <|
                    \_ ->
                        -- exactly 4x normal production from mutation
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, mutanthatchery: 9990"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "mutanthatchery" >> TestUtil.Expect.decimal "9990"
                                , TestUtil.tick (100 * 1000)
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "400"
                                        ]
                                ]
                 , test "mutate lepi (nexus)" <|
                    \_ ->
                        -- exactly 1.5x normal production cap from mutation
                        game0
                            |> TestUtil.updateUnits "nexus: 1, mutantnexus: 2000"
                            |> Expect.all
                                [ TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "0"
                                , TestUtil.tick (100 * 1000)
                                    >> Expect.all
                                        [ TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "15"

                                        -- mutation affects production, yay. How about energy cap?
                                        , TestUtil.tick 9999999999
                                            >> Expect.all
                                                [ TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "15000"
                                                ]
                                        ]
                                ]
                 , test "mutate army" <|
                    \_ ->
                        -- territory mutation's got its own weird formula, trouble deriving a nice round multiplier here.
                        -- 2^50 is pretty close to 2x, though.
                        -- this starts much slower than the other log/asymptote mutations, but scales faster laster.
                        game0
                            |> TestUtil.updateUnits "stinger: 1, mutantarmy: 1e15"
                            |> Expect.all
                                [ TestUtil.unitCount "territory" >> TestUtil.Expect.decimal "0"
                                , TestUtil.tick (100 * 1000)
                                    >> Expect.all
                                        [ TestUtil.unitCount "territory" >> TestUtil.Expect.decimal "630"
                                        ]
                                ]
                 , test "mutate rush (larva)" <|
                    \_ ->
                        -- 2x from mutation
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, nexus: 5, energy: 50000, mutantrush: 90"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "larvarush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "409600"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "mutate bats (larva)" <|
                    \_ ->
                        -- 2x from mutation
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, nexus: 5, energy: 50000, mutantbat: 900"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "larvarush" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "204800"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "48800"
                                        ]
                                ]
                 , test "mutate swarmwarp" <|
                    \_ ->
                        -- 3x from mutation
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, queen: 1, nexus: 5, energy: 40000, mutantswarmwarp: 90"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "40000"
                                , TestUtil.buyOk "swarmwarp" "1"
                                    >> Expect.all
                                        [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "2700"
                                        , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "5400"

                                        -- queens.prod.total * t^2 / 2!
                                        -- = 1 * 2 * ((2700) ^ 2) / 2
                                        -- = 2700 ^ 2
                                        -- = 7290000
                                        , TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "7290000"

                                        -- swarmwarp doesn't generate energy
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "38000"
                                        ]
                                ]
                 , test "clone mutation (uncapped -> capped)" <|
                    \_ ->
                        -- 2.8x from mutation
                        game0
                            |> TestUtil.updateUnits "hatchery: 1, larva: 210000, nexus: 5, energy: 50000, mutantclone: 90"
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "210000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "50000"
                                , TestUtil.buyOk "clonelarvae" "2"
                                    >> Expect.all
                                        [ -- cast 1: 210000 + min(210000, 280000) = 420000
                                          -- cast 2: 420000 + min(420000, 280000) = 700000
                                          TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "700000"
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "26000"
                                        ]
                                ]
                 , test "mutation unlock upgrade cost increases as you buy others" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "mutagen: 15626"
                            |> Expect.all
                                [ TestUtil.upgradeCount "mutatemeat" >> TestUtil.Expect.decimal "0"
                                , TestUtil.upgradeCount "mutatehatchery" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "15626"
                                , TestUtil.buyOk "mutatemeat" "1"
                                    >> Expect.all
                                        [ TestUtil.upgradeCount "mutatemeat" >> TestUtil.Expect.decimal "1"
                                        , TestUtil.upgradeCount "mutatehatchery" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "15625"
                                        , TestUtil.buyOk "mutatehatchery" "1"
                                            >> Expect.all
                                                [ TestUtil.upgradeCount "mutatemeat" >> TestUtil.Expect.decimal "1"
                                                , TestUtil.upgradeCount "mutatehatchery" >> TestUtil.Expect.decimal "1"
                                                , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "0"
                                                ]
                                        ]
                                ]
                 , test "ascension" <|
                    \_ ->
                        -- energy/spell counts here were chosen carefully.
                        -- * 5.6m = default first ascension cost
                        -- * 2500 energy * 200 clonearmy-casts = 500000 energy spent
                        -- * 500k / 50k = cost halved 10 times
                        -- * 5.6m / (2^10) = 5468.75 actual ascension cost
                        game0
                            |> Game.updateUpgrades (TestUtil.decimals "expansion: 5")
                            |> Game.updateSpells (TestUtil.decimals "clonearmy: 200")
                            |> TestUtil.updateUnits "nexus: 1, energy: 5000, premutagen: 9, mutagen: 3, crystal: 7, larva: 11"
                            -- |> Game.snapshot
                            |> Expect.all
                                [ TestUtil.unitCount "ascension" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "9"
                                , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "3"
                                , TestUtil.upgradeCount "expansion" >> TestUtil.Expect.decimal "5"
                                , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "5000"
                                , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "1"
                                , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "0"
                                , TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "7"
                                , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "11"
                                , TestUtil.buyOk "ascension" "1"
                                    >> Expect.all
                                        [ -- ascends incremented
                                          TestUtil.unitCount "ascension" >> TestUtil.Expect.decimal "1"

                                        -- mutagen preserved; premutagen converted
                                        , TestUtil.unitCount "premutagen" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "12"

                                        -- all upgrades cleared
                                        , TestUtil.upgradeCount "expansion" >> TestUtil.Expect.decimal "0"

                                        -- most units cleared...
                                        , TestUtil.unitCount "energy" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "nexus" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "0"

                                        -- ...except for the exceptions
                                        , TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "7"

                                        -- ...and "cleared" for starting units doesn't mean zero
                                        , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10"
                                        ]
                                ]
                 , test "ascend with mutant-lepi increasing cost" <|
                    \_ ->
                        -- identical numbers to above test, except more energy and lots more mutantnexus (to increase cost).
                        -- Ascending now costs 70000ish.
                        game0
                            |> Game.updateUpgrades (TestUtil.decimals "expansion: 5")
                            |> Game.updateSpells (TestUtil.decimals "clonearmy: 200")
                            |> TestUtil.updateUnits "nexus: 1, energy: 5000, premutagen: 9, mutagen: 3, crystal: 7, larva: 11, mutantnexus: 1e99"
                            |> TestUtil.buy "ascension" "1"
                            |> Expect.equal (Err "not enough resources: energy")
                 , test "hatchery/expansion crystals on cooldown: not visible, no crystals" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 3333333"
                            |> TestUtil.buyOk "hatchery" "1"
                            |> Expect.all
                                [ TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "0"
                                ]
                 , test "hatchery/expansion crystals on cooldown" <|
                    \_ ->
                        game0
                            |> TestUtil.updateUnits "meat: 3333333, nexus: 1"
                            |> TestUtil.buyOk "hatchery" "1"
                            |> Expect.all
                                [ -- free crystals!
                                  TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "500"
                                , TestUtil.tickPlus (30 * 60 * 1000 - 1)
                                    >> TestUtil.buyOk "hatchery" "1"
                                    >> Expect.all
                                        [ -- no free crystals: still on cooldown
                                          TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "500"

                                        -- finish up the cooldown and try again
                                        , TestUtil.tickPlus 1
                                            >> TestUtil.buyOk "hatchery" "1"
                                            >> Expect.all
                                                [ -- more free crystals!
                                                  TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "1000"
                                                ]
                                        ]
                                ]
                 , test "mutagen respec" <|
                    \_ ->
                        game0
                            |> Game.updateUpgrades (TestUtil.decimals "mutatemeat: 1, mutatehatchery: 1")
                            |> TestUtil.updateUnits "mutagen: 100, mutantmeat: 10000, crystal: 5555"
                            >> Expect.all
                                [ Game.units >> TestUtil.Expect.isVisible "mutantmeat"
                                , Game.units >> TestUtil.Expect.isVisible "mutanthatchery"
                                , Game.units >> TestUtil.Expect.isNotVisible "mutantbat"
                                , TestUtil.buyOk "mutagenrespec" "1"
                                    >> Expect.all
                                        [ -- spends the crystals
                                          TestUtil.unitCount "crystal" >> TestUtil.Expect.decimal "555"

                                        -- resets mutagen upgrades and units
                                        , TestUtil.upgradeCount "mutatemeat" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.upgradeCount "mutatehatchery" >> TestUtil.Expect.decimal "0"
                                        , TestUtil.unitCount "mutantmeat" >> TestUtil.Expect.decimal "0"

                                        -- mutagen units are no longer visible
                                        , Game.units >> TestUtil.Expect.isNotVisible "mutantmeat"
                                        , Game.units >> TestUtil.Expect.isNotVisible "mutanthatchery"
                                        , Game.units >> TestUtil.Expect.isNotVisible "mutantbat"

                                        -- restores spent mutagen. how much to expect, exactly?
                                        -- * mutantmeat: 10000
                                        -- * 2 upgrades: 1 + 15625
                                        -- * total spent: 25626
                                        -- * respec refunds 70%: 25626 * 0.7 = 17938.2
                                        -- * started with: 100 mutagen
                                        -- * expected total: 18038.2
                                        , TestUtil.unitCount "mutagen" >> TestUtil.Expect.decimal "18038.2"
                                        ]
                                ]
                 , test "accomplished ancestry" <|
                    \_ ->
                        game0
                            |> Game.updateUpgrades (TestUtil.decimals "achievementbonus: 3")
                            |> TestUtil.updateUnits "hatchery: 1"
                            -- 1000 apoints: x2 production per achievementbonus
                            |> Game.debugAchievementPoints { val = 1000, max = 0, percent = 0 }
                            |> Game.refreshSnapshotUnits
                            |> Expect.all
                                [ TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "0"
                                , Game.achievementPoints >> .val >> Expect.equal 1000
                                , TestUtil.tickPlus (100 * 1000)
                                    >> Expect.all
                                        [ Game.achievementPoints >> .val >> Expect.equal 1000

                                        -- 1 larva/sec * (1 + 1000/1000)^3 achievementbonus = 8 larva/sec
                                        , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "800"
                                        ]
                                ]
                 , describe "fancy buy parsing"
                    [ test "percent" <|
                        \_ ->
                            game0
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "100%"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "9"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "11"
                                    ]
                    , test "target" <|
                        \_ ->
                            game0
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "@8"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "30"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "12"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "8"
                                    ]

                    -- because it's infuriating to see @ links round down
                    , test "target-precision" <|
                        \_ ->
                            game0
                                |> TestUtil.updateUnits "meat: 9e999, drone: 9e999, larva: 9e999, queen: 55981.8"
                                |> TestUtil.buyOk "queen" "@100000"
                                |> Expect.all
                                    -- not 999997! round up for @!
                                    [ TestUtil.unitCount "queen" >> TestUtil.Expect.decimalGreaterThan "100000"
                                    ]
                    , test "val: with-twins" <|
                        \_ ->
                            game0
                                |> Game.updateUpgrades (TestUtil.decimals "dronetwin: 2")
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "5"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "25"
                                    ]
                    , test "twinval" <|
                        \_ ->
                            game0
                                |> Game.updateUpgrades (TestUtil.decimals "dronetwin: 2")
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "=20"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "25"
                                    ]
                    , test "twinval: uneven" <|
                        \_ ->
                            game0
                                |> Game.updateUpgrades (TestUtil.decimals "dronetwin: 2")
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "=17"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "25"
                                    ]
                    , test "target: with-twins" <|
                        \_ ->
                            game0
                                |> Game.updateUpgrades (TestUtil.decimals "dronetwin: 2")
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "@25"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "10"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "25"
                                    ]
                    , test "target: with-twins, uneven" <|
                        \_ ->
                            -- if uneven, round up. @ links are infuriating otherwise.
                            game0
                                |> Game.updateUpgrades (TestUtil.decimals "dronetwin: 2")
                                |> TestUtil.updateUnits "meat: 60, larva: 15, drone: 5"
                                |> TestUtil.buyOk "drone" "@28"
                                |> Expect.all
                                    [ TestUtil.unitCount "meat" >> TestUtil.Expect.decimal "0"
                                    , TestUtil.unitCount "larva" >> TestUtil.Expect.decimal "9"
                                    , TestUtil.unitCount "drone" >> TestUtil.Expect.decimal "29"
                                    ]
                    , test "buy more than 1e308 at once" <|
                        \_ ->
                            game0
                                |> TestUtil.updateUnits "meat: 9e999, drone: 9e999, larva: 9e999"
                                |> TestUtil.buyOk "queen" "1e400"
                                |> Expect.all
                                    [ TestUtil.unitCount "queen" >> TestUtil.Expect.decimal "1e400"
                                    ]
                    ]
                 ]
                )
            ]
        , describe "achievements"
            (let
                game0 =
                    Game.empty gameData now

                achievement name =
                    gameData
                        |> GameData.achievements
                        |> List.filter (\a -> Achievement.name a == name)
                        |> List.head
                        |> TestUtil.unsafeJust ("no such achievement: " ++ name)

                status name =
                    Game.achievements
                        >> List.filter (\( a, _ ) -> Achievement.name a == name)
                        >> List.head
                        >> TestUtil.unsafeJust ("no such achievement: " ++ name)
                        >> Tuple.second
             in
             [ test "item-based" <|
                \_ ->
                    game0
                        |> TestUtil.updateUnits "meat: 2011, larva: 202"
                        |> TestUtil.buyOk "drone" "1"
                        |> Game.clearNewAchievements
                        |> Expect.all
                            [ status "drone1" >> Game.Achievement.isEarned >> Expect.true "drone1 earned"
                            , status "drone2" >> Game.Achievement.isVisible >> Expect.true "drone2 visible"
                            , status "drone3" >> Game.Achievement.isVisible >> Expect.true "drone3 visible"
                            , Game.newAchievements >> Expect.equal Nothing
                            , TestUtil.buyOk "drone" "200"
                                >> Expect.all
                                    [ status "drone1" >> Game.Achievement.isEarned >> Expect.true "drone1 earned"
                                    , status "drone2" >> Game.Achievement.isEarned >> Expect.true "drone2 earned"
                                    , status "drone3" >> Game.Achievement.isVisible >> Expect.true "drone3 visible"
                                    , Game.newAchievements >> Expect.equal (Just ( now, achievement "drone2", [] ))
                                    ]
                            ]
             ]
            )
        ]


nowMs : Int
nowMs =
    0


now : Posix
now =
    Time.millisToPosix nowMs


brokenExample : D.Value
brokenExample =
    "{}" |> D.decodeString D.value |> TestUtil.unsafeOk
