module PlayFab.ChunkedStateTest exposing (all)

import Expect
import Json.Decode as D
import Json.Encode as E
import PlayFab.ChunkedState as ChunkedState
import Test exposing (..)


all : Test
all =
    describe "chunkedState tests"
        [ describe "decoder"
            [ test "missing" <|
                \_ ->
                    "{}"
                        |> D.decodeString (ChunkedState.decoder "state" [ "val" ])
                        |> Expect.err
            , test "one chunk" <|
                \_ ->
                    "{\"state\": {\"val\": \"abc123\"}}"
                        |> D.decodeString (ChunkedState.decoder "state" [ "val" ])
                        |> Expect.equal (Ok "abc123")
            , test "two chunks" <|
                \_ ->
                    "{\"state\": {\"val\": \"abc\"}, \"state1\": {\"val\": \"123\"}}"
                        |> D.decodeString (ChunkedState.decoder "state" [ "val" ])
                        |> Expect.equal (Ok "abc123")
            , test "three chunks" <|
                \_ ->
                    "{\"state\": {\"val\": \"abc\"}, \"state1\": {\"val\": \"123\"}, \"state2\": {\"val\": \"def\"}}"
                        |> D.decodeString (ChunkedState.decoder "state" [ "val" ])
                        |> Expect.equal (Ok "abc123def")
            ]
        , describe "encoder"
            [ test "empty" <|
                \_ ->
                    ""
                        |> ChunkedState.encoderNextKey { prefix = "state", size = 3 }
                        |> Tuple.mapFirst (E.encode 0)
                        |> Expect.equal ( "{\"state\":\"\"}", "state1" )
            , test "one chunk" <|
                \_ ->
                    "abc"
                        |> ChunkedState.encoderNextKey { prefix = "state", size = 3 }
                        |> Tuple.mapFirst (E.encode 0)
                        |> Expect.equal ( "{\"state\":\"abc\"}", "state1" )
            , test "1/2 chunk" <|
                \_ ->
                    "ab"
                        |> ChunkedState.encoderNextKey { prefix = "state", size = 3 }
                        |> Tuple.mapFirst (E.encode 0)
                        |> Expect.equal ( "{\"state\":\"ab\"}", "state1" )
            , test "two chunks" <|
                \_ ->
                    "abc123"
                        |> ChunkedState.encoderNextKey { prefix = "state", size = 3 }
                        |> Tuple.mapFirst (E.encode 0)
                        |> Expect.equal ( "{\"state\":\"abc\",\"state1\":\"123\"}", "state2" )
            , test "3/2 chunks" <|
                \_ ->
                    "abc12"
                        |> ChunkedState.encoderNextKey { prefix = "state", size = 3 }
                        |> Tuple.mapFirst (E.encode 0)
                        |> Expect.equal ( "{\"state\":\"abc\",\"state1\":\"12\"}", "state2" )
            ]
        ]
