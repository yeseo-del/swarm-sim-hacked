module DecimalDict exposing (add, mul, sub, subEpsilon, unsafeSub)

import Decimal exposing (Decimal)
import Dict exposing (Dict)


add : String -> Decimal -> Dict String Decimal -> Dict String Decimal
add key d =
    Dict.update key (Maybe.withDefault Decimal.zero >> Decimal.add d >> Just)


mul : String -> Decimal -> Dict String Decimal -> Dict String Decimal
mul key d =
    Dict.update key (Maybe.withDefault Decimal.zero >> Decimal.mul d >> Just)


unsafeSub : String -> Decimal -> Dict String Decimal -> Dict String Decimal
unsafeSub key d =
    Dict.update key (Maybe.withDefault Decimal.zero >> Decimal.sub d >> Just)


{-| Acceptable error percentage for subtraction.

For example; if something costs 1,000,000 and the user has 999,999; allow it.
This allows our woefully imprecise `Decimal` library to look mostly-correct to
casual users.

-}
subEpsilon : Float
subEpsilon =
    0.999999


sub : String -> String -> Decimal -> Dict String Decimal -> Result String (Dict String Decimal)
sub err key d dict =
    let
        val0 =
            Dict.get key dict |> Maybe.withDefault Decimal.zero

        -- val1 =Decimal.sub val0 d
    in
    -- epsilon shenanigans to accept minor decimal imprecision
    if Decimal.gte val0 (Decimal.mulFloat subEpsilon d) then
        dict
            |> Dict.insert key (Decimal.sub val0 d |> Decimal.max Decimal.zero)
            |> Ok

    else
        Err err
