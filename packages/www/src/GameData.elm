module GameData exposing
    ( GameData
    , achievementByName
    , achievements
    , cappedUnits
    , decoder
    , homeTab
    , initUnits
    , itemByName
    , itemBySlug
    , items
    , itemsByCost
    , lang
    , mtxById
    , mtxByName
    , mtxs
    , spellByName
    , spellBySlug
    , spells
    , spellsByTab
    , spellsByTabId
    , tabBySlug
    , tabs
    , unitById
    , unitByName
    , unitBySlug
    , units
    , unitsByTab
    , unitsByTabId
    , unitsByTabName
    , upgradeByName
    , upgrades
    , upgradesByUnit
    , upgradesByUnitId
    )

import Array exposing (Array)
import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Dict.Extra
import GameData.Achievement as Achievement exposing (Achievement)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.Item as Item exposing (Item)
import GameData.Lang as Lang
import GameData.Mtx as Mtx exposing (Mtx)
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Json.Decode as D
import Maybe.Extra
import Parser.Stat
import Set exposing (Set)


type GameData
    = GameData Lists Indexes Lang.LangSet


type alias Lists =
    { tabs : List Tab
    , units : List Unit
    , upgrades : List Upgrade
    , spells : List Spell
    , achievements : List Achievement
    , mtxs : List Mtx
    }


type alias Indexes =
    { items : List Item
    , tabsByName : Dict String Tab
    , unitsByName : Dict String Unit
    , unitsBySlug : Dict String Unit
    , unitsByTab : Dict String (List Unit)

    -- , watchableUnits : Set String
    , upgradesByName : Dict String Upgrade
    , upgradesByUnit : Dict String (List Upgrade)
    , upgradesByTab : Dict String (List Upgrade)
    , spellsByName : Dict String Spell
    , spellsBySlug : Dict String Spell
    , spellsByTab : Dict String (List Spell)
    , itemsByName : Dict String Item
    , itemsBySlug : Dict String Item
    , itemsByCost : Dict String (List ( Item, CostVal ))
    , achievesByName : Dict String Achievement
    , mtxsByName : Dict String Mtx

    -- , achievesByItem : Dict String (List Achievement)
    }


createIndexes : Lists -> Indexes
createIndexes l =
    let
        unitsByName =
            l.units |> Dict.Extra.fromListBy Unit.name

        items_ =
            List.map Item.Unit l.units ++ List.map Item.Upgrade l.upgrades ++ List.map Item.Spell l.spells
    in
    { items = items_
    , tabsByName = l.tabs |> Dict.Extra.fromListBy Tab.name

    -- units
    , unitsByName = unitsByName
    , unitsBySlug = l.units |> Dict.Extra.fromListBy (.slug >> String.toLower)
    , unitsByTab = l.units |> Dict.Extra.groupBy (.tab >> Tab.idToString) |> Dict.map (always List.reverse)

    -- , watchableUnits = l.units |> List.filter .watchable |> List.map Unit.name |> Set.fromList
    -- upgrades
    , upgradesByName = l.upgrades |> Dict.Extra.fromListBy Upgrade.name
    , upgradesByUnit = l.upgrades |> Dict.Extra.groupBy (.unitId >> Unit.idToString)
    , upgradesByTab =
        l.upgrades
            |> Dict.Extra.groupBy
                (\u ->
                    Dict.get (Unit.idToString u.unitId) unitsByName
                        |> Maybe.map (.tab >> Tab.idToString)
                        |> Maybe.withDefault "__none__"
                )
            |> Dict.remove "__none__"

    -- spells
    , spellsByName = l.spells |> Dict.Extra.fromListBy Spell.name
    , spellsBySlug = l.spells |> Dict.Extra.fromListBy (.slug >> String.toLower)
    , spellsByTab = l.spells |> Dict.Extra.groupBy (.tab >> Tab.idToString)

    -- items
    , itemsByName = items_ |> Dict.Extra.fromListBy Item.name
    , itemsBySlug =
        items_
            |> List.filterMap (\item -> item |> Item.slug |> Maybe.map (Tuple.pair item))
            |> Dict.Extra.fromListBy Tuple.second
            |> Dict.map (always Tuple.first)
    , itemsByCost =
        items_
            |> List.concatMap (\item -> item |> Item.cost |> List.map (Tuple.mapSecond (Tuple.pair item)))
            |> Dict.Extra.groupBy (Tuple.first >> Unit.idToString)
            |> Dict.map (\_ -> List.map Tuple.second)

    -- achievements
    , achievesByName = l.achievements |> Dict.Extra.fromListBy Achievement.name
    , mtxsByName = l.mtxs |> Dict.Extra.fromListBy (.id >> Mtx.idToString)
    }


lists : GameData -> Lists
lists (GameData ls _ _) =
    ls


indexes : GameData -> Indexes
indexes (GameData _ is _) =
    is



-- SELECTORS


lang : Maybe Lang.Entry -> GameData -> Lang.LangSet
lang e0 (GameData _ _ l) =
    case e0 of
        Nothing ->
            -- by far the most common case. don't union dict.empty.
            l

        Just e ->
            { l
                | en_us = l.en_us |> Dict.union e
            }


tabs : GameData -> List Tab
tabs =
    lists >> .tabs


homeTab : GameData -> Maybe Tab
homeTab =
    -- The decoder could probably guarantee we get one or more tabs, to remove
    -- the `Maybe` here. No practical use for that yet though.
    lists >> .tabs >> List.head


tabBySlug : String -> GameData -> Maybe Tab
tabBySlug name (GameData _ is _) =
    is.tabsByName |> Dict.get (String.toLower name)


units : GameData -> List Unit
units =
    lists >> .units


unitBySlug : String -> GameData -> Maybe Unit
unitBySlug name =
    indexes >> .unitsBySlug >> Dict.get (String.toLower name)


unitByName : String -> GameData -> Maybe Unit
unitByName name =
    indexes >> .unitsByName >> Dict.get name


unitById : Unit.Id -> GameData -> Maybe Unit
unitById =
    Unit.idToString >> unitByName


upgradeByName : String -> GameData -> Maybe Upgrade
upgradeByName name =
    indexes >> .upgradesByName >> Dict.get name


spellByName : String -> GameData -> Maybe Spell
spellByName name =
    indexes >> .spellsByName >> Dict.get name


spellBySlug : String -> GameData -> Maybe Spell
spellBySlug name =
    indexes >> .spellsBySlug >> Dict.get (String.toLower name)


itemByName : String -> GameData -> Maybe Item
itemByName name =
    indexes >> .itemsByName >> Dict.get name


itemBySlug : String -> GameData -> Maybe Item
itemBySlug name =
    indexes >> .itemsBySlug >> Dict.get (String.toLower name)


unitsByTab : Tab -> GameData -> List Unit
unitsByTab tab =
    unitsByTabId tab.id


unitsByTabId : Tab.Id -> GameData -> List Unit
unitsByTabId =
    Tab.idToString >> unitsByTabName


unitsByTabName : String -> GameData -> List Unit
unitsByTabName tab =
    indexes >> .unitsByTab >> Dict.get tab >> Maybe.withDefault []


spellsByTab : Tab -> GameData -> List Spell
spellsByTab tab =
    spellsByTabId tab.id


spellsByTabId : Tab.Id -> GameData -> List Spell
spellsByTabId tab =
    indexes >> .spellsByTab >> Dict.get (Tab.idToString tab) >> Maybe.withDefault []


upgradesByUnit : Unit -> GameData -> List Upgrade
upgradesByUnit unit =
    upgradesByUnitId unit.id


upgradesByUnitId : Unit.Id -> GameData -> List Upgrade
upgradesByUnitId id =
    indexes >> .upgradesByUnit >> Dict.get (Unit.idToString id) >> Maybe.withDefault []


itemsByCost : Unit.Id -> GameData -> List ( Item, CostVal )
itemsByCost id =
    indexes >> .itemsByCost >> Dict.get (Unit.idToString id) >> Maybe.withDefault []


upgrades : GameData -> List Upgrade
upgrades =
    lists >> .upgrades


spells : GameData -> List Spell
spells =
    lists >> .spells


initUnits : GameData -> List ( Unit, Decimal )
initUnits =
    units >> List.filterMap (\u -> u.init |> Maybe.map (Tuple.pair u))


cappedUnits : GameData -> List ( Unit, Parser.Stat.Expr )
cappedUnits =
    units >> List.filterMap (\u -> u.maxCount |> Maybe.map (Tuple.pair u))


items : GameData -> List Item
items g =
    [ g |> units |> List.map Item.Unit
    , g |> upgrades |> List.map Item.Upgrade
    , g |> spells |> List.map Item.Spell
    ]
        |> List.concat


achievements : GameData -> List Achievement
achievements =
    lists >> .achievements


achievementByName : String -> GameData -> Maybe Achievement
achievementByName name =
    indexes >> .achievesByName >> Dict.get name


mtxs : GameData -> List Mtx
mtxs =
    lists >> .mtxs


mtxByName : String -> GameData -> Maybe Mtx
mtxByName name =
    indexes >> .mtxsByName >> Dict.get name


mtxById : Mtx.Id -> GameData -> Maybe Mtx
mtxById id =
    mtxByName <| Mtx.idToString id



-- SERIALIZATION


decoder : D.Decoder (Lang.LangSet -> GameData)
decoder =
    let
        decode0 =
            D.map2 decode1
                Cost.decoder
                OnBuy.decoder
                |> D.andThen identity

        decode1 costs onBuys =
            let
                cost id =
                    Dict.get id costs |> Maybe.withDefault []

                onBuy id =
                    Dict.get id onBuys |> Maybe.withDefault []
            in
            D.andThen (decode2 cost onBuy)
                (D.field "units" (D.map (List.indexedMap (\i u -> u i)) <| D.list <| Unit.decoder cost onBuy))

        decode2 cost onBuy units_ =
            let
                unitsByName =
                    units_ |> Dict.Extra.fromListBy Unit.name
            in
            D.map6 Lists
                (D.field "tabs" (D.list Tab.decoder))
                (D.succeed units_)
                (D.field "upgrades" (D.list <| Upgrade.decoder cost onBuy (\id -> Dict.get id unitsByName)))
                (D.field "spells" (D.list <| Spell.decoder cost onBuy))
                Achievement.decoder
                (D.field "mtx" (D.list Mtx.decoder))
                |> D.map (\l -> GameData l <| createIndexes l)
    in
    decode0
