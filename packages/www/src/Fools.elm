module Fools exposing (Fools(..), isAfter, isOn, status, toString)

import Iso8601
import Route.Config as Config
import Session exposing (Session)
import Time exposing (Posix)


type Fools
    = Invited
    | On
    | Off
    | After


toString : Fools -> String
toString s =
    case s of
        Invited ->
            "invited foolin'"

        On ->
            "yes foolin'"

        After ->
            "after foolin'"

        Off ->
            "no foolin'"


status : Session -> Fools
status session =
    -- there's a few ways april-fools can be enabled.
    -- First, forced with a querystring flag:
    case Config.get Config.Fools (Session.query session) of
        Just fools ->
            -- we can pretend it's a certain date:
            case Iso8601.toTime fools of
                Ok now ->
                    dateStatus now

                _ ->
                    -- if it's not a date, it's a fixed flag value
                    case fools of
                        "after" ->
                            After

                        "invited" ->
                            Invited

                        "off" ->
                            Off

                        -- a few more, to match querystring bool parsing
                        "" ->
                            Off

                        "0" ->
                            Off

                        -- anything else is enabled
                        _ ->
                            On

        Nothing ->
            -- If no querystring flag (common case), we use the current date.
            -- The timezone is fixed - we could use the session timezone, but
            -- I'd rather have a consistent start time for all users. Worse
            -- experience for non-Americans, but no spoilers in chat.
            case Session.now session of
                Just now ->
                    dateStatus now

                _ ->
                    Off



-- test times, from old-swarmsim
-- TODO: these should really be unit tests (but they're very unlikely to ever break...)
--
-- off (before)
-- ?fools=2015-03-31T22:59:59-05:00
--
-- on
-- ?fools=2015-03-31T23:00:00-05:00
-- ?fools=2015-03-31T21:05:00-08:00
-- ?fools=2015-04-01T00:01:00-05:00
--
-- after
-- ?fools=2015-04-01T21:00:00-08:00
-- ?fools=2015-04-03T21:00:00-08:00
--
-- off (after)
-- ?fools=2015-04-04T21:00:00-08:00


dateStatus : Posix -> Fools
dateStatus now =
    let
        tz : Time.Zone
        tz =
            Time.customZone (-4 * 60) []
    in
    case ( Time.toMonth tz now, Time.toDay tz now ) of
        ( Time.Apr, 1 ) ->
            -- we never show the images without the user manually approving -
            -- they might be playing at work. Show a message that invites them;
            -- actually showing the images must be enabled by url.
            Invited

        ( Time.Apr, 2 ) ->
            After

        ( Time.Apr, 3 ) ->
            After

        _ ->
            Off


isOn : Session -> Bool
isOn =
    status >> (==) On


isAfter : Session -> Bool
isAfter =
    status >> (==) After
