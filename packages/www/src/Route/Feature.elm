module Route.Feature exposing (Feature(..), activate, deactivate, debugs, filterDefaults, insert, isActive, list, temps, toString, toggle)

{-| Boolean feature switches.
-}

import Dict exposing (Dict)
import Maybe.Extra


type
    Feature
    -- Show various debugging output, and a link to the `/debug` page
    = Debug
      -- Show an FPS counter
    | Fps
      -- We're inside a Kongregate iframe
    | Kongregate
      -- In-game translation overrides
    | Translator
      -- Enable Paypal and Kreds purchases
    | Mtx
      -- Enable the new responsive mobile interface
    | MobileUI


list : List Feature
list =
    [ Debug
    , Fps
    , Kongregate
    , Translator
    , Mtx
    , MobileUI
    ]


debugs : List Feature
debugs =
    [ Debug
    , Fps
    , Translator
    ]


temps : List Feature
temps =
    [ Mtx
    , MobileUI
    ]


toString : Feature -> String
toString feature =
    case feature of
        Debug ->
            "debug"

        Fps ->
            "fps"

        Kongregate ->
            "kongregate"

        Translator ->
            "translator"

        Mtx ->
            "mtx"

        MobileUI ->
            "mobileui"


defaultValue : Feature -> Bool
defaultValue f =
    case f of
        Mtx ->
            True

        MobileUI ->
            True

        _ ->
            False


stringToBool : String -> Bool
stringToBool val =
    case val of
        "0" ->
            False

        "" ->
            False

        _ ->
            True


isActive : Feature -> Dict String String -> Bool
isActive feature =
    case feature of
        _ ->
            Dict.get (toString feature) >> Maybe.Extra.unwrap (defaultValue feature) stringToBool


activate : Feature -> Dict String String -> Dict String String
activate f d =
    insert f True d


deactivate : Feature -> Dict String String -> Dict String String
deactivate f d =
    insert f False d


remove : Feature -> Dict String String -> Dict String String
remove f =
    Dict.remove (toString f)


toggle : Feature -> Dict String String -> Dict String String
toggle f d =
    insert f (not <| isActive f d) d


insert : Feature -> Bool -> Dict String String -> Dict String String
insert f b =
    if b == defaultValue f then
        remove f

    else
        Dict.insert (toString f) <|
            if b then
                "1"

            else
                "0"


isDefault : Feature -> Dict String String -> Bool
isDefault f q =
    isActive f q == defaultValue f


filterDefaults : Dict String String -> Dict String String
filterDefaults query =
    let
        fold f q =
            if isActive f q == defaultValue f then
                remove f q

            else
                q
    in
    List.foldl fold query list
