module Api exposing (update)

import Browser.Navigation as Nav
import Game exposing (Game)
import Game.Achievement
import Game.BuyOrder
import Game.Order as Order exposing (Order)
import Game.Statistics
import Game.UnitsSnapshot
import GameData exposing (GameData)
import Json.Decode as D
import Json.Encode as E
import JsonUtil
import Ports
import RemoteData exposing (RemoteData)
import Session exposing (Session)


type Msg
    = Get
    | Buy String String
    | Undo
    | Nav String
    | Reset


update : D.Value -> Session -> ( Session, Cmd msg )
update req session0 =
    -- Decode the requestId separately, so we can include it in later error messages.
    case D.decodeValue requestIdDecoder req of
        Err err ->
            ( session0 |> Session.pushError ("swarmApi couldn't decode requestId: " ++ D.errorToString err), Cmd.none )

        Ok requestId ->
            case call requestId req session0 of
                Err err ->
                    ( session0, Ports.apiResponse <| E.object [ ( "requestId", E.string requestId ), ( "error", E.string err ) ] )

                Ok ( session1, cmd1, res ) ->
                    let
                        ( session2, cmd2 ) =
                            Session.grantAchievement "bot-api" session1
                    in
                    ( session2, Cmd.batch [ cmd1, cmd2, Ports.apiResponse res ] )


call : String -> D.Value -> Session -> Result String ( Session, Cmd msg, D.Value )
call requestId req session0 =
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                now =
                    Session.safeNow game0 session0

                res : List ( String, E.Value ) -> E.Value
                res =
                    (++)
                        [ ( "requestId", E.string requestId )
                        , ( "timestamp", now |> JsonUtil.posixEncoder )
                        ]
                        >> E.object
            in
            case D.decodeValue requestDecoder req of
                Err err ->
                    Err <| "swarmApi couldn't decode call: " ++ D.errorToString err

                Ok msg ->
                    case msg of
                        Get ->
                            Ok ( session0, Cmd.none, res [ ( "game", game0 |> Game.snapshotUnits now |> gameEncoder ) ] )

                        Buy itemName count ->
                            case GameData.itemByName itemName (Game.gameData game0) of
                                Nothing ->
                                    Err <| "no such item: " ++ itemName

                                Just item ->
                                    let
                                        game =
                                            Game.snapshotUnits now game0
                                    in
                                    Order.fromBuyN (Game.setBuyN item count game) (Game.units game) item
                                        |> Result.andThen (\o -> Game.BuyOrder.buy o game)
                                        |> Result.map (Session.persistWrite session0)
                                        |> Result.map (\( g, cmd ) -> ( Session.setGame True g session0, cmd, res [ ( "game", gameEncoder g ) ] ))

                        Undo ->
                            let
                                ( session, cmd ) =
                                    Session.undo session0

                                response =
                                    case Session.game session of
                                        RemoteData.Success g ->
                                            [ ( "game", gameEncoder g ) ]

                                        _ ->
                                            []
                            in
                            Ok ( session, cmd, res response )

                        Reset ->
                            Session.resetGame session0
                                |> Result.map (\session -> ( session, Cmd.none, res [] ))

                        Nav path ->
                            Ok ( session0, Nav.pushUrl (Session.nav session0) path, res [] )

        _ ->
            Err "game still loading. Try again in a moment."


{-| Encode a representation of the game state for api responses.

"Why not just use `Game.encoder`?" I don't want the game's persisted format
coupled to user's bot-api scripts. This is more tedious, but gives us precise
control over what the bot-api can access.

-}
gameEncoder : Game -> E.Value
gameEncoder game =
    E.object
        [ ( "created", game |> Game.created |> JsonUtil.posixEncoder )
        , ( "units", game |> Game.units |> Game.UnitsSnapshot.toDict |> E.dict identity JsonUtil.decimalEncoder )
        , ( "upgrades", game |> Game.upgradesDict |> E.dict identity JsonUtil.decimalEncoder )
        , ( "statistics", game |> Game.statistics |> Game.Statistics.encoder )
        , ( "achievements", game |> Game.achievementsDict |> Game.Achievement.encoder )
        ]


requestIdDecoder : D.Decoder String
requestIdDecoder =
    D.field "requestId" D.string


requestDecoder : D.Decoder Msg
requestDecoder =
    let
        bodyDecoder name =
            D.field "args" <|
                case name of
                    "get" ->
                        D.succeed Get

                    "undo" ->
                        D.succeed Undo

                    "reset" ->
                        D.succeed Reset

                    "nav" ->
                        D.map Nav
                            (D.field "path" D.string)

                    "buy" ->
                        D.map2 Buy
                            (D.field "item" D.string)
                            (D.field "count" <|
                                -- be forgiving of js numbers
                                D.oneOf
                                    [ D.string
                                    , D.float |> D.map String.fromFloat
                                    ]
                            )

                    _ ->
                        D.fail <| "no such api function: " ++ name
    in
    D.field "name" D.string |> D.andThen bodyDecoder
