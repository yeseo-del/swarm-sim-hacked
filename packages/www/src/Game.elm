module Game exposing
    ( Game, empty, load
    , gameData, locale, created, units, snapshotted, unitCount, upgradeCount, spellCount, itemCount, production, graph, polynomials, buyN, watched
    , setLocale, updateUnits, snapshotUnits, setBuyN, insertWatched
    , encoder, decoder, persistWrite
    , AchievementsShown, AchievementsSortOrder(..), CostUI(..), VelocityUI(..), achievementPoints, achievementProgress, achievements, achievementsDict, achievementsShown, addItem, addItemId, clearNewAchievements, clearUI, cooldownRemaining, costUI, debugAchievementPoints, debugNewAchievement, encoded, evalUnits, expireNewAchievements, fadeNewAchievement, flipLastUnitByTab, getGameVar, getVar, grantAchievement, lastUnitByTab, mapAchievementsShown, mtxHistory, newAchievements, numberFormat, popCooldown, pushStatistics, randomStep, recalculateVisibility, refreshSnapshotUnits, removeItemId, setCostUI, setLastUnitByTab, setNumberFormat, setTheme, setVelocityUI, setVelocityUnit, skipTime, snapItemCount, stateDicts, statistics, theme, updateKongMtxHistory, updatePlayFabMtxHistory, updateSpells, updateUpgrades, upgradeCountName, upgradesDict, velocityUI, velocityUnit, velocityUnitSeconds
    )

{-| The game. All persistent game state.


# Creation

@docs Game, empty, load


# Accessors

@docs gameData, locale, created, units, snapshotted, unitCount, upgradeCount, spellCount, itemCount, production, graph, polynomials, buyN, watched


# Updates

@docs setLocale, updateUnits, snapshotUnits, setBuyN, insertWatched


# Serialization

@docs encoder, decoder, persistWrite

-}

import Array exposing (Array)
import Decimal exposing (Decimal)
import DecimalDict
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game.Achievement
import Game.BuyN as BuyN
import Game.MtxHistory as MtxHistory exposing (MtxHistory)
import Game.NumberFormat as NumberFormat exposing (NumberFormat)
import Game.ProductionGraph as ProductionGraph
import Game.Statistics as Statistics exposing (Statistics)
import Game.Theme as Theme exposing (Theme)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Game.VelocityUnit as VelocityUnit exposing (VelocityUnit)
import Game.Watched as Watched exposing (Watched)
import GameData exposing (GameData)
import GameData.Achievement as Achievement exposing (Achievement)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.Item as Item exposing (Item)
import GameData.Item.Id as ItemId
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab.Id as Tab
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Json.Decode as D
import Json.Decode.Pipeline as P
import Json.Encode as E
import JsonUtil
import Kongregate.Inventory
import Locale exposing (Locale)
import Maybe.Extra
import Parser.Stat
import Parser.Visibility
import PlayFab.Inventory
import Polynomial exposing (Polynomial)
import Ports
import Random exposing (Generator)
import RandomUtil
import Route exposing (Route)
import Set exposing (Set)
import Time exposing (Posix)


type Game
    = Game Data


type alias Data =
    { gameData : GameData
    , locale : Maybe Locale
    , theme : Theme
    , numberFormat : NumberFormat
    , velocityUnit : VelocityUnit
    , costUI : CostUI
    , velocityUI : VelocityUI
    , created : Posix
    , encoded : Posix
    , units : UnitsSnapshot
    , graph : ProductionGraph.Graph
    , polynomials : Dict String Polynomial
    , upgrades : Dict String Decimal
    , spells : Dict String Decimal
    , buyN : BuyN.Settings
    , watched : Dict String Watched
    , lastUnitByTab : Dict String String
    , randomSeeds : Dict String Int
    , cooldown : Dict String Posix
    , statistics : Statistics
    , achievements : Dict String Posix
    , newAchievements : Maybe ( Posix, Achievement, List Achievement )
    , achievementStatuses : List ( Achievement, Game.Achievement.Status )
    , achievementPoints : Game.Achievement.Points
    , achievementsShown : AchievementsShown
    , mtxHistory : MtxHistory
    }


type CostUI
    = FancyCostUI
    | SimpleCostUI


costUIFromString : String -> CostUI
costUIFromString str =
    case str of
        "fancy" ->
            FancyCostUI

        "simple" ->
            SimpleCostUI

        _ ->
            SimpleCostUI


costUIToString : CostUI -> String
costUIToString c =
    case c of
        FancyCostUI ->
            "fancy"

        SimpleCostUI ->
            "simple"


costUIDecoder : D.Decoder CostUI
costUIDecoder =
    D.string |> D.map costUIFromString


type VelocityUI
    = HiddenVelocityUI
    | FancyVelocityUI


velocityUIFromString : String -> VelocityUI
velocityUIFromString str =
    case str of
        "hidden" ->
            HiddenVelocityUI

        "fancy" ->
            FancyVelocityUI

        _ ->
            HiddenVelocityUI


velocityUIToString : VelocityUI -> String
velocityUIToString c =
    case c of
        FancyVelocityUI ->
            "fancy"

        HiddenVelocityUI ->
            "hidden"


velocityUIDecoder : D.Decoder VelocityUI
velocityUIDecoder =
    D.string |> D.map velocityUIFromString


type AchievementsSortOrder
    = AchievementsSortByDefault
    | AchievementsSortByCompletion


achievementsSortOrderToString : AchievementsSortOrder -> String
achievementsSortOrderToString s =
    case s of
        AchievementsSortByDefault ->
            "default"

        AchievementsSortByCompletion ->
            "default"


achievementsSortOrderByString : Dict String AchievementsSortOrder
achievementsSortOrderByString =
    [ AchievementsSortByDefault
    , AchievementsSortByCompletion
    ]
        |> List.map (\o -> ( achievementsSortOrderToString o, o ))
        |> Dict.fromList


achievementsSortOrderFromString : String -> Maybe AchievementsSortOrder
achievementsSortOrderFromString name =
    Dict.get name achievementsSortOrderByString


type alias AchievementsShown =
    { earned : Bool
    , unearned : Bool
    , masked : Bool
    , order : AchievementsSortOrder
    , reverse : Bool
    }


defaultAchievementsShown : AchievementsShown
defaultAchievementsShown =
    { earned = True
    , unearned = True
    , masked = True
    , order = AchievementsSortByDefault
    , reverse = False
    }



-- CONSTRUCTORS


empty : GameData -> Posix -> Game
empty gameData_ created_ =
    { gameData = gameData_
    , created = created_
    , encoded = created_
    , theme = Theme.Default
    , costUI = SimpleCostUI
    , velocityUI = HiddenVelocityUI
    , units = UnitsSnapshot.create created_ (initUnits gameData_) Dict.empty Set.empty
    , upgrades = Dict.empty
    , spells = Dict.empty
    , polynomials = Dict.empty
    , graph = ProductionGraph.empty
    , locale = Nothing
    , numberFormat = NumberFormat.default
    , velocityUnit = VelocityUnit.default
    , buyN = BuyN.empty
    , watched = Dict.empty
    , lastUnitByTab = Dict.empty
    , randomSeeds = Dict.empty
    , cooldown = Dict.empty
    , statistics = Statistics.empty
    , achievements = Dict.empty
    , achievementPoints = Game.Achievement.emptyPoints
    , newAchievements = Nothing
    , achievementStatuses = []
    , achievementsShown = defaultAchievementsShown
    , mtxHistory = MtxHistory.empty
    }
        |> Game
        |> rebuildGraph



-- initFromDecoder needs a silly number of parameters. It's easy to mess up their order.
-- Usually the typechecker will catch this early and painlessly. It won't catch this if two
-- parameters happen to have the same type, though. This has caused bugs in production!
--
-- The opaque types below make this safer. By forcing parameters to have distinct types,
-- we can rely on the typechecker to enforce parameter order.


type DecodeNow
    = DecodeNow Posix


type DecodeCreated
    = DecodeCreated Posix


type DecodeSnapshotted
    = DecodeSnapshotted Posix


type DecodeEncoded
    = DecodeEncoded Posix


initFromDecoder migration gameData_ (DecodeNow now) (DecodeCreated created_) (DecodeSnapshotted snapshotted_) (DecodeEncoded encoded_) units_ upgrades spells locale_ theme_ numberFormat_ velocityUnit_ buyN_ watched_ lastUnitByTab_ randomSeeds_ cooldown_ statistics_ achievements_ costUI_ velocityUI_ achievementsShown_ mtxHistory_ =
    { gameData = gameData_
    , created = created_
    , encoded = encoded_
    , locale = locale_
    , theme = theme_
    , numberFormat = numberFormat_
    , velocityUnit = velocityUnit_
    , units = UnitsSnapshot.create snapshotted_ units_ Dict.empty Set.empty
    , upgrades = upgrades
    , spells = spells
    , polynomials = Dict.empty
    , graph = ProductionGraph.empty
    , buyN = buyN_
    , watched = watched_
    , lastUnitByTab = lastUnitByTab_
    , randomSeeds = randomSeeds_
    , cooldown = cooldown_
    , statistics = statistics_
    , achievements = achievements_
    , achievementPoints = Game.Achievement.points gameData_ achievements_
    , newAchievements = Nothing
    , achievementStatuses = []
    , costUI = costUI_
    , velocityUI = velocityUI_
    , achievementsShown = achievementsShown_
    , mtxHistory = mtxHistory_
    }
        |> (\g -> { g | achievementStatuses = Game.Achievement.statuses g achievements_ })
        |> Game
        |> migration
        |> rebuildGraph


initUnits : GameData -> Dict String Decimal
initUnits =
    GameData.initUnits
        >> List.map (Tuple.mapFirst Unit.name)
        >> Dict.fromList


{-| Decode a nonempty json value, or start from scratch with an empty one.
-}
load : GameData -> Posix -> Maybe D.Value -> Result D.Error Game
load gameData_ created_ maybeJson =
    case maybeJson of
        Nothing ->
            -- new game, start from scratch
            empty gameData_ created_ |> Ok

        Just json ->
            -- not-new game, decode json
            D.decodeValue (decoder gameData_ created_) json



-- SELECTORS


unwrap : Game -> Data
unwrap (Game g) =
    g


gameData : Game -> GameData
gameData =
    unwrap >> .gameData


locale : Game -> Maybe Locale
locale =
    unwrap >> .locale


numberFormat : Game -> NumberFormat
numberFormat =
    unwrap >> .numberFormat


velocityUnit : Game -> VelocityUnit
velocityUnit =
    unwrap >> .velocityUnit


velocityUnitSeconds : Game -> Result String Float
velocityUnitSeconds game =
    let
        vu =
            velocityUnit game
    in
    case vu of
        VelocityUnit.Second ->
            Ok 1

        VelocityUnit.Minute ->
            Ok 60

        VelocityUnit.Hour ->
            Ok <| 60 * 60

        VelocityUnit.Day ->
            Ok <| 60 * 60 * 24

        VelocityUnit.Swarmwarp ->
            case game |> gameData |> GameData.spellByName "swarmwarp" of
                Nothing ->
                    Err "no spell named swarmwarp"

                Just spell ->
                    case List.head spell.onBuy.addTime of
                        Nothing ->
                            Err "swarmwarp doesn't add time"

                        Just onBuy ->
                            Parser.Stat.evalWithDefault Decimal.zero (getGameVar game) onBuy
                                |> Decimal.toFloat
                                |> Ok


created : Game -> Posix
created =
    unwrap >> .created


encoded : Game -> Posix
encoded =
    unwrap >> .encoded


units : Game -> UnitsSnapshot
units =
    unwrap >> .units


snapshotted : Game -> Posix
snapshotted =
    units >> UnitsSnapshot.at


unitCount : Unit.Id -> Game -> Decimal
unitCount id =
    units >> UnitsSnapshot.count id


upgradeCount : Upgrade.Id -> Game -> Decimal
upgradeCount id =
    upgradeCountName (Upgrade.idToString id)


upgradeCountName : String -> Game -> Decimal
upgradeCountName name =
    unwrap >> .upgrades >> Dict.get name >> Maybe.withDefault Decimal.zero


upgradesDict : Game -> Dict String Decimal
upgradesDict =
    unwrap >> .upgrades


spellCount : Spell.Id -> Game -> Decimal
spellCount id =
    unwrap >> .spells >> Dict.get (Spell.idToString id) >> Maybe.withDefault Decimal.zero


itemCount : Item.Id -> Game -> Decimal
itemCount id =
    case id of
        ItemId.UnitId i ->
            unitCount i

        ItemId.UpgradeId i ->
            upgradeCount i

        ItemId.SpellId i ->
            spellCount i


snapItemCount : Item.Id -> UnitsSnapshot -> Game -> Decimal
snapItemCount id snap game =
    case id of
        ItemId.UnitId i ->
            UnitsSnapshot.count i snap

        _ ->
            itemCount id game


production : Unit -> Game -> UnitsSnapshot -> Maybe UnitsSnapshot.Production
production unit game =
    UnitsSnapshot.production unit (gameData game) (getVar game)


polynomials : Game -> Dict String Polynomial
polynomials =
    unwrap >> .polynomials


graph : Game -> ProductionGraph.Graph
graph =
    unwrap >> .graph


buyN : Item -> Game -> ( String, Result String BuyN.Request )
buyN item =
    unwrap >> .buyN >> BuyN.get item


watched : Item -> Game -> Maybe Watched
watched item =
    if Item.isWatchable item then
        unwrap >> .watched >> Dict.get (Item.name item) >> Maybe.withDefault Watched.Cost1 >> Just

    else
        always Nothing


lastUnitByTab : Tab.Id -> Game -> Route
lastUnitByTab tab (Game g) =
    case g.lastUnitByTab |> Dict.get (Tab.toString tab) of
        Just name ->
            case ( GameData.unitBySlug name g.gameData, GameData.spellBySlug name g.gameData ) of
                ( Just unit, _ ) ->
                    Route.unit (Tab.toString tab) name

                ( _, Just spell ) ->
                    Route.Spell (Tab.toString tab) name

                _ ->
                    Route.Tab (Tab.toString tab)

        _ ->
            Route.Tab (Tab.toString tab)


flipLastUnitByTab : Game -> Tab.Id -> Route
flipLastUnitByTab game id =
    lastUnitByTab id game


mtxHistory : Game -> MtxHistory
mtxHistory =
    unwrap >> .mtxHistory



-- STAT EVALUATION


getVar : Game -> UnitsSnapshot -> String -> Maybe Decimal
getVar game units_ name =
    getVarDicts game units_ name
        |> Maybe.Extra.orElse
            (if String.endsWith "_velocity" name then
                -- ... String.slice (0 -(String.length "_velocity")) ...
                Dict.get (String.slice 0 -9 name) (UnitsSnapshot.velocities units_)

             else if String.endsWith "_spent" name then
                -- ... String.slice (0 -(String.length "_spent")) ...
                spent (String.slice 0 -6 name) game

             else if name == "achievementpoints" then
                (achievementPoints game).val |> Decimal.fromInt |> Just

             else
                Nothing
            )


getVarDicts : Game -> UnitsSnapshot -> String -> Maybe Decimal
getVarDicts ((Game g) as game) units_ name =
    Dict.get name (UnitsSnapshot.toDict units_)
        |> Maybe.Extra.orElse (Dict.get name g.upgrades)
        |> Maybe.Extra.orElse (Dict.get name g.spells)


getGameVar : Game -> String -> Maybe Decimal
getGameVar g =
    getVar g (units g)


{-| How many of the given unit have you ever spent?

beware, this assumes the target unit can only be manually purchased. Don't use
it for something like drones, which can be produced by other units.

-}
spent : String -> Game -> Maybe Decimal
spent name game =
    let
        gd =
            gameData game
    in
    case GameData.unitByName name gd of
        Nothing ->
            Nothing

        Just unit ->
            let
                costs : List ( Item, CostVal )
                costs =
                    GameData.itemsByCost unit.id gd
                        |> List.filter (\( item, _ ) -> isResetOnAscend item)

                evalCostTotal : ( Item, CostVal ) -> Decimal
                evalCostTotal ( parent, cost ) =
                    case cost of
                        Cost.FactorCost _ _ ->
                            -- TODO, but this isn't used at the moment
                            Decimal.zero

                        Cost.FixedCost expr ->
                            Parser.Stat.evalWithDefault Decimal.zero (getVarDicts game (units game)) expr
                                |> Decimal.mul (itemCount (Item.id parent) game)
            in
            case costs of
                [] ->
                    Nothing

                _ ->
                    costs |> List.map evalCostTotal |> Decimal.sum |> Just


isResetOnAscend : Item -> Bool
isResetOnAscend item =
    case item of
        Item.Unit u ->
            not u.ascendPreserve

        _ ->
            True


achievements : Game -> List ( Achievement, Game.Achievement.Status )
achievements =
    unwrap >> .achievementStatuses


achievementsDict : Game -> Dict String Posix
achievementsDict =
    unwrap >> .achievements


achievementPoints : Game -> Game.Achievement.Points
achievementPoints =
    unwrap >> .achievementPoints


newAchievements : Game -> Maybe ( Posix, Achievement, List Achievement )
newAchievements =
    unwrap >> .newAchievements


clearNewAchievements : Game -> Game
clearNewAchievements (Game g) =
    Game { g | newAchievements = Nothing }


fadeNewAchievement : Posix -> Game -> Bool
fadeNewAchievement now ((Game g) as game) =
    case g.newAchievements of
        Nothing ->
            False

        Just ( started, _, _ ) ->
            Time.posixToMillis now >= Time.posixToMillis started + 4000


expireNewAchievements : Posix -> Game -> Game
expireNewAchievements now ((Game g) as game) =
    case g.newAchievements of
        Nothing ->
            game

        Just ( started, first, rest ) ->
            let
                expires =
                    Time.posixToMillis started + 5000
            in
            if Time.posixToMillis now >= expires then
                case rest of
                    [] ->
                        Game { g | newAchievements = Nothing }

                    next :: rest1 ->
                        expireNewAchievements now <| Game { g | newAchievements = Just ( Time.millisToPosix expires, next, rest1 ) }

            else
                game


debugNewAchievement : Posix -> Achievement -> Game -> Game
debugNewAchievement now achievement (Game g) =
    Game
        { g
            | newAchievements =
                case g.newAchievements of
                    Nothing ->
                        Just ( now, achievement, [] )

                    Just ( started, first, rest ) ->
                        Just ( started, first, rest ++ [ achievement ] )
        }


debugAchievementPoints : Game.Achievement.Points -> Game -> Game
debugAchievementPoints points (Game g) =
    Game { g | achievementPoints = points }


achievementsShown : Game -> AchievementsShown
achievementsShown =
    unwrap >> .achievementsShown


mapAchievementsShown : (AchievementsShown -> AchievementsShown) -> Game -> Game
mapAchievementsShown fn (Game g) =
    Game { g | achievementsShown = fn g.achievementsShown }



-- UNITS-SNAPSHOTS


buildGraph : Game -> ProductionGraph.Graph
buildGraph g =
    ProductionGraph.graph (gameData g) (getGameVar g)


buildPolynomials : UnitsSnapshot -> ProductionGraph.Graph -> Dict String Polynomial
buildPolynomials units_ =
    ProductionGraph.polynomials (UnitsSnapshot.toDict units_)


rebuildGraph : Game -> Game
rebuildGraph ((Game g) as game) =
    let
        graph_ =
            buildGraph game

        poly =
            buildPolynomials g.units graph_

        ((Game g1) as game1) =
            Game { g | graph = graph_, polynomials = poly }
    in
    if UnitsSnapshot.velocities g.units == Dict.empty then
        -- When initializing a `Game`, we bootstrap unit velocites with an empty
        -- dictionary because we lacked the graph/polynomials to calculate them.
        -- Now that we've built the graph, fill in velocities too.
        Game { g1 | units = evalUnits (UnitsSnapshot.at g.units) game1 }

    else
        game1


unitCaps : Game -> List ( Unit, Decimal )
unitCaps game =
    game
        |> gameData
        |> GameData.cappedUnits
        |> List.map (Tuple.mapSecond (Parser.Stat.evalWithDefault Decimal.zero <| getGameVar game))


evalUnits : Posix -> Game -> UnitsSnapshot
evalUnits now game =
    evalUnits_ now Duration.zero (units game |> UnitsSnapshot.visibleNames) game


skipTime : Duration -> Game -> UnitsSnapshot
skipTime duration game =
    evalUnits_ (snapshotted game) duration (units game |> UnitsSnapshot.visibleNames) game


clearUnitsVisibility : Game -> UnitsSnapshot
clearUnitsVisibility game =
    evalUnits_ (snapshotted game) Duration.zero Set.empty game


evalUnits_ : Posix -> Duration -> Set String -> Game -> UnitsSnapshot
evalUnits_ now duration visibleNames game =
    UnitsSnapshot.eval (getVar game) (unwrap game).gameData now (units game |> UnitsSnapshot.since now |> Duration.add duration) (unwrap game).polynomials (unitCaps game) visibleNames



-- UPDATES


updateUnits : UnitsSnapshot -> Game -> Game
updateUnits units_ (Game g) =
    { g | units = units_ }
        |> Game
        |> rebuildGraph


updateUpgrades : Dict String Decimal -> Game -> Game
updateUpgrades ups (Game g) =
    Game { g | upgrades = ups }


updateSpells : Dict String Decimal -> Game -> Game
updateSpells spells (Game g) =
    Game { g | spells = spells }


{-| Update the units-snapshot time; when all units are calculated from.

This used to be called `reify`.

-}
snapshotUnits : Posix -> Game -> Game
snapshotUnits now game =
    game |> updateUnits (game |> evalUnits now)


{-| Clear and recalculate visibility for all items.

Normally, once an item is visible, it never becomes invisible. We cache this so
spending currency won't show/hide items repeatedly. Two exceptions - ascension
and respecs - must explicitly clear visibility.

-}
recalculateVisibility : Game -> Game
recalculateVisibility game =
    game |> updateUnits (clearUnitsVisibility game)


{-| Run me after UnitsSnapshot updates to make visibility, etc. catch up
-}
refreshSnapshotUnits : Game -> Game
refreshSnapshotUnits game =
    snapshotUnits (snapshotted game) game


setLocale : Locale -> Game -> Game
setLocale locale_ (Game g) =
    Game { g | locale = Just locale_ }


setNumberFormat : NumberFormat -> Game -> Game
setNumberFormat format (Game g) =
    Game { g | numberFormat = format }


setVelocityUnit : VelocityUnit -> Game -> Game
setVelocityUnit format (Game g) =
    Game { g | velocityUnit = format }


setBuyN : Item -> String -> Game -> Game
setBuyN item val (Game g) =
    Game { g | buyN = g.buyN |> BuyN.insert item val }


addItem : Item -> Decimal -> Game -> Game
addItem item =
    addItemId (Item.id item)


addItemId : Item.Id -> Decimal -> Game -> Game
addItemId id count (Game g) =
    rebuildGraph <|
        case id of
            ItemId.UnitId u ->
                Game { g | units = g.units |> UnitsSnapshot.add u count }

            ItemId.UpgradeId u ->
                Game { g | upgrades = g.upgrades |> DecimalDict.add (Upgrade.idToString u) count }

            ItemId.SpellId u ->
                Game { g | spells = g.spells |> DecimalDict.add (Spell.idToString u) count }


{-| Remove a purchased item. Used with respecs.
-}
removeItemId : Item.Id -> Game -> Game
removeItemId id (Game g) =
    rebuildGraph <|
        case id of
            ItemId.UnitId u ->
                Game { g | units = g.units |> UnitsSnapshot.remove u }

            ItemId.UpgradeId u ->
                Game { g | upgrades = g.upgrades |> Dict.remove (Upgrade.idToString u) }

            ItemId.SpellId u ->
                Game { g | spells = g.spells |> Dict.remove (Spell.idToString u) }


insertWatched : Item -> Watched -> Game -> Game
insertWatched item val ((Game g) as game0) =
    if Item.isWatchable item then
        Game { g | watched = g.watched |> Dict.insert (Item.name item) val }

    else
        game0


setLastUnitByTab : String -> Maybe String -> Game -> Game
setLastUnitByTab tab unit (Game g) =
    let
        update =
            case unit of
                Nothing ->
                    Dict.remove tab

                Just u ->
                    Dict.insert tab u
    in
    Game { g | lastUnitByTab = update g.lastUnitByTab }


{-| Clear last-unit-by-tab and buy-n settings.
-}
clearUI : Game -> Game
clearUI (Game g) =
    Game { g | lastUnitByTab = Dict.empty, buyN = BuyN.empty }


cooldownRemaining : Posix -> OnBuy.Cooldown -> Game -> Duration
cooldownRemaining now cooldown game =
    case game |> unwrap |> .cooldown |> Dict.get cooldown.group of
        Nothing ->
            Duration.zero

        Just at ->
            Duration.sub cooldown.val (Duration.since { before = at, after = now })
                |> Duration.max Duration.zero


isCooldownReady : Posix -> OnBuy.Cooldown -> Game -> Bool
isCooldownReady now cooldown game =
    (cooldownRemaining now cooldown game |> Duration.toMillis) <= 0


popCooldown : OnBuy.Cooldown -> Game -> Maybe Game
popCooldown cooldown ((Game g) as game) =
    let
        now =
            snapshotted game
    in
    if isCooldownReady now cooldown game then
        Just <| Game { g | cooldown = g.cooldown |> Dict.insert cooldown.group now }

    else
        Nothing


statistics : Game -> Statistics
statistics =
    unwrap >> .statistics


pushStatistics : Statistics.Order o -> Game -> Game
pushStatistics order ((Game g) as game) =
    let
        sinceFirst =
            Duration.since { before = g.created, after = snapshotted game }
    in
    { g | statistics = g.statistics |> Statistics.push sinceFirst order }
        |> Game.Achievement.push order (snapshotted game)
        |> Game


grantAchievement : String -> Posix -> Game -> Maybe Game
grantAchievement name now =
    -- achievements affect larvae production, so we must snapshot
    snapshotUnits now
        >> unwrap
        >> Game.Achievement.grant name now
        >> Maybe.map Game


theme : Game -> Theme
theme =
    unwrap >> .theme


setTheme : Theme -> Game -> Game
setTheme theme_ (Game g) =
    Game { g | theme = theme_ }


costUI : Game -> CostUI
costUI =
    unwrap >> .costUI


setCostUI : CostUI -> Game -> Game
setCostUI c (Game g) =
    Game { g | costUI = c }


velocityUI : Game -> VelocityUI
velocityUI =
    unwrap >> .velocityUI


setVelocityUI : VelocityUI -> Game -> Game
setVelocityUI v (Game g) =
    Game { g | velocityUI = v }


{-| `Random.step` tied to an item, updating the game's stored random seed.
-}
randomStep : Item -> Generator a -> Game -> ( a, Game )
randomStep item gen (Game g) =
    let
        key =
            Item.name item

        seed0 : Random.Seed
        seed0 =
            g.randomSeeds
                |> Dict.get key
                -- Initial seeds are based on game creation time and the existing number of seeds.
                -- Random rolls from one unit don't influence others; ex. hatchery-mutagen doesn't affect expansion-mutagen.
                |> Maybe.withDefault (Time.posixToMillis g.created + Dict.size g.randomSeeds)
                |> Random.initialSeed

        ( val, seed1 ) =
            Random.step gen seed0

        -- We must persist a seed to prevent easy savescumming, but Elm can't serialize `Random.seed` -
        -- seed internals are not exposed anywhere, as far as I can tell.
        --
        -- Instead, generate an `Int` to use as the next random seed and persist that instead.
        -- Not sure how cryptographically secure that is, but this is a single-player game, who cares.
        --
        -- Old-swarmsim didn't save seeds, but deterministically built seeds as a string based on
        -- [start-time, ascension-count, unit, count]; its RNG supported string seeds. Elm RNG seeds
        -- require an `Int`. Instead of creating a String -> Int hashing method to emulate the old
        -- way, I chose to persist seeds for each parent - mostly because the implementation's easier.
        ( nextSeed, _ ) =
            Random.step RandomUtil.seedInt seed1
    in
    ( val, Game { g | randomSeeds = g.randomSeeds |> Dict.insert key nextSeed } )


{-| Are two games from different save-sources equal-ish? This is used for detecting save conflicts.
-}
stateDicts (Game g) =
    { units = g.units
    , upgrades = g.upgrades
    , spells = g.spells
    , statistics = g.statistics
    , created = g.created
    }


achievementProgress : Game -> Achievement -> List Parser.Visibility.Progress
achievementProgress (Game g) =
    Game.Achievement.progress g


updatePlayFabMtxHistory : PlayFab.Inventory.Inventory -> Game -> Game
updatePlayFabMtxHistory serverItems ((Game g) as game) =
    updateMtxHistory
        (MtxHistory.updatePlayfabInventory g.gameData serverItems g.mtxHistory)
        game


updateKongMtxHistory : Kongregate.Inventory.Inventory -> Game -> Game
updateKongMtxHistory serverItems ((Game g) as game) =
    updateMtxHistory
        (MtxHistory.updateKongInventory g.gameData serverItems g.mtxHistory)
        game


updateMtxHistory : ( Int, MtxHistory ) -> Game -> Game
updateMtxHistory ( crystals, hist ) ((Game g) as game) =
    if crystals <= 0 then
        game

    else
        Game { g | mtxHistory = hist }
            -- We can skip `evalUnits` before updating units here, because nothing
            -- adds crystals per second. Careful about copying this elsewhere,
            -- where things may have velocities!
            --
            -- We still need to do bookkeeping afterward in `updateUnits`.
            -- `{g|units=...}` breaks because polynomials also evaluate constants.
            |> updateUnits (g.units |> UnitsSnapshot.addName "crystal" (Decimal.fromInt crystals))



-- SERIALIZATION


encoder : Posix -> Game -> E.Value
encoder now (Game g) =
    E.object
        [ ( "persistVersion", 1 |> E.int )
        , ( "created", g.created |> JsonUtil.posixEncoder )
        , ( "snapshotted", g.units |> UnitsSnapshot.at |> JsonUtil.posixEncoder )
        , ( "encoded", now |> JsonUtil.posixEncoder )
        , ( "units", g.units |> UnitsSnapshot.toDict |> E.dict identity JsonUtil.decimalEncoder )
        , ( "upgrades", g.upgrades |> E.dict identity JsonUtil.decimalEncoder )
        , ( "spells", g.spells |> E.dict identity JsonUtil.decimalEncoder )
        , ( "locale", g.locale |> Maybe.Extra.unwrap E.null Locale.encoder )
        , ( "theme", g.theme |> Theme.encoder )
        , ( "numberFormat", g.numberFormat |> NumberFormat.encoder )
        , ( "velocityUnit", g.velocityUnit |> VelocityUnit.encoder )
        , ( "buyN", g.buyN |> BuyN.encoder )
        , ( "watched", g.watched |> E.dict identity Watched.encoder )
        , ( "lastUnitByTab", g.lastUnitByTab |> E.dict identity E.string )
        , ( "randomSeeds", g.randomSeeds |> E.dict identity E.int )
        , ( "cooldown", g.cooldown |> E.dict identity JsonUtil.posixEncoder )
        , ( "statistics", g.statistics |> Statistics.encoder )
        , ( "achievements", g.achievements |> Game.Achievement.encoder )
        , ( "costUI", g.costUI |> costUIToString |> E.string )
        , ( "velocityUI", g.velocityUI |> velocityUIToString |> E.string )
        , ( "achievementsShown", g.achievementsShown |> achievementsShownEncoder )
        , ( "mtxHistory", g.mtxHistory |> MtxHistory.encoder )
        ]


decoder : GameData -> Posix -> D.Decoder Game
decoder gameData_ now =
    let
        versions =
            Array.fromList [ decoder0, decoder1 ]

        selectVersion ver =
            Array.get ver versions
                |> Maybe.Extra.unwrap (D.fail "invalid persistVersion") (\f -> f gameData_ now)
    in
    D.succeed selectVersion
        |> P.optional "persistVersion" D.int 0
        |> D.andThen identity


{-| Decode modern Swarmsim saves. Inverse of the above `encoder`.
-}
decoder1 : GameData -> Posix -> D.Decoder Game
decoder1 gameData_ now =
    D.succeed (initFromDecoder identity gameData_ (DecodeNow now))
        |> P.required "created" (JsonUtil.posixDecoder |> D.map DecodeCreated)
        |> P.required "snapshotted" (JsonUtil.posixDecoder |> D.map DecodeSnapshotted)
        |> P.custom
            (D.oneOf
                [ D.field "encoded" JsonUtil.posixDecoder
                , D.field "snapshotted" JsonUtil.posixDecoder
                ]
                |> D.map DecodeEncoded
            )
        |> P.required "units" (D.dict JsonUtil.decimalDecoder)
        |> P.required "upgrades" (D.dict JsonUtil.decimalDecoder)
        |> P.required "spells" (D.dict JsonUtil.decimalDecoder)
        |> P.required "locale" (D.nullable Locale.decoder)
        |> P.optional "theme" Theme.decoder Theme.Default
        |> P.optional "numberFormat" NumberFormat.decoder NumberFormat.default
        |> P.optional "velocityUnit" VelocityUnit.decoder VelocityUnit.default
        |> P.required "buyN" BuyN.decoder
        |> P.required "watched" (D.dict Watched.decoder)
        |> P.optional "lastUnitByTab" (D.dict D.string) Dict.empty
        |> P.required "randomSeeds" (D.dict D.int)
        |> P.required "cooldown" (D.dict JsonUtil.posixDecoder)
        |> P.required "statistics" Statistics.decoder
        |> P.custom Game.Achievement.decoder
        |> P.optional "costUI" costUIDecoder SimpleCostUI
        |> P.optional "velocityUI" velocityUIDecoder HiddenVelocityUI
        |> P.optional "achievementsShown" achievementsShownDecoder defaultAchievementsShown
        |> P.optional "mtxHistory" MtxHistory.decoder MtxHistory.empty


{-| Decode Swarmsim saves from the old Coffeescript version of the game.
-}
decoder0 : GameData -> Posix -> D.Decoder Game
decoder0 gameData_ now =
    D.succeed (initFromDecoder migrateFromV0 gameData_ (DecodeNow now))
        |> P.requiredAt [ "date", "started" ] (JsonUtil.posixDecoder |> D.map DecodeCreated)
        |> P.requiredAt [ "date", "reified" ] (JsonUtil.posixDecoder |> D.map DecodeSnapshotted)
        |> P.requiredAt [ "date", "reified" ] (JsonUtil.posixDecoder |> D.map DecodeEncoded)
        |> P.required "unittypes" (D.dict JsonUtil.decimalDecoder)
        |> P.required "upgrades" (D.dict JsonUtil.decimalDecoder)
        |> P.hardcoded Dict.empty
        |> P.hardcoded Nothing
        |> P.hardcoded Theme.Default
        |> P.optionalAt [ "options", "notation" ] NumberFormat.decoder NumberFormat.default
        |> P.optionalAt [ "options", "velocityUnit" ] VelocityUnit.decoder VelocityUnit.default
        |> P.hardcoded BuyN.empty
        |> P.optional "watched" (D.dict Watched.decoder) Dict.empty
        |> P.hardcoded Dict.empty
        -- can't preserve random seeds from another language. No big deal.
        |> P.hardcoded Dict.empty
        -- old-swarmsim had cooldowns, but their serialization sucked.
        -- they're not worth the trouble to decode here.
        -- result: people get a free one-time crystal cooldown, no worries
        |> P.hardcoded Dict.empty
        |> P.required "statistics" Statistics.decoder
        |> P.custom Game.Achievement.decoder
        |> P.optional "costUI" costUIDecoder SimpleCostUI
        |> P.optional "velocityUI" velocityUIDecoder HiddenVelocityUI
        |> P.optional "achievementsShown" achievementsShownDecoder defaultAchievementsShown
        |> P.optional "mtx" MtxHistory.coffeeDecoder MtxHistory.empty


achievementsShownEncoder : AchievementsShown -> E.Value
achievementsShownEncoder a =
    E.object
        [ ( "earned", a.earned |> E.bool )
        , ( "unearned", a.unearned |> E.bool )
        , ( "masked", a.masked |> E.bool )
        , ( "order", a.order |> achievementsSortOrderToString |> E.string )
        , ( "reverse", a.reverse |> E.bool )
        ]


achievementsShownDecoder : D.Decoder AchievementsShown
achievementsShownDecoder =
    D.map5 AchievementsShown
        (D.field "earned" D.bool)
        (D.field "unearned" D.bool)
        (D.field "masked" D.bool)
        (D.field "order" (D.string |> D.map (achievementsSortOrderFromString >> Maybe.withDefault AchievementsSortByDefault)))
        (D.field "reverse" D.bool)


migrateFromV0 : Game -> Game
migrateFromV0 (Game g) =
    let
        spellCounts =
            g.upgrades |> Dict.filter (\name _ -> GameData.spellByName name g.gameData |> Maybe.Extra.isJust)
    in
    Game
        { g
            | units =
                g.units
                    |> UnitsSnapshot.mapCounts
                        (Dict.remove "invisiblehatchery"
                            >> Dict.insert "hatchery" (Decimal.add Decimal.one <| Maybe.withDefault Decimal.zero <| Dict.get "hatchery" g.upgrades)
                        )
            , upgrades = g.upgrades |> Dict.remove "hatchery" |> (\d -> Dict.diff d spellCounts)
            , spells = spellCounts
        }


persistWrite : Posix -> Game -> ( Game, Cmd msg )
persistWrite now game =
    ( game, game |> encoder now |> Ports.persistWrite )
