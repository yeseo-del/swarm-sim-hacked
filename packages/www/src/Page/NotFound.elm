module Page.NotFound exposing (view)

import Game exposing (Game)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)
import View.Lang as T


view : Session -> Game -> List (Html msg)
view session game =
    let
        lang =
            T.gameToLang session game
    in
    [ lang.html T.ViewNotFound ]
