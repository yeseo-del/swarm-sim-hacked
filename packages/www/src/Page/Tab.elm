module Page.Tab exposing (Model, Msg(..), create, toSession, update, view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Item.Id as ItemId
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Select
import Session exposing (Session)
import View.Icon
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav


type Model
    = Model String Session


create : String -> Session -> ( Model, Cmd Msg )
create tab session0 =
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                ( game, cmd ) =
                    game0 |> Game.setLastUnitByTab tab Nothing |> Session.persistWrite session0
            in
            ( Model tab (Session.setGame False game session0), cmd )

        _ ->
            ( Model tab session0, Cmd.none )


toSession : Model -> Session
toSession (Model _ s) =
    s


type Msg
    = GotTabNavMsg View.TabNav.Msg
    | GotSession Session
    | ClickedRow Route


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model tabSlug session) as model) =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session
                |> Tuple.mapFirst (Model tabSlug)

        GotSession s ->
            ( Model tabSlug s, Cmd.none )

        ClickedRow route ->
            ( model, Route.pushUrl (Session.nav session) (Session.query session) route )


view : Model -> Game -> List (Html Msg)
view ((Model tabSlug session) as model) game =
    View.NotReady.viewResult session (Game.gameData game) <|
        let
            snap =
                Session.safeUnits game session
        in
        case Select.tabBySlug tabSlug game snap of
            Just stab ->
                Ok <|
                    (View.TabNav.viewFromGame Nothing session game
                        |> List.map (H.map GotTabNavMsg)
                    )
                        ++ viewReady session game snap stab

            Nothing ->
                Err "no such tab"


viewReady : Session -> Game -> UnitsSnapshot -> Select.STab -> List (Html Msg)
viewReady session game snapshot stab =
    let
        lang =
            T.gameToLang session game

        units =
            Select.unitCountsByTab stab game snapshot

        spells =
            Select.spellsByTab stab game snapshot

        achieves =
            case stab of
                Select.AllTab ->
                    True

                Select.NormalTab tab ->
                    tab.showAchievements
    in
    [ table [ class "table sidebar tab", class "table-hover" ]
        [ tbody []
            ((if achieves && (Game.achievementPoints game).val > 0 then
                [ viewAchievementsEntry session game ]

              else
                []
             )
                ++ (spells |> List.map (viewSpellEntry stab session game snapshot))
                ++ (units |> List.map (viewUnitEntry stab session game snapshot))
            )
        ]
    ]



-- view*Entry are copied from/based on UnitSidebar. There are enough differences
-- that copying instead of parameterizing is worth it.


viewAchievementsEntry : Session -> Game -> Html Msg
viewAchievementsEntry session game =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        points =
            Game.achievementPoints game

        bonus : Float
        bonus =
            Game.gameData game
                |> GameData.unitByName "hatchery"
                |> Maybe.andThen .prod
                |> Maybe.andThen
                    (\( _, prod ) ->
                        Parser.Stat.evalFullWithDefault Decimal.zero (Game.getGameVar game) prod
                            |> .exports
                            |> Dict.get "achievementbonus"
                    )
                |> Maybe.map Decimal.toFloat
                |> Maybe.withDefault 1
    in
    tr [ onClick <| ClickedRow Route.Achievements ]
        [ td [ class "upgradable-icon" ] []
        , td [ class "item-name" ]
            [ a [ Route.href query Route.Achievements ]
                [ span [ class "list-icon-resource icon-achievements" ] []
                , lang.html <| T.ViewUnitSidebarAchievementPoints
                ]
            ]
        , td [ class "count" ] [ lang.html <| T.ViewUnitSidebarCount <| Decimal.fromInt points.val ]
        , td [ class "velocity" ] [ T.text "+", text <| String.fromInt <| floor <| 100 * (bonus - 1), T.text "% larvae" ]
        ]


viewSpellEntry : Select.STab -> Session -> Game -> UnitsSnapshot -> Spell -> Html Msg
viewSpellEntry stab session game snapshot spell =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab
    in
    -- sadly, we can't make an actual `<a href...>` cover the whole table row
    tr
        [ onClick <| ClickedRow <| Route.Spell tabSlug spell.slug ]
        [ td [ class "upgradable-icon" ] [{- no upgrade indicators for spells -}]
        , td [ class "item-name" ]
            [ a [ Route.href query <| Route.Spell tabSlug spell.slug ]
                [ span [ class "list-icon-resource", class <| "icon-" ++ Spell.name spell ] []
                , lang.html <| T.ViewUnitSidebarSpellName spell.id
                , text " "
                ]
            ]
        , td [ class "count" ] [{- no visible count for spells -}]
        , td [ class "velocity" ] [{- no visible count for spells -}]
        ]


viewUnitEntry : Select.STab -> Session -> Game -> UnitsSnapshot -> ( Unit, Decimal ) -> Html Msg
viewUnitEntry stab session game snapshot ( unit, count ) =
    let
        isWatchTriggered =
            Select.isUnitSidebarWatchTriggered unit game snapshot

        isAscendTriggered =
            Select.isUnitSidebarAscendTriggered unit game snapshot

        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab

        velocity : Decimal
        velocity =
            UnitsSnapshot.velocity unit.id snapshot

        production : Decimal
        production =
            UnitsSnapshot.production unit (Game.gameData game) (Game.getVar game) snapshot
                |> Maybe.map .total
                |> Maybe.withDefault Decimal.zero
    in
    -- sadly, we can't make an actual `<a href...>` cover the whole table row
    tr
        [ onClick <| ClickedRow <| Route.unit tabSlug unit.slug ]
        [ td [ class "upgradable-icon" ]
            ((if isWatchTriggered then
                [ View.Icon.upgradable ]

              else
                []
             )
                ++ (if isAscendTriggered then
                        [ View.Icon.fas "rocket" ]

                    else
                        []
                   )
            )
        , td [ class "item-name" ]
            [ a [ Route.href query <| Route.unit tabSlug unit.slug ]
                [ span [ class "list-icon-resource", class <| "icon-" ++ Unit.name unit ] []
                , lang.html <| T.ViewUnitSidebarUnitName unit.id
                , text " "
                ]
            ]
        , td [ class "count" ] [ lang.html <| T.ViewUnitSidebarCount count ]
        , td [ class "velocity" ]
            (if velocity /= Decimal.zero then
                [ lang.html <| T.ViewTabVelocity unit velocity ]
                -- special exception for territory units

             else if Tab.idToString unit.tab == "territory" && production /= Decimal.zero then
                [ lang.html <| T.ViewTabVelocity unit production ]

             else
                []
            )
        ]
