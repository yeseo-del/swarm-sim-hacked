module Page.Changelog exposing (Msg, create, update, view)

import Game exposing (Game)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Markdown
import RemoteData exposing (RemoteData)
import Session exposing (Session)
import View.Icon
import View.Lang as T
import View.NotReady
import View.TabNav


type Msg
    = GotTabNavMsg View.TabNav.Msg


create : Session -> ( Session, Cmd msg )
create =
    Session.grantAchievement "changelog"


update : Msg -> Session -> ( Session, Cmd msg )
update msg session =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session


view : Session -> Game -> List (Html Msg)
view session game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewReady session game


viewReady : Session -> Game -> List (Html msg)
viewReady session game =
    let
        lang =
            T.gameToLang session game
    in
    [ h1 [] [ lang.html T.ViewChangelogTitle ]
    , p []
        [ a [ target "_blank", rel "noopener", href "https://gitlab.com/erosson/swarm-elm" ]
            [ View.Icon.fas "code", text " ", lang.html T.ViewChangelogSource ]
        ]
    , p [] [ lang.html T.ViewChangelogScarybee ]
    , hr [] []
    , Session.changelog session |> Markdown.toHtml []
    ]
