module Page.Register exposing (Model, Msg(..), create, toSession, update, view)

import Browser
import Dict exposing (Dict)
import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import PlayFab
import PlayFab.Error
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import View.FormUtil as FormUtil
import View.Icon
import View.Lang as T


type Model
    = Model
        { session : Session
        , email : String
        , password : String
        , password2 : String
        , remember : Bool
        , response : RemoteData PlayFab.Error.Error PlayFab.User
        }


create : Session -> Model
create session_ =
    Model
        { session = session_
        , email = ""
        , password = ""
        , password2 = ""
        , remember = False
        , response = RemoteData.NotAsked
        }


toSession : Model -> Session
toSession (Model m) =
    m.session



-- VIEW


view : Model -> GameData -> List (Html Msg)
view (Model model) gd =
    let
        lang =
            T.gameDataToLang model.session gd

        error =
            FormUtil.failure model.response

        qs =
            Session.query model.session
    in
    [ h3 [] [ lang.html T.ViewRegisterTitle ]
    , H.form [ onSubmit Submit ]
        [ div [ class "form-group" ]
            (label [ for "playfab-login-email" ] [ lang.html T.ViewRegisterFieldEmail ]
                :: FormUtil.field (PlayFab.errorField error "Email")
                    input
                    [ type_ "email", id "playfab-login-email", class "form-control", value model.email, onInput InputEmail ]
                    []
            )
        , div [ class "form-group" ]
            (label [ for "playfab-login-password" ] [ lang.html T.ViewRegisterFieldPassword ]
                :: FormUtil.field (PlayFab.errorField error "Password")
                    input
                    [ type_ "password", id "playfab-login-password", class "form-control", value model.password, onInput InputPassword ]
                    []
            )
        , div [ class "form-group" ]
            (label [ for "playfab-login-password2" ] [ lang.html T.ViewRegisterFieldPassword2 ]
                :: FormUtil.field (PlayFab.errorField error "Password2")
                    input
                    [ type_ "password", id "playfab-login-password2", class "form-control", value model.password2, onInput InputPassword2 ]
                    []
            )
        , div [ class "form-group form-check" ]
            [ input [ type_ "checkbox", id "playfab-login-remember", class "form-check-input", checked model.remember, onCheck InputRemember ] []
            , label [ for "playfab-login-remember", class "form-check-label" ] [ lang.html T.ViewRegisterFieldRememberMe ]
            ]
        , PlayFab.Error.toMessages error |> FormUtil.error
        , if RemoteData.isLoading model.response then
            button [ type_ "submit", class "btn btn-primary disabled", disabled True ] [ View.Icon.loading, text " ", lang.html T.ViewRegisterSubmit ]

          else
            button [ type_ "submit", class "btn btn-primary" ] [ lang.html T.ViewRegisterSubmit ]
        ]
    , div [] [ a [ class "btn btn-link", Route.href qs Route.Login ] [ lang.html T.ViewRegisterLinkLogin ] ]
    ]



-- UPDATE


type Msg
    = GotSession Session
    | Submit
    | InputEmail String
    | InputPassword String
    | InputPassword2 String
    | InputRemember Bool
    | RegisterResponse (Result PlayFab.Error.Error PlayFab.User)
    | RememberMeResponse (Result PlayFab.Error.Error PlayFab.User)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model model) as model0) =
    case msg of
        GotSession s ->
            ( Model { model | session = s }, Cmd.none )

        InputEmail val ->
            ( Model { model | email = val }, Cmd.none )

        InputPassword val ->
            ( Model { model | password = val }, Cmd.none )

        InputPassword2 val ->
            ( Model { model | password2 = val }, Cmd.none )

        InputRemember val ->
            ( Model { model | remember = val }, Cmd.none )

        Submit ->
            if model.password == model.password2 then
                ( Model { model | response = RemoteData.Loading }
                , PlayFab.emailRegister (PlayFab.TitleId.fromSession model.session) model RegisterResponse
                )

            else
                let
                    res =
                        { error = ""
                        , fields = Dict.singleton "Password2" [ "Passwords do not match." ]
                        }
                            |> PlayFab.Error.FormError
                            |> RemoteData.Failure
                in
                ( Model { model | response = res }, Cmd.none )

        RegisterResponse res ->
            let
                model1 =
                    { model | response = res |> RemoteData.fromResult }
            in
            case res of
                Ok u0 ->
                    let
                        ( user, cmd0 ) =
                            User.decodeGameReq u0
                    in
                    ( Model { model1 | session = model.session |> Session.setUser (RemoteData.Success <| Just user) }
                    , Cmd.batch
                        [ cmd0
                        , Ports.writeUserSessionTicket <| Just <| User.ticketToString <| User.sessionTicket user
                        , if model.remember then
                            PlayFab.setRememberId (PlayFab.TitleId.fromSession model.session) user RememberMeResponse

                          else
                            Route.pushUrl (Session.nav model.session) (Session.query model.session) Route.Home
                        ]
                    )

                _ ->
                    ( Model model1, Cmd.none )

        RememberMeResponse res ->
            case res of
                Ok user ->
                    ( Model { model | session = model.session |> Session.setRawUser user }
                    , Cmd.batch
                        [ Ports.writeUserRememberId <| Maybe.map User.rememberIdToFullString <| User.rememberId user
                        , Route.pushUrl (Session.nav model.session) (Session.query model.session) Route.Home
                        ]
                    )

                _ ->
                    ( model0, Cmd.none )
