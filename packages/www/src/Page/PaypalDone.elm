module Page.PaypalDone exposing (Model, Msg(..), create, toSession, update, view)

{-| Wait for the game to load, then redirect to the correct mtxshop url.

This is where paypal redirects after a purchase. The game needs some time to
load after that, so we use this page instead of a quick redirect in Main.

-}

import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Html exposing (Html)
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import View.Icon


type Model
    = Model Session (Maybe Route.PaypalReturnArgs)


type Msg
    = GotSession Session


create : Session -> String -> Maybe Route.PaypalReturnArgs -> ( Model, Cmd Msg )
create session status args =
    Model session args
        |> update (GotSession session)


toSession : Model -> Session
toSession (Model session _) =
    session


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model session0 args) as model0) =
    case msg of
        GotSession session ->
            ( Model session args
            , case Session.game session of
                RemoteData.Success game ->
                    -- We're loaded! Time to redirect
                    let
                        tab =
                            if game |> Game.units |> UnitsSnapshot.isNameVisible "energy" then
                                "energy"

                            else
                                "all"
                    in
                    Route.pushUrl (Session.nav session) (Session.query session) (Route.MtxShop tab args)

                _ ->
                    Cmd.none
            )


view : List (Html msg)
view =
    [ View.Icon.loading ]
