module Page.Statistics exposing (Msg, update, view)

import Dict exposing (Dict)
import Game exposing (Game)
import Game.Statistics as Statistics exposing (Statistics)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Item.Id as ItemId
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav


type Msg
    = GotTabNavMsg View.TabNav.Msg


update : Msg -> Session -> ( Session, Cmd msg )
update msg session =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session


view : Session -> Game -> List (Html Msg)
view session game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewReady session game


viewReady : Session -> Game -> List (Html msg)
viewReady session game =
    let
        lang =
            T.gameToLang session game

        statistics =
            Game.statistics game

        gd =
            Game.gameData game
    in
    [ h1 [] [ lang.html T.ViewStatisticsTitle ]
    , table [ class "table statistics" ]
        [ thead []
            [ tr []
                [ th [] [ lang.html T.ViewStatisticsHeaderName ]
                , th [] [ lang.html T.ViewStatisticsHeaderEntryCreated ]
                , th [] [ lang.html T.ViewStatisticsHeaderClicks ]
                , th [] [ lang.html T.ViewStatisticsHeaderNum ]
                , th [] [ lang.html T.ViewStatisticsHeaderTwinNum ]
                ]
            ]
        , tbody []
            (List.filterMap (viewUnit lang statistics) (GameData.units gd)
                ++ List.filterMap (viewUpgrade lang statistics) (GameData.upgrades gd)
                ++ List.filterMap (viewSpell lang statistics) (GameData.spells gd)
            )
        ]
    , dl [ class "row" ]
        [ dt [ class "col-3" ] [ lang.html T.ViewStatisticsHeaderSaveSize ]
        , dd [ class "col-9" ] [ lang.html <| T.ViewStatisticsSaveSize <| Maybe.map String.length <| Session.persistExport session ]
        , dt [ class "col-3" ] [ lang.html T.ViewStatisticsHeaderCreated ]
        , dd [ class "col-9" ] [ lang.html <| T.ViewStatisticsCreated { created = Game.created game, now = Session.safeNow game session } ]
        ]
    ]


viewUnit : Lang msg -> Statistics -> Unit -> Maybe (Html msg)
viewUnit lang statistics unit =
    let
        v entry =
            tr []
                [ td [ class "name" ] [ lang.html <| T.ViewStatisticsItemName <| ItemId.UnitId unit.id ]
                , td [] [ lang.html <| T.ViewStatisticsEntryCreated entry.elapsedFirst ]
                , td [] [ lang.html <| T.ViewStatisticsClicks entry.clicks ]
                , td [] [ lang.html <| T.ViewStatisticsDecimal entry.num ]
                , td [] [ lang.html <| T.ViewStatisticsDecimal entry.twinnum ]
                ]
    in
    Maybe.map v (Dict.get (Unit.name unit) statistics.byUnit)


viewUpgrade : Lang msg -> Statistics -> Upgrade -> Maybe (Html msg)
viewUpgrade lang statistics upgrade =
    let
        v entry =
            tr []
                [ td [ class "name" ] [ lang.html <| T.ViewStatisticsItemName <| ItemId.UpgradeId upgrade.id ]
                , td [] [ lang.html <| T.ViewStatisticsEntryCreated entry.elapsedFirst ]
                , td [] [ lang.html <| T.ViewStatisticsClicks entry.clicks ]
                , td [] [ lang.html <| T.ViewStatisticsDecimal entry.num ]
                , td [] [ text "-" ]
                ]
    in
    Maybe.map v (Dict.get (Upgrade.name upgrade) statistics.byUpgrade)


viewSpell : Lang msg -> Statistics -> Spell -> Maybe (Html msg)
viewSpell lang statistics spell =
    let
        v entry =
            tr []
                [ td [ class "name" ] [ lang.html <| T.ViewStatisticsItemName <| ItemId.SpellId spell.id ]
                , td [] [ lang.html <| T.ViewStatisticsEntryCreated entry.elapsedFirst ]
                , td [] [ lang.html <| T.ViewStatisticsClicks entry.clicks ]
                , td [] [ lang.html <| T.ViewStatisticsDecimal entry.num ]
                , td [] [ text "-" ]
                ]
    in
    Maybe.map v (Dict.get (Spell.name spell) statistics.byUpgrade)
