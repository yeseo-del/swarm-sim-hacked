module JsonUtil exposing (decimalDecoder, decimalEncoder, decodeMaybe, decodeResult, posixDecoder, posixEncoder)

import Decimal exposing (Decimal)
import Iso8601
import Json.Decode as D
import Json.Encode as E
import Time exposing (Posix)


decimalEncoder : Decimal -> E.Value
decimalEncoder =
    Decimal.toString >> E.string


decimalDecoder : D.Decoder Decimal
decimalDecoder =
    let
        decode s =
            case Decimal.fromString s of
                Just d ->
                    D.succeed d

                Nothing ->
                    D.fail <| "invalid decimal-string: " ++ s
    in
    D.string |> D.andThen decode


posixEncoder : Posix -> E.Value
posixEncoder =
    Time.posixToMillis >> E.int


posixDecoder : D.Decoder Posix
posixDecoder =
    D.oneOf
        -- Our modern/preferred time format - a UTC integer.
        [ D.int |> D.map Time.millisToPosix

        -- old-swarmsim's time format - an ISO8601 timestamp string.
        , Iso8601.decoder
        ]


decodeResult : Result String a -> D.Decoder a
decodeResult res =
    case res of
        Ok val ->
            D.succeed val

        Err err ->
            D.fail err


decodeMaybe : String -> Maybe a -> D.Decoder a
decodeMaybe err =
    Result.fromMaybe err >> decodeResult
