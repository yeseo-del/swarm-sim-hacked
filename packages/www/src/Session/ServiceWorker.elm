module Session.ServiceWorker exposing (Event(..), decoder)

import Json.Decode as D


type Event
    = UpdateFound
    | OfflineInstalled
    | UpdateInstalled


decoder : D.Decoder Event
decoder =
    let
        -- Decoding events sent by registerServiceWorker.js
        builder status =
            case status of
                "update-found" ->
                    D.succeed UpdateFound

                "update-installed" ->
                    D.succeed UpdateInstalled

                "offline-installed" ->
                    D.succeed OfflineInstalled

                "error" ->
                    D.field "error" D.string
                        |> D.andThen (D.fail << (++) "service worker error: ")

                _ ->
                    D.fail <| "service worker invalid status: " ++ status
    in
    D.field "status" D.string |> D.andThen builder
