module Session.PaypalEnv exposing (PaypalEnv(..), fromQuery, toString, url)

import GameData.Mtx as Mtx exposing (Mtx)
import Json.Encode as E
import PlayFab.User as User exposing (User)
import Route.Config as Config exposing (Config)
import Route.QueryDict as QueryDict exposing (QueryDict)
import Session.Environment as Environment exposing (Environment)
import Session.Iframe as Iframe exposing (Iframe)
import Url.Builder


type PaypalEnv
    = Production
    | ElmBeta
    | Sandbox
    | None


toString : PaypalEnv -> String
toString env =
    case env of
        Production ->
            "production"

        ElmBeta ->
            "elm-beta"

        Sandbox ->
            "sandbox"

        None ->
            "none"


url : PaypalEnv -> User.Id -> Mtx -> String
url paypalEnv userId =
    .paypalUrl
        >> (case paypalEnv of
                Production ->
                    .prod

                ElmBeta ->
                    .elmBeta

                Sandbox ->
                    .sandbox

                None ->
                    -- this is financially safe, and easier than jumping through hoops with Maybe types
                    .sandbox
           )
        >> (\u -> u ++ urlSuffix userId)


urlSuffix : User.Id -> String
urlSuffix userId =
    let
        custom =
            [ ( "playfabId", userId |> User.idToString |> E.string ) ]
                |> E.object
                |> E.encode 0
    in
    [ Url.Builder.string "custom" custom ]
        |> Url.Builder.toQuery
        |> String.replace "?" "&"


fromQuery : Environment -> Iframe -> String -> QueryDict -> PaypalEnv
fromQuery env iframe hostname query =
    case Config.get Config.PaypalEnv query of
        Just "production" ->
            Production

        Just "sandbox" ->
            Sandbox

        Just "elm-beta" ->
            ElmBeta

        Just "none" ->
            None

        _ ->
            case iframe of
                Iframe.Kongregate _ ->
                    None

                Iframe.WWW ->
                    case env of
                        Environment.Production ->
                            case hostname of
                                "elm.swarmsim.com" ->
                                    ElmBeta

                                "www.swarmsim.com" ->
                                    Production

                                "swarmsim.com" ->
                                    Production

                                _ ->
                                    Production

                        Environment.Development ->
                            Sandbox

                        Environment.Unknown _ ->
                            Sandbox
