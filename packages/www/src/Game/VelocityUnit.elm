module Game.VelocityUnit exposing (VelocityUnit(..), decoder, default, encoder, list, toString)

import Dict exposing (Dict)
import Duration exposing (Duration)
import Json.Decode as D
import Json.Encode as E


type VelocityUnit
    = Second
    | Minute
    | Hour
    | Day
    | Swarmwarp


default : VelocityUnit
default =
    Second


list : List VelocityUnit
list =
    [ Second
    , Minute
    , Hour
    , Day
    , Swarmwarp
    ]


toString : VelocityUnit -> String
toString format =
    -- these names are compatible with old-swarmsim.
    case format of
        Second ->
            "sec"

        Minute ->
            "min"

        Hour ->
            "hr"

        Day ->
            "day"

        Swarmwarp ->
            "warp"


byString : Dict String VelocityUnit
byString =
    list |> List.map (\f -> ( toString f, f )) |> Dict.fromList


encoder : VelocityUnit -> E.Value
encoder =
    toString >> E.string


decoder : D.Decoder VelocityUnit
decoder =
    D.string
        |> D.andThen
            (\name ->
                case Dict.get name byString of
                    Just format ->
                        D.succeed format

                    Nothing ->
                        D.fail <| "no such velocityUnit: " ++ name
            )
