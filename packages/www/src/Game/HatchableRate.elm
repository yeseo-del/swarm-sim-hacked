module Game.HatchableRate exposing (Output, hatchableRate)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.Order exposing (twinUnits)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData.Cost as Cost exposing (Cost, CostVal(..))
import GameData.Unit as Unit exposing (Unit)
import Parser.Stat


type alias Percent =
    Float


type alias Output =
    { rate : Decimal
    , percentPerCost : List ( Unit.Id, Percent )
    }


{-| The math behind the velocity details text.

"You can hatch 420 drones per second using 69% of your meat income and 100% of your larvae income."

-}
hatchableRate : Unit -> Game -> UnitsSnapshot -> Result String Output
hatchableRate unit game snapshot =
    if List.isEmpty unit.cost then
        Err "unit is not buyable"

    else
        let
            twins =
                twinUnits game snapshot unit
        in
        fixedCostExprs unit
            |> Result.map (List.map (Tuple.mapSecond (eval game snapshot)))
            |> Result.map (hatchableRateFromFixedCosts twins snapshot)
            |> Result.andThen
                (\out ->
                    if out.rate == Decimal.zero then
                        Err "rate is zero"

                    else
                        Ok out
                )


hatchableRateFromFixedCosts : Decimal -> UnitsSnapshot -> List ( Unit.Id, Decimal ) -> Output
hatchableRateFromFixedCosts twins snapshot costs =
    let
        rateFromCost ( costId, cost ) =
            ( costId, Decimal.div (UnitsSnapshot.velocity costId snapshot) cost )

        rates : List ( Unit.Id, Decimal )
        rates =
            costs |> List.map rateFromCost

        minRate : Decimal
        minRate =
            rates
                |> List.map Tuple.second
                |> Decimal.minimum
                |> Maybe.withDefault Decimal.zero
    in
    { rate = minRate |> Decimal.mul twins
    , percentPerCost = rates |> List.map (Tuple.mapSecond (Decimal.div minRate >> Decimal.toFloat))
    }


fixedCostExprs : Unit -> Result String (List ( Unit.Id, Parser.Stat.Expr ))
fixedCostExprs unit =
    let
        fold ( costId, costVal ) =
            Result.andThen
                (\costs ->
                    case costVal of
                        Cost.FixedCost expr ->
                            Ok <| ( costId, expr ) :: costs

                        _ ->
                            Err <| "non-fixed cost: " ++ Unit.idToString costId
                )
    in
    List.foldr fold (Ok []) unit.cost


eval : Game -> UnitsSnapshot -> Parser.Stat.Expr -> Decimal
eval game snapshot =
    Parser.Stat.evalWithDefault Decimal.zero (Game.getVar game snapshot)
