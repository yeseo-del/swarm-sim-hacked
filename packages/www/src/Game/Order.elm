module Game.Order exposing
    ( Order
    , fromPercent, fromCount, fromBuyN
    , twinUnits
    )

{-| Pay some unit costs and get something in return, or an error if you don't have the money.


# Definitions

@docs Order


# Creating orders, examining costs

@docs fromPercent, fromCount, fromBuyN

-}

import Decimal exposing (Decimal)
import DecimalDict
import Dict exposing (Dict)
import Dict.Extra
import Duration exposing (Duration)
import Game exposing (Game)
import Game.BuyN as BuyN
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Cost as Cost exposing (Cost, CostVal(..))
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import List.Extra
import Maybe.Extra
import Parser.Stat
import Set exposing (Set)


{-| Pay some units, get a buyable in return.

This records the request and total-cost in a form that can be both displayed in
the UI, and processed by a specific buyable's buy-function, without worrying
about how to do cost-math.

-}
type alias Order =
    { item : Item
    , request : BuyN.Request
    , count : Result Decimal Int
    , untwinnedCount : Result Decimal Int
    , isPossible : Bool
    , cost : List ( Unit.Id, Decimal )
    , costNext : List ( Unit.Id, Decimal )

    -- Report non-fatal errors.
    , status : Result String ()
    }



{-
   Order creation - how many things are you buying?

   Order creation has two kinds of errors:
   * fatal errors: the unit's completely unbuyable. no cost defined; unit's not visible; a cost isn't visible. This returns `Err`, and no default order.
   * nonfatal errors: can't afford that unit yet; bogus buyN input. Return a default order of count 1 and a `status` field of `Err`.
-}


fromBuyN : Game -> UnitsSnapshot -> Item -> Result String Order
fromBuyN game snapshot item =
    case Game.buyN item game |> Tuple.second of
        Ok request ->
            create game snapshot item request

        Err err ->
            -- Parse errors in order input aren't fatal, default to ordering 1
            Ok <| createOnNonfatalError game snapshot item err


{-| How many of this unit can I afford to buy with the given percentage of the given bank?
-}
fromPercent : Game -> UnitsSnapshot -> Item -> Float -> Result String Order
fromPercent game snap item percent =
    create game snap item (BuyN.Percent <| floor <| 100 * percent)


fromCount : Game -> UnitsSnapshot -> Item -> Decimal -> Result String Order
fromCount game snap item =
    BuyN.Value >> create game snap item


create : Game -> UnitsSnapshot -> Item -> BuyN.Request -> Result String Order
create game snapshot item request =
    if List.isEmpty (Item.cost item) then
        Err <| "item has no costs defined: " ++ Item.name item

    else if not <| UnitsSnapshot.isItemVisible (Item.id item) snapshot then
        Err <| "item is not visible: " ++ Item.name item

    else if not <| List.all (isCostVisible snapshot) (Item.cost item) then
        Err <|
            "some of the item's costs are not visible: "
                ++ String.join ", "
                    (List.filter (isCostVisible snapshot) (Item.cost item)
                        |> List.map (Tuple.first >> Unit.idToString)
                    )

    else
        case request of
            BuyN.Percent pct100 ->
                let
                    percent =
                        toFloat pct100 / 100
                in
                case numBuyable game snapshot item percent of
                    Nothing ->
                        -- we checked for this above and numBuyable has no other errors, should never happen
                        Err <| "numBuyable: item has no costs defined: " ++ Item.name item

                    Just num ->
                        Decimal.floor num
                            |> createUnsafe game snapshot item (BuyN.Percent <| floor <| 100 * percent) (Just True)
                            |> Ok

            BuyN.Value val ->
                val
                    |> Decimal.floor
                    |> createUnsafe game snapshot item request Nothing
                    |> Ok

            BuyN.TwinValue twinVal ->
                let
                    val =
                        twinVal |> Decimal.flipDiv (twinMult game snapshot item)
                in
                val
                    |> Decimal.ceiling
                    |> createUnsafe game snapshot item request Nothing
                    |> Ok

            BuyN.Target targetVal ->
                let
                    -- amount needed to reach targetVal
                    rawVal =
                        targetVal
                            |> Decimal.flipSub (Game.snapItemCount (Item.id item) snapshot game)
                            |> Decimal.flipDiv (twinMult game snapshot item)
                in
                rawVal
                    -- accept slight rounding errors
                    -- |> Decimal.mulFloat (1 / DecimalDict.subEpsilon)
                    |> Decimal.max Decimal.one
                    |> Decimal.ceiling
                    |> createUnsafe game snapshot item request Nothing
                    |> Ok


numBuyable : Game -> UnitsSnapshot -> Item -> Float -> Maybe Decimal
numBuyable game snap item percent =
    item
        |> Item.cost
        |> List.map (fromPercent1 game snap item <| clamp 0 1 percent)
        |> Decimal.minimum


fromPercent1 : Game -> UnitsSnapshot -> Item -> Float -> Cost -> Decimal
fromPercent1 game snap item percent ( currencyId, cost ) =
    let
        currency =
            UnitsSnapshot.count currencyId snap
                |> Decimal.mulFloat percent
                -- accept slight rounding errors
                |> Decimal.mulFloat (1 / DecimalDict.subEpsilon)
    in
    case cost of
        FixedCost costEach ->
            currency |> Decimal.flipDiv (costEach |> eval game snap)

        FactorCost costFirst factor ->
            let
                count =
                    Game.snapItemCount (Item.id item) snap game
            in
            -- Decimal only supports raising to Float/Int powers.
            -- Count for anything with a cost-factor had better be small enough that that's okay -
            -- if not, quit early, we can't build anything
            case count |> Decimal.toFiniteFloat of
                Nothing ->
                    Decimal.zero

                Just count0 ->
                    let
                        costNext =
                            costFirst |> eval game snap |> Decimal.mul (Decimal.powFloat (Decimal.fromFloat factor) count0)

                        -- https://github.com/swarmsim/swarm/blob/master/app/scripts/services/upgrade.coffee#L86
                        -- https://en.wikipedia.org/wiki/Geometric_progression#Geometric_series
                        -- `currency = costNext * (1 - factor ^ deltaCount) / (1 - factor)`
                        --
                        -- we already know currency. Solve for deltaCount, the maximum number we can afford:
                        -- `factor ^ deltaCount = 1 - currency * (1 - factor) / costNext`
                        -- `deltaCount = logBase factor (1 - currency * (1 - factor) / costNext)`
                        -- that's pseudocode for the definition below. the operators look different, but they're in the right order:
                        deltaCount : Float
                        deltaCount =
                            Decimal.logBase factor (Decimal.one |> Decimal.flipSub (currency |> Decimal.mulFloat (1 - factor) |> Decimal.flipDiv costNext))
                    in
                    deltaCount |> Decimal.fromFloat



-- Order creation, once the number of things we're buying is known


createOnNonfatalError : Game -> UnitsSnapshot -> Item -> String -> Order
createOnNonfatalError game snapshot item err =
    createUnsafe game snapshot item (BuyN.Value Decimal.one) Nothing (Ok 1)
        |> (\o -> { o | status = Err err })


isCostVisible snapshot ( uid, _ ) =
    UnitsSnapshot.isUnitVisible uid snapshot


createUnsafe : Game -> UnitsSnapshot -> Item -> BuyN.Request -> Maybe Bool -> Result Decimal Int -> Order
createUnsafe game snapshot item request isGuaranteed untwinnedCount0 =
    let
        untwinnedCount =
            untwinnedCount0 |> applyMaxLevel game snapshot item

        twinnedCount =
            case untwinnedCount of
                Err d ->
                    d |> Decimal.mul (twinMult game snapshot item) |> Err

                Ok n ->
                    n |> Decimal.fromInt |> Decimal.mul (twinMult game snapshot item) |> Decimal.floor

        cost =
            item |> Item.cost |> List.map (\( curr, val ) -> ( curr, totalCost1 game snapshot item untwinnedCount ( curr, val ) ))

        costNext =
            item |> Item.cost |> List.map (\( curr, val ) -> ( curr, totalCost1 game snapshot item (Ok 1) ( curr, val ) ))

        isPossible =
            -- predict: can we actually buy this order? Sometimes we're just looking.
            if Decimal.lte (Item.resToDec untwinnedCount0) Decimal.zero then
                -- Positive numbers only.
                False

            else
                case isGuaranteed of
                    Just p ->
                        p

                    Nothing ->
                        case numBuyable game snapshot item 1 of
                            Just max ->
                                -- are they attempting to buy more than the max they can afford? The common case.
                                Decimal.lte (Item.resToDec untwinnedCount) max

                            Nothing ->
                                -- can't buy units with no defined cost
                                False
    in
    { item = item
    , request = request
    , count = twinnedCount
    , untwinnedCount = untwinnedCount
    , isPossible = isPossible
    , cost = cost
    , costNext = costNext
    , status = Ok ()
    }


totalCost1 : Game -> UnitsSnapshot -> Item -> Result Decimal Int -> Cost -> Decimal
totalCost1 game snapshot item deltaCount ( currencyId, cost ) =
    case ( deltaCount, cost ) of
        -- fixedCosts are pretty simple
        ( Ok n, FixedCost costEach ) ->
            costEach |> eval game snapshot |> Decimal.mulInt n

        ( Err d, FixedCost costEach ) ->
            costEach |> eval game snapshot |> Decimal.mul d

        -- Anything with a cost-factor needs to have counts small enough to fit in a float.
        -- No such limit for costs, though.
        ( Ok n, FactorCost costFirst factor ) ->
            case Game.snapItemCount (Item.id item) snapshot game |> Decimal.toFiniteFloat of
                Nothing ->
                    Decimal.infinity

                Just count0 ->
                    let
                        costNext =
                            costFirst |> eval game snapshot |> Decimal.mul (Decimal.powFloat (Decimal.fromFloat factor) count0)

                        -- https://github.com/swarmsim/swarm/blob/master/app/scripts/services/upgrade.coffee#L86
                        -- https://en.wikipedia.org/wiki/Geometric_progression#Geometric_series
                        -- `currency = costNext * (1 - factor ^ deltaCount) / (1 - factor)`
                    in
                    costNext |> Decimal.mul (Decimal.one |> Decimal.flipSub (Decimal.powFloat (Decimal.fromFloat factor) (toFloat n))) |> Decimal.flipDivFloat (1 - factor)

        ( Err d, FactorCost costEach _ ) ->
            -- deltaCount is too big for a FactorCost!
            Decimal.infinity


isFactorCost : Item -> Bool
isFactorCost item =
    let
        isFactorCost1 ( _, c ) =
            case c of
                FactorCost _ _ ->
                    True

                _ ->
                    False
    in
    Item.cost item |> List.any isFactorCost1


applyMaxLevel : Game -> UnitsSnapshot -> Item -> Result Decimal Int -> Result Decimal Int
applyMaxLevel game snapshot item buyCount0 =
    case item of
        Item.Upgrade u ->
            let
                maxBuyCount0 : Maybe Decimal
                maxBuyCount0 =
                    -- To guarantee we don't exceed maxLevel, we can't buy
                    -- more than maxLevel minus count (current level).
                    u.maxLevel |> Maybe.map (Decimal.flipSub (Game.snapItemCount (Item.id item) snapshot game))
            in
            case ( maxBuyCount0, buyCount0 ) of
                ( Nothing, _ ) ->
                    buyCount0

                ( Just maxBuyCount, Err d ) ->
                    Err <| Decimal.min d maxBuyCount

                ( Just maxBuyCount, Ok n ) ->
                    Ok <| min n <| floor <| Decimal.toFloat maxBuyCount

        Item.Unit u ->
            -- unit maxCount is usually applied on-tick, not on-buy.
            -- exception: ascensions and respecs only make sense one at a time.
            if u.onBuy.ascend || not (List.isEmpty u.onBuy.respec) then
                Ok 1

            else
                buyCount0

        Item.Spell s ->
            if s.onBuy.ascend || not (List.isEmpty s.onBuy.respec) then
                Ok 1

            else
                buyCount0


twinMult : Game -> UnitsSnapshot -> Item -> Decimal
twinMult game snapshot item =
    case item of
        Item.Unit u ->
            twinUnits game snapshot u

        _ ->
            -- upgrades and spells can't be twinned, silly
            Decimal.one


twinUnits : Game -> UnitsSnapshot -> Unit -> Decimal
twinUnits game snapshot u =
    u.twins |> Maybe.Extra.unwrap Decimal.one (eval game snapshot)


eval : Game -> UnitsSnapshot -> Parser.Stat.Expr -> Decimal
eval game snapshot =
    Parser.Stat.evalWithDefault Decimal.zero (Game.getVar game snapshot)
