module Game.NumberFormat exposing (NumberFormat(..), decoder, default, encoder, list, toConfig, toShortConfig, toString)

import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import NumberSuffix2
import NumberSuffix2.Config as Config exposing (Config, scientificConfig, standardConfig)


type NumberFormat
    = Standard
    | Scientific
    | Engineering
    | LongScale
    | Alphabetic


default : NumberFormat
default =
    Standard


list : List NumberFormat
list =
    [ Standard
    , Scientific
    , Engineering
    , LongScale
    , Alphabetic
    ]


toConfig : NumberFormat -> Config
toConfig format =
    case format of
        Standard ->
            standardConfig

        Scientific ->
            scientificConfig

        Engineering ->
            { standardConfig | getSuffix = Config.suffixEngineering }

        LongScale ->
            { standardConfig | getSuffix = Config.suffixLongScale }

        Alphabetic ->
            { standardConfig | getSuffix = Config.suffixAlphabetic }


toShortConfig : NumberFormat -> Config
toShortConfig format =
    case format of
        Standard ->
            { standardConfig | getSuffix = Config.suffixStandardShort }

        Scientific ->
            scientificConfig

        Engineering ->
            { standardConfig | getSuffix = Config.suffixEngineering }

        LongScale ->
            { standardConfig | getSuffix = Config.suffixLongScaleShort }

        Alphabetic ->
            { standardConfig | getSuffix = Config.suffixAlphabetic }


toString : NumberFormat -> String
toString format =
    -- standard/scientific/engineering names are compatible with old-swarmsim.
    -- (Hybrid format's gone now, not worth the trouble.)
    case format of
        Standard ->
            "standard-decimal"

        Scientific ->
            "scientific-e"

        Engineering ->
            "engineering"

        LongScale ->
            "longScale"

        Alphabetic ->
            "alphabetic"


byString : Dict String NumberFormat
byString =
    list |> List.map (\f -> ( toString f, f )) |> Dict.fromList


encoder : NumberFormat -> E.Value
encoder =
    toString >> E.string


decoder : D.Decoder NumberFormat
decoder =
    D.string
        |> D.andThen
            (\name ->
                case Dict.get name byString of
                    Just format ->
                        D.succeed format

                    Nothing ->
                        D.fail <| "no such numberformat: " ++ name
            )
