module Game.MtxHistory exposing
    ( KongStatus
    , MtxHistory
    , PlayFabStatus
    , Status(..)
    , coffeeDecoder
    , decoder
    , empty
    , encoder
    , kongInventoryStatus
    , playfabInventoryStatus
    , updateKongInventory
    , updatePlayfabInventory
    )

import Dict exposing (Dict)
import GameData exposing (GameData)
import Json.Decode as D
import Json.Decode.Pipeline as P
import Json.Encode as E
import Kongregate.Inventory
import PlayFab.Inventory
import Set exposing (Set)


type alias PlayfabItemId =
    String


type alias KongregateItemId =
    String


type alias MtxHistory =
    -- We should only allow one of these, but legacy saves might sneak in both,
    -- and it's safer to preserve all player transactions
    { playfab : Set PlayfabItemId
    , kongregate : Set KongregateItemId
    }


empty : MtxHistory
empty =
    MtxHistory Set.empty Set.empty


{-| Where does this mtx-entry exist?

ServerOnly (crystals not yet added to this save) should eventually become Synced.
ClientOnly happens if we move a save from one account to another.

-}
type Status item
    = ServerOnly item
    | Synced item
    | ClientOnly PlayfabItemId


type alias PlayFabStatus =
    Status PlayFab.Inventory.Item


type alias KongStatus =
    Status Kongregate.Inventory.Item


kongInventoryStatus : Kongregate.Inventory.Inventory -> MtxHistory -> List KongStatus
kongInventoryStatus serverItems hist =
    let
        serverStatus item =
            if Set.member (Kongregate.Inventory.idToString item.id) hist.kongregate then
                Synced item

            else
                ServerOnly item

        clientOnly =
            serverItems
                |> List.map (.id >> Kongregate.Inventory.idToString)
                |> Set.fromList
                |> Set.diff hist.playfab
                |> Set.toList
                |> List.map ClientOnly
    in
    (serverItems |> List.map serverStatus) ++ clientOnly


updateKongInventory : GameData -> Kongregate.Inventory.Inventory -> MtxHistory -> ( Int, MtxHistory )
updateKongInventory gd serverItems hist0 =
    let
        pending : List ( Kongregate.Inventory.Item, Int )
        pending =
            hist0
                |> kongInventoryStatus serverItems
                |> List.filterMap
                    (\status ->
                        case status of
                            ServerOnly item ->
                                Just item

                            _ ->
                                Nothing
                    )
                |> List.filterMap
                    (\item ->
                        GameData.mtxById item.identifier gd
                            |> Maybe.map (\mtx -> ( item, mtx.count ))
                    )
    in
    ( pending |> List.map Tuple.second |> List.sum
    , { hist0
        | kongregate =
            hist0.kongregate
                |> Set.union
                    (pending
                        |> List.map (Tuple.first >> .id >> Kongregate.Inventory.idToString)
                        |> Set.fromList
                    )
      }
    )


playfabInventoryStatus : PlayFab.Inventory.Inventory -> MtxHistory -> List PlayFabStatus
playfabInventoryStatus serverItems hist =
    let
        serverStatus item =
            if Set.member (PlayFab.Inventory.instanceIdToString item.instanceId) hist.playfab then
                Synced item

            else
                ServerOnly item

        clientOnly =
            serverItems
                |> List.map (.instanceId >> PlayFab.Inventory.instanceIdToString)
                |> Set.fromList
                |> Set.diff hist.playfab
                |> Set.toList
                |> List.map ClientOnly
    in
    (serverItems |> List.map serverStatus) ++ clientOnly


updatePlayfabInventory : GameData -> PlayFab.Inventory.Inventory -> MtxHistory -> ( Int, MtxHistory )
updatePlayfabInventory gd serverItems hist0 =
    let
        pending : List ( PlayFab.Inventory.Item, Int )
        pending =
            hist0
                |> playfabInventoryStatus serverItems
                |> List.filterMap
                    (\status ->
                        case status of
                            ServerOnly item ->
                                Just item

                            _ ->
                                Nothing
                    )
                |> List.filterMap
                    (\item ->
                        GameData.mtxByName (PlayFab.Inventory.itemIdToString item.itemId) gd
                            |> Maybe.map (\mtx -> ( item, mtx.count ))
                    )
    in
    ( pending |> List.map Tuple.second |> List.sum
    , { hist0
        | playfab =
            hist0.playfab
                |> Set.union
                    (pending
                        |> List.map (Tuple.first >> .instanceId >> PlayFab.Inventory.instanceIdToString)
                        |> Set.fromList
                    )
      }
    )


encoder : MtxHistory -> E.Value
encoder hist =
    E.object <|
        [ ( "playfab", hist.playfab |> setEncoder )
        , ( "kongregate", hist.kongregate |> setEncoder )
        ]


decoder : D.Decoder MtxHistory
decoder =
    D.succeed MtxHistory
        |> P.optional "playfab" setDecoder Set.empty
        |> P.optional "kongregate" setDecoder Set.empty


{-| Decode mtx history from the old Coffeescript version.
-}
coffeeDecoder : D.Decoder MtxHistory
coffeeDecoder =
    D.succeed MtxHistory
        |> P.optionalAt [ "playfab", "items" ] objectSetDecoder Set.empty
        |> P.optional "kongregate" objectSetDecoder Set.empty


setEncoder : Set String -> E.Value
setEncoder =
    Set.toList >> E.list E.string


setDecoder : D.Decoder (Set String)
setDecoder =
    D.list D.string |> D.map Set.fromList


objectSetDecoder : D.Decoder (Set String)
objectSetDecoder =
    D.dict D.value |> D.map (Dict.keys >> Set.fromList)
