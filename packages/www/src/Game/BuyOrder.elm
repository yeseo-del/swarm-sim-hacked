module Game.BuyOrder exposing (buy)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.OnBuy
import Game.Order as Order exposing (Order)
import Game.Statistics as Statistics exposing (Statistics)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item, resToDec)
import GameData.Unit as Unit exposing (Unit)
import Maybe.Extra
import Parser.Stat


{-| Execute an order: pay, add purchased items, and run onBuy triggers.

This is separate from the `order` module to avoid a circular dependency with `Game.OnBuy`, where respec must examine orders.

-}
buy : Order -> Game -> Result String Game
buy o =
    -- No need for validation here - order creation does it. Invalid orders can't exist.
    -- Orders we can't pay for *can* exist, but `pay` fails when that happens - no need to check up front
    pay o
        >> Result.map (Game.addItem o.item (resToDec o.count))
        >> Result.andThen (Game.OnBuy.run o)
        >> Result.map (applyMinUnits >> Game.pushStatistics o)
        >> Result.map Game.refreshSnapshotUnits


applyMinUnits : Game -> Game
applyMinUnits game =
    let
        minUnits : List ( Unit, Decimal )
        minUnits =
            Game.gameData game
                |> GameData.units
                |> List.filterMap
                    (\u ->
                        u.minCount
                            |> Maybe.map (Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game))
                            -- don't apply minUnits if it's zero. This keeps persisted data a little cleaner
                            -- by not adding units with a minCount of zero to unitsSnapshot. Without this filter,
                            -- they'd be persisted with count zero - same behavior, just a little cleaner.
                            |> Maybe.Extra.filter ((/=) Decimal.zero)
                            |> Maybe.map (Tuple.pair u)
                    )

        fold : ( Unit, Decimal ) -> UnitsSnapshot -> UnitsSnapshot
        fold ( u, minCount ) =
            UnitsSnapshot.update u.id (Maybe.Extra.unwrap minCount (Decimal.max minCount) >> Just)
    in
    Game.updateUnits (minUnits |> List.foldl fold (Game.units game)) game


{-| Pay costs if possible, or return an error if not.

Caller is responsible for giving the player something worthwhile after paying.

-}
pay : Order -> Game -> Result String Game
pay o game =
    List.foldl (\( id, c ) -> Result.andThen <| UnitsSnapshot.sub id c) (Ok (Game.units game)) o.cost
        |> Result.map (\units -> Game.updateUnits units game)
