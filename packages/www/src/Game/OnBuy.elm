module Game.OnBuy exposing (run)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Item.Id as ItemId
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Unit as Unit exposing (Unit)
import Parser.Stat
import Random exposing (Generator)
import RandomUtil
import Set exposing (Set)
import Time exposing (Posix)


run : Order -> Game -> Result String Game
run { item, count } game =
    item |> Item.onBuy |> .all |> List.foldl (\onBuy -> Result.andThen <| run1 item count onBuy) (Ok game)


run1 : Item -> Result Decimal Int -> OnBuy -> Game -> Result String Game
run1 item count trigger game =
    let
        dCount =
            Item.resToDec count
    in
    case trigger of
        OnBuy.AddUnits childUnit expr ->
            game |> onBuyAddUnits childUnit expr dCount |> Ok

        OnBuy.AddUnitsCooldown unitId expr cooldown ->
            -- TODO: cooldown addUnits only works if the unit's already visible. This
            -- happens to be how I'd prefer hatchery/expansion crystals to behave,
            -- and that happens to be the only instance of AddUnitsCooldown today.
            -- It's kind of surprising/unexpected behavior compared to non-cooldown
            -- addUnits, though; it's a difference in behavior that's not cooldown
            -- related. Should probably change it somehow.
            case Game.gameData game |> GameData.unitById unitId of
                Nothing ->
                    game |> Ok

                Just unit ->
                    let
                        units =
                            Game.units game
                    in
                    if not (UnitsSnapshot.isUnitVisible unitId units && UnitsSnapshot.isTabVisible unit.tab units) then
                        game |> Ok

                    else
                        case Game.popCooldown cooldown game of
                            Nothing ->
                                game |> Ok

                            Just game1 ->
                                -- cooldown addUnits only fires once (after the first, it's waiting again)
                                game1 |> onBuyAddUnits unitId expr Decimal.one |> Ok

        OnBuy.MulUnits childUnit expr ->
            let
                units1 =
                    game
                        |> Game.units
                        |> UnitsSnapshot.mul childUnit
                            (expr
                                |> Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game)
                                |> Decimal.flipPowFloat (dCount |> Decimal.toFloat)
                            )
            in
            Game.updateUnits units1 game |> Ok

        OnBuy.AddTime expr ->
            let
                dur =
                    expr
                        |> Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game)
                        |> Decimal.mul dCount
                        |> Decimal.toFloat
                        |> Duration.fromSecs
            in
            Game.updateUnits (Game.skipTime dur game) game |> Ok

        OnBuy.AddRandomUnits args ->
            case count of
                Ok n ->
                    let
                        start =
                            (Game.itemCount (Item.id item) game |> Decimal.toFloat |> floor) - n

                        levels =
                            List.range (start + 1) (start + n)
                    in
                    List.foldl (addRandomUnitsOnBuy item args) game levels |> Ok

                err ->
                    Err "order.count is too large for AddRandomUnits"

        OnBuy.Ascend ->
            Ok <| ascend game

        OnBuy.Respec currency rate resetNames ->
            let
                spent : Decimal
                spent =
                    respecSpent currency resetNames game

                refunded =
                    Decimal.mulFloat rate spent
            in
            game
                |> respecReset resetNames
                |> Game.addItemId (ItemId.UnitId currency) refunded
                |> Ok


respecSpent : Unit.Id -> Set String -> Game -> Decimal
respecSpent currency resetNames game0 =
    let
        gd =
            Game.gameData game0

        resetCounts : List ( Item, Decimal )
        resetCounts =
            resetNames
                |> Set.toList
                |> List.filterMap (\name -> GameData.itemByName name gd)
                |> List.map (\resetItem -> ( resetItem, Game.itemCount (Item.id resetItem) game0 ))

        -- _ =
        -- resetCounts |> List.map (Tuple.mapBoth Item.name Decimal.toString) |> Debug.log "reset counts"
        --
        -- How much did we spend/how much to refund? This is tricky,
        -- because mutagen-unlock cost-expressions reference themselves.
        -- Changing their counts changes the costs.
        -- So to get the cost, we reset everything, add the units we had
        -- one type at a time, and total what we would've spent on orders
        -- as we go.
        loopSpent ( item, count ) ( game, orders ) =
            case Order.fromCount game (Game.units game) item count of
                Err err ->
                    ( game, orders )

                Ok o ->
                    ( Game.addItem item count game |> Game.refreshSnapshotUnits
                    , o :: orders
                    )

        spentOrders : List Order
        spentOrders =
            resetCounts
                |> List.sortBy (Tuple.first >> respecSortBy)
                |> List.foldl loopSpent ( respecReset resetNames game0, [] )
                |> Tuple.second
    in
    spentOrders
        |> List.concatMap (.cost >> List.filter (Tuple.first >> (==) currency) >> List.map Tuple.second)
        |> Decimal.sum


{-| The order in which we simulate buying respec items to determine what was spent.

Buy upgrades first, since they make units visible.

TODO: this is fragile and mutagen-specific. We really ought to have something
more robust, like retrying invisible units or persisting expenses separately.

-}
respecSortBy : Item -> ( Float, String )
respecSortBy item =
    let
        n =
            case item of
                Item.Upgrade _ ->
                    0

                Item.Unit _ ->
                    1

                Item.Spell _ ->
                    2
    in
    ( n, Item.name item )


respecReset : Set String -> Game -> Game
respecReset wipedNames game =
    wipedNames
        |> Set.toList
        |> List.filterMap (\name -> GameData.itemByName name (Game.gameData game))
        |> List.map Item.id
        |> List.foldl Game.removeItemId game
        |> Game.recalculateVisibility


onBuyAddUnits : Unit.Id -> Parser.Stat.Expr -> Decimal -> Game -> Game
onBuyAddUnits id expr dCount game =
    if Parser.Stat.variables expr |> Set.member (Unit.idToString id) |> not then
        -- the common case - simply multiply
        let
            units1 =
                game
                    |> Game.units
                    |> UnitsSnapshot.add id
                        (expr
                            |> Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game)
                            |> Decimal.mul dCount
                        )
        in
        Game.updateUnits units1 game

    else
        -- clone-larvae case: the expression references itself.
        -- Simple multiplying won't work. Hope the number's smallish,
        -- because to support this case, we're looping.
        let
            loopAdd : Int -> UnitsSnapshot -> UnitsSnapshot
            loopAdd n snap =
                if n <= 0 then
                    snap

                else
                    loopAdd (n - 1)
                        (snap
                            |> UnitsSnapshot.add id
                                (expr |> Parser.Stat.evalWithDefault Decimal.zero (Game.getVar game snap))
                        )

            units1 =
                loopAdd (Decimal.toFloat dCount |> floor) (Game.units game)
        in
        Game.updateUnits units1 game


ascend : Game -> Game
ascend game =
    let
        ascendPreserve : List Unit
        ascendPreserve =
            game |> Game.gameData |> GameData.units |> List.filter .ascendPreserve

        initUnits : Dict String Decimal
        initUnits =
            game
                |> Game.gameData
                |> GameData.initUnits
                |> List.map (Tuple.mapFirst Unit.name)
                |> Dict.fromList

        unitsDict : Dict String Decimal
        unitsDict =
            ascendPreserve
                |> List.map (\u -> ( Unit.name u, Game.unitCount u.id game ))
                |> Dict.fromList
                |> Dict.union initUnits
    in
    game
        |> Game.updateUpgrades Dict.empty
        |> Game.updateSpells Dict.empty
        |> Game.updateUnits (UnitsSnapshot.create (Game.snapshotted game) unitsDict Dict.empty Set.empty)
        |> Game.clearUI


addRandomUnitsOnBuy : Item -> OnBuy.AddRandomUnitsData -> Int -> Game -> Game
addRandomUnitsOnBuy item args level game0 =
    let
        getCount name =
            if name == Item.name item then
                Just <| Decimal.fromInt level

            else
                Game.getGameVar game0 name

        chance0 : Float
        chance0 =
            args.chance |> Parser.Stat.evalWithDefault Decimal.zero getCount |> Decimal.toFloat

        randomCount0 : Decimal
        randomCount0 =
            args.count |> Parser.Stat.evalWithDefault Decimal.zero getCount

        ( ( chance1, randomCount ), game1 ) =
            Game.randomStep
                item
                (Random.pair
                    (RandomUtil.chance chance0)
                    (RandomUtil.nearDecimal 0.9 1.1 randomCount0)
                )
                game0

        -- This is a relic of swarm1, specific to mutagen.
        -- Levels divisible by 8 always grant mutagen because streaks of misses aren't fun.
        -- If this code ever sees randomness that isn't mutagen, it might need to change.
        chance : Bool
        chance =
            level >= args.minimum && (chance1 || (modBy 8 level == 0))
    in
    if chance then
        game1 |> Game.addItemId (ItemId.UnitId args.child) randomCount

    else
        game1
