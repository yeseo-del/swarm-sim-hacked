module Game.BuyN exposing
    ( Request(..), parse
    , Settings, empty
    , get, insert
    , encoder, decoder
    )

{-| A buy-N input and its parsed value.


# Parsing settings values

@docs Request, parse


# Settings dictionary definition

@docs Settings, empty


# Settings values access

@docs get, insert


# Serialization

@docs encoder, decoder

-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Item as Item exposing (Item)
import Json.Decode as D
import Json.Encode as E
import Maybe.Extra
import Parser exposing ((|.), (|=), Parser)
import Set exposing (Set)



-- Parsing


type Request
    = Percent Int
    | Value Decimal
    | Target Decimal
    | TwinValue Decimal


parse : String -> Result String Request
parse =
    Parser.run parser >> Result.mapError Parser.deadEndsToString


parser : Parser Request
parser =
    Parser.oneOf
        -- numbers with prefixes
        [ Parser.succeed Target
            |. Parser.symbol "@"
            |= parseNumber
        , Parser.succeed TwinValue
            |. Parser.symbol "="
            |= parseNumber

        -- no prefix - could be percent or exact-value
        , Parser.succeed Tuple.pair
            |= parseNumber
            |= Parser.oneOf
                [ Parser.succeed (Decimal.toFloat >> floor >> Percent)
                    |. Parser.symbol "%"
                , Parser.succeed Value
                ]
            |. Parser.end
            |> Parser.map (\( n, fn ) -> fn n)
        ]


floatFromInts : Int -> Int -> Float
floatFromInts ones decs =
    toFloat ones + (toFloat decs / (10 ^ toFloat (String.length (String.fromInt decs))))


parseNumber : Parser Decimal
parseNumber =
    -- parse things that look number-ish, and let Decimal.toString do the heavy lifting
    Parser.variable
        { start = Char.isDigit
        , inner = \c -> Char.isDigit c || c == 'e' || c == '.' || c == '-' || c == '+'
        , reserved = Set.empty
        }
        |> Parser.andThen
            (\s ->
                case Decimal.fromString s of
                    Just d ->
                        Parser.succeed d

                    Nothing ->
                        Parser.problem <| "not a decimal: " ++ s
            )



-- Settings/persistence


type Settings
    = Settings (Dict String String)


empty : Settings
empty =
    Settings Dict.empty


init1 : String
init1 =
    "100%"


get : Item -> Settings -> ( String, Result String Request )
get item (Settings dict) =
    let
        raw =
            dict |> Dict.get (Item.buyGroup item) |> Maybe.withDefault init1
    in
    ( raw, parse raw )


insert : Item -> String -> Settings -> Settings
insert item val (Settings dict) =
    dict |> Dict.insert (Item.buyGroup item) val |> Settings



-- Serialization


encoder : Settings -> E.Value
encoder (Settings dict) =
    E.dict identity E.string dict


decoder : D.Decoder Settings
decoder =
    D.dict D.string |> D.map Settings
