module Game.Statistics exposing (Entry, Order, Statistics, decoder, empty, encoder, push)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import GameData.Item as Item exposing (Item, resToDec)
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Json.Decode as D
import Json.Decode.Pipeline as P
import Json.Encode as E
import JsonUtil
import Time exposing (Posix)


type alias Statistics =
    { clicks : Int, byUnit : Dict String Entry, byUpgrade : Dict String Entry }


type alias Entry =
    { clicks : Int, num : Decimal, twinnum : Decimal, elapsedFirst : Duration }


empty : Statistics
empty =
    { clicks = 0, byUnit = Dict.empty, byUpgrade = Dict.empty }


emptyEntry : Duration -> Entry
emptyEntry dur =
    { clicks = 0, num = Decimal.zero, twinnum = Decimal.zero, elapsedFirst = dur }


type alias Order o =
    { o | item : Item, count : Result Decimal Int, untwinnedCount : Result Decimal Int }


push : Duration -> Order o -> Statistics -> Statistics
push sinceFirst order statistics0 =
    let
        statistics =
            { statistics0 | clicks = statistics0.clicks + 1 }

        update =
            updateEntry sinceFirst order >> Just
    in
    case order.item of
        Item.Unit unit ->
            { statistics | byUnit = statistics.byUnit |> Dict.update (Unit.name unit) update }

        Item.Upgrade upgrade ->
            { statistics | byUpgrade = statistics.byUpgrade |> Dict.update (Upgrade.name upgrade) update }

        -- TODO: tracking spells and upgrades together is for old-swarmsim compatibility. Migrate to a third group!
        Item.Spell spell ->
            { statistics | byUpgrade = statistics.byUpgrade |> Dict.update (Spell.name spell) update }


updateEntry : Duration -> Order o -> Maybe Entry -> Entry
updateEntry sinceFirst { item, count, untwinnedCount } entry0 =
    let
        entry : Entry
        entry =
            entry0 |> Maybe.withDefault (emptyEntry sinceFirst)
    in
    { entry
        | clicks = entry.clicks + 1
        , num = Decimal.add entry.num (Item.resToDec untwinnedCount)
        , twinnum = Decimal.add entry.twinnum (Item.resToDec count)
    }



-- serialization


encoder : Statistics -> E.Value
encoder stats =
    E.object
        [ ( "clicks", stats.clicks |> E.int )
        , ( "byUnit", stats.byUnit |> E.dict identity entryEncoder )
        , ( "byUpgrade", stats.byUpgrade |> E.dict identity entryEncoder )
        ]


decoder : D.Decoder Statistics
decoder =
    D.map3 Statistics
        (D.field "clicks" D.int)
        (D.field "byUnit" <| D.dict entryDecoder)
        (D.field "byUpgrade" <| D.dict entryDecoder)


entryEncoder : Entry -> E.Value
entryEncoder item =
    E.object
        [ ( "clicks", item.clicks |> E.int )
        , ( "num", item.num |> JsonUtil.decimalEncoder )
        , ( "twinnum", item.twinnum |> JsonUtil.decimalEncoder )
        , ( "elapsedFirst", item.elapsedFirst |> Duration.toMillis |> E.int )
        ]


entryDecoder : D.Decoder Entry
entryDecoder =
    D.succeed Entry
        |> P.required "clicks" D.int
        |> P.required "num" JsonUtil.decimalDecoder
        |> P.custom
            (D.oneOf
                [ D.field "twinnum" JsonUtil.decimalDecoder

                -- old save files are sometimes missing twinnum
                , D.field "num" JsonUtil.decimalDecoder
                ]
            )
        |> P.required "elapsedFirst" (D.int |> D.map Duration.fromMillis)
