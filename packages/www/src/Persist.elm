module Persist exposing (exportDecoder, readDecoder)

import Json.Decode as D


readDecoder : D.Decoder (Maybe ( D.Value, String ))
readDecoder =
    let
        builder status =
            case status of
                "empty" ->
                    D.succeed Nothing

                "success" ->
                    D.map2 Tuple.pair
                        (D.field "value" D.value)
                        (D.field "raw" D.string)
                        |> D.map Just

                "error" ->
                    -- let the decoder handle error context
                    D.fail "persistRead javascript error"

                _ ->
                    D.fail <| "persistRead javascript unknown-status: " ++ status
    in
    D.field "status" D.string |> D.andThen builder


exportDecoder : D.Decoder (Maybe String)
exportDecoder =
    let
        builder status =
            case status of
                "empty" ->
                    D.succeed Nothing

                "success" ->
                    D.field "value" D.string
                        |> D.map Just

                _ ->
                    D.fail <| "persistRead javascript unknown-status: " ++ status
    in
    D.field "status" D.string |> D.andThen builder
