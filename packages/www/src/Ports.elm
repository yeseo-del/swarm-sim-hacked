port module Ports exposing (apiRequest, apiResponse, konami, onUnload, onUnloadWrite, persistExport, persistImport, persistImportRead, persistRead, persistWrite, playfabDecodeSaveReq, playfabDecodeSaveRes, readUserSessionTicket, serviceWorker, translatorRead, translatorWrite, writeUserRememberId, writeUserSessionTicket)

import Json.Decode as D


{-| JS decoded some game state that Elm should load.
-}
port persistRead : (D.Value -> msg) -> Sub msg


{-| Elm has some game state that JS should encode and save to localstorage.
-}
port persistWrite : D.Value -> Cmd msg


{-| Elm has some encoded game state that JS should decode. JS will return the decoded value via `persistRead`.
-}
port persistImport : String -> Cmd msg


{-| JS decoded some game state from an import that Elm should load.
-}
port persistImportRead : (D.Value -> msg) -> Sub msg


{-| JS has some encoded game state that Elm should allow the user to export to, say, a text file. (Elm can't decode the value itself.)
-}
port persistExport : (D.Value -> msg) -> Sub msg


{-| JS detected that the developer just updated the website, and we should nag the user to refresh the page.
-}
port serviceWorker : (D.Value -> msg) -> Sub msg


{-| The user logged in to PlayFab. Save their session ticket to localstorage, and when they refresh, call PlayFab to fetch account info.

These expire after 24 hours, and that's sadly not configurable.

-}
port writeUserSessionTicket : Maybe String -> Cmd msg


{-| The user's "remember me" token. Never expires.
-}
port writeUserRememberId : Maybe String -> Cmd msg


{-| The user logged in to PlayFab outside of Elm - perhaps in another tab, or before refreshing the page.
-}
port readUserSessionTicket : ({ sessionTicket : Maybe String, rememberId : Maybe String } -> msg) -> Sub msg


port playfabDecodeSaveReq : { userId : String, encoded : String } -> Cmd msg


port playfabDecodeSaveRes : (D.Value -> msg) -> Sub msg


{-| Notify Elm that the page is closing. Cleanup; prepare to write a saved game.
-}
port onUnload : ({} -> msg) -> Sub msg


{-| Write a saved game while the page is closing.
-}
port onUnloadWrite : { playfabUrl : String, game : D.Value } -> Cmd msg


port konami : (() -> msg) -> Sub msg


port apiRequest : (D.Value -> msg) -> Sub msg


port apiResponse : D.Value -> Cmd msg


port translatorRead : (D.Value -> msg) -> Sub msg


port translatorWrite : D.Value -> Cmd msg
