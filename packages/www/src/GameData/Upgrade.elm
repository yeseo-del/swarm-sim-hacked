module GameData.Upgrade exposing (Id, Upgrade, decoder, idToString, name)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Unit as Unit exposing (Unit)
import GameData.Unit.Id as UnitId
import GameData.Upgrade.Id as Id
import GameData.Util
import Json.Decode as D
import Parser.Visibility


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Upgrade =
    { id : Id
    , cost : List Cost
    , onBuy : OnBuy.Index
    , unitId : Unit.Id
    , unit : Unit
    , visible : Parser.Visibility.Expr
    , buyGroup : String
    , maxLevel : Maybe Decimal
    , isBulkBuyable : Bool
    }


name : Upgrade -> String
name =
    .id >> idToString


decoder : (String -> List Cost) -> (String -> List OnBuy) -> (String -> Maybe Unit) -> D.Decoder Upgrade
decoder cost onBuy unitFromId =
    let
        builder id unitId =
            case unitFromId (UnitId.toString unitId) of
                Nothing ->
                    D.fail <| "no such unitid for upgrade: " ++ UnitId.toString unitId

                Just unit ->
                    D.map4 (Upgrade id (idToString id |> cost) (idToString id |> onBuy |> OnBuy.indexFromList) unitId unit)
                        -- upgrades are only visible if their unit is also visible!
                        (D.field "visible" (Parser.Visibility.fromJson |> D.map (\v -> Parser.Visibility.and [ unit.visible, v ])))
                        (D.field "buyGroup" D.string)
                        (D.maybe <| D.field "maxLevel" GameData.Util.decimalFromJson)
                        (D.field "isBulkBuyable" D.bool)
    in
    D.map2 builder
        (D.field "name" Id.decoder)
        (D.field "unit" UnitId.decoder)
        |> D.andThen identity
