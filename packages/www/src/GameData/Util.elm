module GameData.Util exposing (boolFromJson, decimalFromJson)

import Decimal exposing (Decimal)
import Json.Decode as D


{-| Decode a boolean from a spreadsheet string.

Different from Json.Decode.bool - spreadsheets emit bools as strings.

-}
boolFromJson : D.Decoder Bool
boolFromJson =
    let
        decode str =
            case str |> String.toLower of
                "false" ->
                    False

                "0" ->
                    False

                "" ->
                    False

                _ ->
                    True
    in
    D.string |> D.map decode


{-| Only for gamedata spreadsheet decoding! This will fail for data persisted as strings.
-}
decimalFromJson : D.Decoder Decimal
decimalFromJson =
    D.float |> D.map Decimal.fromFloat
