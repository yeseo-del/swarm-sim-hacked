module GameData.Unit exposing (Id, Unit, decoder, idToString, name)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Tab.Id as TabId
import GameData.Unit.Id as Id
import GameData.Util
import Json.Decode as D
import Json.Decode.Pipeline as P
import Maybe.Extra
import Parser.Stat
import Parser.Visibility


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Unit =
    { id : Id
    , cost : List Cost
    , onBuy : OnBuy.Index
    , slug : String
    , tab : TabId.Id
    , init : Maybe Decimal
    , ascendPreserve : Bool
    , tier : Maybe Int
    , prod : Maybe ( Id, Parser.Stat.Expr )
    , warnfirst : Maybe ( Id, Int )
    , visible : Parser.Visibility.Expr
    , watchable : Bool
    , twins : Maybe Parser.Stat.Expr
    , minCount : Maybe Parser.Stat.Expr
    , maxCount : Maybe Parser.Stat.Expr

    -- Index in the array of all units. ProductionGraph needs this.
    , ordinal : Int
    }


name : Unit -> String
name =
    .id >> idToString



-- serialization


decoder : (String -> List Cost) -> (String -> List OnBuy) -> D.Decoder (Int -> Unit)
decoder cost onBuy =
    let
        builder id =
            D.succeed (Unit id (idToString id |> cost) (idToString id |> onBuy |> OnBuy.indexFromList))
                |> P.required "slug" D.string
                |> P.required "tab" TabId.decoder
                |> P.optional "init" (D.maybe GameData.Util.decimalFromJson) Nothing
                |> P.optional "ascendPreserve" GameData.Util.boolFromJson False
                |> P.optional "tier" (D.maybe D.int) Nothing
                |> P.custom
                    (D.oneOf
                        [ D.succeed Tuple.pair
                            |> P.required "prod.unittype" Id.decoder
                            |> P.required "prod.expr" Parser.Stat.fromJson
                            |> D.map Just
                        , D.succeed Nothing
                        ]
                    )
                |> P.custom
                    (D.map2 Tuple.pair
                        (D.field "warnfirst.unittype" Id.decoder)
                        (D.field "warnfirst.val" D.int)
                        |> D.maybe
                    )
                |> P.required "visible" Parser.Visibility.fromJson
                |> P.optional "watchable" D.bool False
                |> P.optional "twins" (Parser.Stat.fromJson |> D.map Just) Nothing
                |> P.optional "minCount" (Parser.Stat.fromJson |> D.map Just) Nothing
                |> P.optional "maxCount" (Parser.Stat.fromJson |> D.map Just) Nothing
    in
    D.field "name" Id.decoder |> D.andThen builder


maybeFail : String -> Maybe a -> D.Decoder a
maybeFail err val =
    case val of
        Nothing ->
            D.fail err

        Just yay ->
            D.succeed yay
