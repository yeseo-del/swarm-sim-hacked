module GameData.Spell exposing (Id, Spell, decoder, idToString, name)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Spell.Id as Id
import GameData.Tab.Id as TabId
import GameData.Util
import Json.Decode as D
import Parser.Visibility


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Spell =
    { id : Id
    , cost : List Cost
    , onBuy : OnBuy.Index
    , slug : String
    , tab : TabId.Id
    , visible : Parser.Visibility.Expr
    }


name : Spell -> String
name =
    .id >> idToString


decoder : (String -> List Cost) -> (String -> List OnBuy) -> D.Decoder Spell
decoder cost onBuy =
    let
        builder id =
            D.map3 (Spell id (idToString id |> cost) (idToString id |> onBuy |> OnBuy.indexFromList))
                (D.field "slug" D.string)
                (D.field "tab" TabId.decoder)
                (D.field "visible" Parser.Visibility.fromJson)
    in
    D.field "name" Id.decoder |> D.andThen builder
