module GameData.Spell.Id exposing (Id, decoder, toString)

import Json.Decode as D


type Id
    = Id String


toString : Id -> String
toString (Id id) =
    id


decoder : D.Decoder Id
decoder =
    D.string |> D.map Id
