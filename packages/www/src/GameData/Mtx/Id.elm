module GameData.Mtx.Id exposing (Id, decoder, fromString, toString)

import Json.Decode as D


type Id
    = Id String


{-| Hardcoded tab id. Use sparingly.
-}
fromString : String -> Id
fromString =
    Id


toString : Id -> String
toString (Id id) =
    id


decoder : D.Decoder Id
decoder =
    D.string |> D.map Id
