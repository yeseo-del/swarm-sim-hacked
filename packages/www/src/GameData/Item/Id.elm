module GameData.Item.Id exposing (Id(..), toString)

import GameData.Spell.Id as SpellId
import GameData.Unit.Id as UnitId
import GameData.Upgrade.Id as UpgradeId


type Id
    = UnitId UnitId.Id
    | UpgradeId UpgradeId.Id
    | SpellId SpellId.Id


toString : Id -> String
toString wrap =
    case wrap of
        UnitId i ->
            UnitId.toString i

        UpgradeId i ->
            UpgradeId.toString i

        SpellId i ->
            SpellId.toString i
