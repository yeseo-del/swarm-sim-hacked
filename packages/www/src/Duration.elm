module Duration exposing
    ( Duration, since, fromMillis, fromSecs
    , add, sub, min, max, zero, sum, mean, negate
    , toMillis, toSecs, toMinutes
    , getFields, getMillis, getSecs, getMins, getHours, getDays
    , toString, toStringDHMS, toStringColons, ColonsConfig
    , addPosix
    )

{-| Some amount of elapsed time.


# Creation

@docs Duration, since, fromMillis, fromSecs


# Math

@docs add, sub, min, max, zero, sum, mean, negate


# Exporting an entire duration

@docs toMillis, toSecs, toMinutes


# Extracting pieces of the duration for formatting

@docs getFields, getMillis, getSecs, getMins, getHours, getDays


# Stringifying

@docs toString, toStringDHMS, toStringColons, ColonsConfig

-}

import Time exposing (Posix)


type Duration
    = Duration Int


fromMillis : Int -> Duration
fromMillis =
    Duration


toMillis : Duration -> Int
toMillis (Duration n) =
    n


{-| Elm's integer math gets screwy for integers above some maximum. For places where that's a possibility, convert to float first.
-}
toMillisF : Duration -> Float
toMillisF =
    toMillis >> toFloat


add : Duration -> Duration -> Duration
add (Duration a) (Duration b) =
    Duration <| a + b


sub : Duration -> Duration -> Duration
sub (Duration a) (Duration b) =
    Duration <| a - b


negate : Duration -> Duration
negate (Duration a) =
    Duration -a


sum : List Duration -> Duration
sum =
    List.map toMillis >> List.sum >> fromMillis


mean : List Duration -> Duration
mean ds =
    toMillis (sum ds)
        // Basics.max 1 (List.length ds)
        |> fromMillis


min : Duration -> Duration -> Duration
min (Duration a) (Duration b) =
    Duration <| Basics.min a b


max : Duration -> Duration -> Duration
max (Duration a) (Duration b) =
    Duration <| Basics.max a b


since : { before : Posix, after : Posix } -> Duration
since { before, after } =
    Time.posixToMillis after - Time.posixToMillis before |> fromMillis


units1 =
    { sec = 1000
    , min = 60
    , hour = 60
    , day = 24
    , year = 365
    }


units =
    { sec = 1000
    , min = 1000 * 60
    , hour = 1000 * 60 * 60
    , day = 1000 * 60 * 60 * 24
    , year = 1000 * 60 * 60 * 24 * 365
    }


fromSecs : Float -> Duration
fromSecs =
    (*) units.sec >> floor >> fromMillis


toSecs : Duration -> Float
toSecs f =
    (f |> toMillis |> toFloat) / units.sec


toMinutes : Duration -> Float
toMinutes f =
    (f |> toMillis |> toFloat) / units.min


addPosix : Duration -> Posix -> Posix
addPosix duration now =
    toMillis duration + Time.posixToMillis now |> Time.millisToPosix



-- Exporting chunks


getMillis : Duration -> Int
getMillis d =
    toMillis d |> modBy units.sec


getSecs : Duration -> Int
getSecs d =
    -- if only it were this easy, harharhar
    (toMillisF d / units.sec) |> floor |> modBy units1.min


getMins : Duration -> Int
getMins d =
    (toMillisF d / units.min) |> floor |> modBy units1.hour


getHours : Duration -> Int
getHours d =
    (toMillisF d / units.hour) |> floor |> modBy units1.day


getDays : Duration -> Int
getDays d =
    (toMillisF d / units.day) |> floor |> modBy units1.year


getYears : Duration -> Int
getYears d =
    toMillisF d / units.year |> floor


type alias Fields =
    { millis : Int, secs : Int, mins : Int, hours : Int, days : Int, years : Int }


getFields : Duration -> Fields
getFields d =
    { millis = getMillis d
    , secs = getSecs d
    , mins = getMins d
    , hours = getHours d
    , days = getDays d
    , years = getYears d
    }


toStringDHMS : Duration -> String
toStringDHMS d =
    let
        f =
            getFields d
    in
    if f.years >= 1000000 then
        "???? y"

    else if f.years >= 10 then
        String.fromInt f.days ++ "y"

    else if f.years > 0 then
        String.fromInt f.years ++ "y " ++ String.fromInt f.days ++ "d"

    else if f.days > 0 then
        String.fromInt f.days ++ "d " ++ String.fromInt f.hours ++ "h"

    else if f.hours > 0 then
        String.fromInt f.hours ++ "h " ++ String.fromInt f.mins ++ "m"

    else if f.mins > 0 then
        String.fromInt f.mins ++ "m " ++ String.fromInt f.secs ++ "s"

    else
        String.fromInt f.secs ++ "s " ++ String.fromInt f.millis ++ "ms"


pad0 : Int -> Int -> String
pad0 n =
    String.fromInt >> String.padLeft n '0'


toString : Duration -> String
toString =
    toStringColons defaultColonsConfig


type alias ColonsConfig =
    { showMillis : Bool }


defaultColonsConfig =
    ColonsConfig False


toStringColons : ColonsConfig -> Duration -> String
toStringColons c d =
    let
        f =
            getFields d
    in
    (if f.years > 1000 then
        -- [yyyy, ddd, hh, mm, ss]
        [ "????", "???", "??", "??", "??" ]
            ++ (if c.showMillis then
                    [ "????" ]

                else
                    []
               )

     else
        (if f.years > 0 then
            [ String.fromInt f.years, pad0 3 f.days ]

         else if f.days > 0 then
            [ String.fromInt f.days ]

         else
            []
        )
            ++ [ pad0 2 f.hours, pad0 2 f.mins, pad0 2 f.secs ]
            ++ (if c.showMillis then
                    [ pad0 4 f.millis ]

                else
                    []
               )
    )
        |> String.join ":"


zero : Duration
zero =
    Duration 0
