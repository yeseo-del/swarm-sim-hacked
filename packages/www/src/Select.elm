module Select exposing (STab(..), bulkBuyableItems, isTabAscendTriggered, isTabWatchTriggered, isUnitSidebarAscendTriggered, isUnitSidebarWatchTriggered, itemBySlug, itemWatchedOrder, spellBySlug, spellsByTab, tabBySlug, tabToSlug, unitBySlug, unitCountsByTab, unitCountsDiff, unitsByTab, unwrapTab, upgradesByUnitId)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Game.Watched as Watched exposing (Watched)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Maybe.Extra
import RemoteData exposing (RemoteData)
import Session exposing (Session)
import Time exposing (Posix)


type STab
    = NormalTab Tab
    | AllTab


tabBySlug : String -> Game -> UnitsSnapshot -> Maybe STab
tabBySlug tabSlug game snapshot =
    -- The "/tab/all" url is a bit of a special case: show all units. The
    -- tab itself isn't visible, though - without a unit to show or language
    -- identifiers, it has no `Tab` instance. We special-case it here.
    --
    -- "all" is the only case where the tab is `RemoteData.Success None`;
    -- all other nonexistent tabs are `RemoteData.Failure`s.
    if tabSlug == "all" then
        Just AllTab

    else
        game
            |> Game.gameData
            |> GameData.tabBySlug tabSlug
            |> Maybe.Extra.filter (isTabVisible snapshot)
            |> Maybe.map NormalTab


tabToSlug : STab -> String
tabToSlug stab =
    case stab of
        AllTab ->
            "all"

        NormalTab tab ->
            Tab.name tab


unwrapTab : STab -> Maybe Tab
unwrapTab stab =
    case stab of
        NormalTab tab ->
            Just tab

        AllTab ->
            Nothing


unitBySlug : String -> Game -> UnitsSnapshot -> Maybe ( Unit, Decimal )
unitBySlug unitSlug game snapshot =
    game
        |> Game.gameData
        |> GameData.unitBySlug unitSlug
        |> Maybe.Extra.filter (isUnitVisible snapshot)
        |> Maybe.map (\u -> ( u, UnitsSnapshot.count u.id snapshot ))


spellBySlug : String -> Game -> UnitsSnapshot -> Maybe Spell
spellBySlug spellSlug game snapshot =
    game
        |> Game.gameData
        |> GameData.spellBySlug spellSlug
        |> Maybe.Extra.filter (isSpellVisible snapshot)


itemBySlug : String -> Game -> UnitsSnapshot -> Maybe Item
itemBySlug itemSlug game snapshot =
    game
        |> Game.gameData
        |> GameData.itemBySlug itemSlug
        |> Maybe.Extra.filter (isItemVisible snapshot)


unitsByTab : STab -> Game -> UnitsSnapshot -> List Unit
unitsByTab stab game snapshot =
    (case stab of
        AllTab ->
            game
                |> Game.gameData
                |> GameData.units

        NormalTab tab ->
            game
                |> Game.gameData
                |> GameData.unitsByTab tab
                -- territory unit sorting - highest empowers go first.
                -- if empowers are equal (or missing, as with most units),
                -- keep initial order.
                |> List.indexedMap Tuple.pair
                |> List.sortBy (\( i, u ) -> ( -1 * unitEmpower game u, i ))
                |> List.map Tuple.second
    )
        |> List.filter (isUnitVisible snapshot)


unitCountsByTab : STab -> Game -> UnitsSnapshot -> List ( Unit, Decimal )
unitCountsByTab stab game snapshot =
    unitsByTab stab game snapshot
        |> List.map (\u -> ( u, UnitsSnapshot.count u.id snapshot ))


unitEmpower : Game -> Unit -> Int
unitEmpower game u =
    let
        upgradeName =
            Unit.idToString u.id ++ "empower"
    in
    Game.upgradeCountName upgradeName game |> Decimal.toFloat |> floor


unitCountsDiff : ( Game, UnitsSnapshot ) -> ( Game, UnitsSnapshot ) -> List ( Unit, Result ( Decimal, Decimal ) Decimal )
unitCountsDiff ( game1, snap1 ) ( game2, snap2 ) =
    let
        isVisible u =
            isUnitVisible snap1 u || isUnitVisible snap2 u

        countDiff u =
            let
                count1 =
                    UnitsSnapshot.count u.id snap1

                count2 =
                    UnitsSnapshot.count u.id snap2
            in
            ( u
            , if Decimal.isAlmostEqual 1.001 count1 count2 then
                Ok count1

              else
                Err ( count1, count2 )
            )
    in
    game1
        |> Game.gameData
        |> GameData.units
        |> List.filter isVisible
        |> List.map countDiff


spellsByTab : STab -> Game -> UnitsSnapshot -> List Spell
spellsByTab stab game snapshot =
    case stab of
        AllTab ->
            game
                |> Game.gameData
                |> GameData.spells
                |> List.filter (isSpellVisible snapshot)

        NormalTab tab ->
            game
                |> Game.gameData
                |> GameData.spellsByTab tab
                |> List.filter (isSpellVisible snapshot)


upgradesByUnitId : Unit.Id -> Game -> UnitsSnapshot -> List Upgrade
upgradesByUnitId id game snapshot =
    game
        |> Game.gameData
        |> GameData.upgradesByUnitId id
        |> List.filter (isUpgradeVisible snapshot)


isUnitVisible snapshot u =
    UnitsSnapshot.isUnitVisible u.id snapshot


isSpellVisible snapshot u =
    UnitsSnapshot.isSpellVisible u.id snapshot


isItemVisible snapshot u =
    UnitsSnapshot.isItemVisible (Item.id u) snapshot


isUpgradeIdVisible snapshot id =
    UnitsSnapshot.isUpgradeVisible id snapshot


isUpgradeVisible snapshot u =
    isUpgradeIdVisible snapshot u.id


isTabVisible snapshot t =
    UnitsSnapshot.isTabVisible t.id snapshot


isItemWatchTriggered : Float -> Game -> UnitsSnapshot -> Item -> Bool
isItemWatchTriggered pct game snapshot item =
    itemWatchedOrder pct game snapshot item
        |> Result.map .isPossible
        |> Result.withDefault False


itemWatchedOrder : Float -> Game -> UnitsSnapshot -> Item -> Result String Order
itemWatchedOrder pct game snapshot item =
    case Game.watched item game of
        Just Watched.Cost1 ->
            Order.fromPercent game snapshot item (pct * 1)

        Just Watched.Cost2 ->
            Order.fromPercent game snapshot item (pct * 1 / 2)

        Just Watched.Cost4 ->
            Order.fromPercent game snapshot item (pct * 1 / 4)

        _ ->
            Err "watched-order not possible"


{-| Is a watch triggered for this unit's upgrades, or this unit itself?
-}
isUnitSidebarWatchTriggered : Unit -> Game -> UnitsSnapshot -> Bool
isUnitSidebarWatchTriggered unit game snapshot =
    Item.Unit unit
        :: (List.map Item.Upgrade <| upgradesByUnitId unit.id game snapshot)
        |> List.any (isItemWatchTriggered 1 game snapshot)


isTabWatchTriggered : Tab -> Game -> UnitsSnapshot -> Bool
isTabWatchTriggered tab game snapshot =
    unitsByTab (NormalTab tab) game snapshot
        |> List.any (\u -> isUnitSidebarWatchTriggered u game snapshot)


isUnitSidebarAscendTriggered : Unit -> Game -> UnitsSnapshot -> Bool
isUnitSidebarAscendTriggered unit game snapshot =
    unit.onBuy.ascend
        && (Order.fromPercent game snapshot (Item.Unit unit) 1
                |> Result.map .isPossible
                |> Result.withDefault False
           )


isTabAscendTriggered : Tab -> Game -> UnitsSnapshot -> Bool
isTabAscendTriggered tab game snapshot =
    unitsByTab (NormalTab tab) game snapshot
        |> List.any (\u -> isUnitSidebarAscendTriggered u game snapshot)


bulkBuyableItems : Float -> Game -> UnitsSnapshot -> List Item
bulkBuyableItems pct game snapshot =
    game
        |> Game.gameData
        |> GameData.items
        |> List.filter (\i -> UnitsSnapshot.isItemVisible (Item.id i) snapshot)
        |> List.filter (isItemWatchTriggered pct game snapshot)
        |> List.filter isBulkBuyable


isBulkBuyable : Item -> Bool
isBulkBuyable item =
    case item of
        Item.Upgrade u ->
            u.isBulkBuyable

        _ ->
            True
