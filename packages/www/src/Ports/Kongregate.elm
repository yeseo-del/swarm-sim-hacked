port module Ports.Kongregate exposing
    ( LoginData
    , kongregateInit
    , kongregateLogin
    , kongregatePurchaseItemsReq
    , kongregatePurchaseItemsRes
    , kongregateRequestUserItemListReq
    , kongregateRequestUserItemListRes
    , kongregateShowKredPurchaseDialog
    , kongregateShowRegistrationBox
    , kongregateSubmitStats
    )

import Json.Decode as D


{-| Load kongregate javascript files
-}
port kongregateInit : () -> Cmd msg


type alias LoginData =
    { userId : String
    , username : String
    , gameAuthToken : String
    , isGuest : Bool
    }


{-| New kongregate login information. Also, kongregateInit finished
-}
port kongregateLogin : (LoginData -> msg) -> Sub msg


port kongregateShowRegistrationBox : () -> Cmd msg


port kongregateShowKredPurchaseDialog : () -> Cmd msg


{-| kongregate.mtx.purchaseItems
<https://www.kongregate.com/developer_center/docs/en/microtransaction-client-api>
-}
port kongregatePurchaseItemsReq : String -> Cmd msg


port kongregatePurchaseItemsRes : ({ item : String, success : Bool } -> msg) -> Sub msg


{-| kongregate.mtx.requestUserItemList
<https://www.kongregate.com/developer_center/docs/en/microtransaction-client-api>

"requestUserItemListReq[uest]" is the dumbest function name ever. Sorry.
Explanation: I don't want to stray from kongregate API names, and
"fooReq/fooRes" is the port naming convention I've used most places.

-}
port kongregateRequestUserItemListReq : () -> Cmd msg


port kongregateRequestUserItemListRes : ({ success : Bool, data : D.Value } -> msg) -> Sub msg


port kongregateSubmitStats : D.Value -> Cmd msg
