module View.ItemDebug exposing (view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.ProductionGraph as ProductionGraph
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.OnBuy as OnBuy
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Polynomial exposing (Polynomial)
import Route.Feature as Feature
import Session exposing (Session)
import View.Lang as T exposing (Lang)


view : Session -> Game -> UnitsSnapshot -> List Item -> List (Html msg)
view session game snapshot items =
    if Feature.isActive Feature.Debug (Session.query session) then
        let
            graph =
                Game.graph game

            t =
                Duration.since { before = Game.snapshotted game, after = UnitsSnapshot.at snapshot }
        in
        [ div [ class "card" ]
            [ h6 [ class "card-header" ] [ text "ItemDebug view" ]
            , div [ class "card-body" ]
                [ p []
                    [ text "in the polynomials below, "

                    -- , br [] []
                    , text "t = "
                    , code [] [ text <| String.fromFloat (Duration.toSecs t) ]
                    , br [] []
                    , text "(alternately, t = "
                    , code [] [ text <| Duration.toStringDHMS t ]
                    , text " or "
                    , code [] [ text <| Duration.toString t ]
                    , text ")"
                    ]
                , ul []
                    (items
                        |> List.map
                            (\item ->
                                case item of
                                    Item.Unit unit ->
                                        [ viewPolynomialAndGraph session game snapshot unit ]

                                    Item.Spell spell ->
                                        []

                                    Item.Upgrade upgrade ->
                                        -- we don't have upgrade pages; this shouldn't happen
                                        []
                            )
                        |> List.concat
                    )
                , a [ target "_blank", href <| ProductionGraph.toDOTUrl graph ] [ text "full unit production graph" ]
                , hr [] []
                , text "on-buy effects:"
                , ul []
                    (items
                        |> List.map
                            (\item ->
                                case item of
                                    Item.Unit unit ->
                                        li [] (text (Item.name item) :: viewOnBuyDebug game snapshot item)
                                            :: (Game.gameData game
                                                    |> GameData.upgradesByUnit unit
                                                    |> List.map (\up -> li [] (text (Upgrade.name up) :: viewOnBuyDebug game snapshot (Item.Upgrade up)))
                                               )

                                    Item.Spell spell ->
                                        [ li [] (text (Item.name item) :: viewOnBuyDebug game snapshot item)
                                        ]

                                    Item.Upgrade upgrade ->
                                        -- we don't have upgrade pages; this shouldn't happen
                                        []
                            )
                        |> List.concat
                    )
                ]
            ]
        ]

    else
        []


viewPolynomialAndGraph : Session -> Game -> UnitsSnapshot -> Unit -> Html msg
viewPolynomialAndGraph session game snapshot unit =
    let
        lang =
            T.gameToLang session game

        graph =
            Game.graph game |> ProductionGraph.unitParents unit

        poly =
            Game.polynomials game |> Dict.get (Unit.name unit) |> Maybe.withDefault []

        coeffs =
            graph |> ProductionGraph.parentCoefficients |> Dict.get (Unit.name unit) |> Maybe.withDefault []
    in
    li []
        [ a [ target "_blank", href <| ProductionGraph.toDOTUrl graph ] [ text unit.slug ]
        , text " = "
        , code [] (viewPoly lang poly)

        -- graph has this information, and it uses lots of space
        --, ul []
        --    (coeffs
        --        |> List.map
        --            (\( parent, ks ) ->
        --                li []
        --                    [ text parent.slug
        --                    , text ": "
        --                    , text <| String.join " * " <| List.map (lang.str << T.DebugDecimal) <| ks
        --                    ]
        --            )
        --    )
        ]


viewPoly : Lang msg -> Polynomial -> List (Html msg)
viewPoly lang =
    let
        render : Int -> String -> Html msg
        render degree coeff =
            span
                (if coeff == "0" && degree /= 0 then
                    [ class "zeropoly" ]

                 else
                    []
                )
                (text coeff
                    :: (case degree of
                            0 ->
                                [ text "" ]

                            1 ->
                                [ text "t + " ]

                            _ ->
                                [ text " t", sup [] [ text <| String.fromInt degree ], text " + " ]
                       )
                )
    in
    Polynomial.formatCoefficients (T.DebugDecimal >> lang.str) >> List.indexedMap render >> List.reverse


viewOnBuyDebug : Game -> UnitsSnapshot -> Item -> List (Html msg)
viewOnBuyDebug game snapshot item =
    let
        body child label exprs =
            let
                ctxs =
                    exprs |> List.map (Parser.Stat.evalFullWithDefault Decimal.zero (Game.getGameVar game))
            in
            div []
                [ text child
                , text ", "
                , text label
                , ul []
                    (ctxs
                        |> List.map
                            (\ctx ->
                                li []
                                    [ ctx.out
                                        |> Decimal.toString
                                        |> text
                                    , ctx.exports
                                        |> Dict.toList
                                        |> List.map (\( n, d ) -> li [] [ text n, text ": ", text <| Decimal.toString d ])
                                        |> ul []
                                    ]
                            )
                    )
                ]
    in
    item
        |> Item.onBuy
        |> .all
        |> List.map
            (\onBuy ->
                case onBuy of
                    OnBuy.AddUnits child expr ->
                        body (Unit.idToString child) "add" [ expr ]

                    OnBuy.AddUnitsCooldown child expr cooldown ->
                        let
                            label =
                                "addCooldown: "
                                    ++ Duration.toString (Game.cooldownRemaining (UnitsSnapshot.at snapshot) cooldown game)
                                    ++ " (cooldown.group: "
                                    ++ cooldown.group
                                    ++ ")"
                        in
                        body (Unit.idToString child) label [ expr ]

                    OnBuy.MulUnits child expr ->
                        body (Unit.idToString child) "mul" [ expr ]

                    OnBuy.AddTime expr ->
                        body "ALL" "time" [ expr ]

                    OnBuy.AddRandomUnits args ->
                        body (Unit.idToString args.child) "addRandom" [ args.chance, args.count ]

                    OnBuy.Ascend ->
                        body "ALL" "ascend" []

                    OnBuy.Respec currency rate _ ->
                        body (Unit.idToString currency) "respec" []
            )
