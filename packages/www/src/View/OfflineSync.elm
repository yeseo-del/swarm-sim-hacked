module View.OfflineSync exposing (Msg(..), update, view)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.OfflineSync as OfflineSync
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Iso8601
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Select
import Session exposing (Session)
import View.Icon
import View.Lang as T exposing (Lang)


type Msg
    = UseLocalOverwriteRemote
    | UseRemoteOverwriteLocal
    | UseLocalWithLogout


update : Msg -> Session -> ( Session, Cmd Msg )
update msg session =
    case Session.offlineSyncConflict session of
        Just conflict ->
            let
                resolvedSession =
                    Session.clearOfflineSyncConflict session
            in
            case msg of
                UseLocalOverwriteRemote ->
                    -- No changes needed! The local save's already loaded, and we'll overwrite the remote one on save.
                    ( resolvedSession, Cmd.none )

                UseRemoteOverwriteLocal ->
                    -- Overwrite the local save with the remote one. No remote changes needed.
                    ( resolvedSession |> Session.setGame False conflict.remote.game, Cmd.none )

                UseLocalWithLogout ->
                    Session.logout resolvedSession

        _ ->
            -- If there's no save conflicts, nothing else here makes sense
            ( session, Cmd.none )


view : Session -> GameData -> OfflineSync.ConflictData -> List (Html Msg)
view session gameData conflict =
    let
        lang =
            T.gameDataToLang session gameData

        localSnap =
            Game.evalUnits (Session.safeNow conflict.local.game session) conflict.local.game

        remoteSnap =
            Game.evalUnits (Session.safeNow conflict.remote.game session) conflict.remote.game

        diff : List ( Unit, Result ( Decimal, Decimal ) Decimal )
        diff =
            Select.unitCountsDiff ( conflict.local.game, localSnap ) ( conflict.remote.game, remoteSnap )
    in
    [ div [ id OfflineSync.modalId, class "conflict card bg-warning", attribute "role" "dialog" ]
        [ div [ class "card-header" ]
            [ h5 [ class "card-title" ] [ View.Icon.fas "exclamation-triangle", text " ", lang.html T.ViewConflictTitle ]
            ]
        , div [ class "card-body" ]
            [ p [] [ lang.html T.ViewConflictDesc ]
            , hr [] []
            , div []
                [ viewDiffLine
                    [ lang.html T.ViewConflictExportLocal
                    , input [ readonly True, value conflict.local.encoded ] []
                    ]
                    [ lang.html T.ViewConflictExportRemote
                    , input [ readonly True, value conflict.remote.encoded ] []
                    ]
                , viewTextDiffLine
                    (lang.str <| T.ViewConflictGameCreated <| Game.created <| conflict.local.game)
                    (lang.str <| T.ViewConflictGameCreated <| Game.created <| conflict.remote.game)
                , viewTextDiffLine
                    (lang.str <| T.ViewConflictGameEncoded <| Game.encoded <| conflict.local.game)
                    (lang.str <| T.ViewConflictGameEncoded <| Game.encoded <| conflict.remote.game)
                , hr [] []
                , div [] (diff |> List.map (viewUnitDiffEntry session conflict.local.game >> div []))
                , hr [] []
                , viewDiffLine
                    [ div []
                        [ button [ class "btn btn-primary", attribute "aria-label" "close", onClick UseLocalOverwriteRemote ]
                            [ lang.html T.ViewConflictButtonLocal ]
                        ]
                    , div []
                        [ button [ class "btn btn-secondary", attribute "aria-label" "close", onClick UseLocalWithLogout ]
                            [ lang.html T.ViewConflictButtonLogout ]
                        ]
                    ]
                    [ div []
                        [ button [ class "btn btn-primary", attribute "aria-label" "close", onClick UseRemoteOverwriteLocal ]
                            [ lang.html T.ViewConflictButtonRemote ]
                        ]
                    ]
                ]
            ]
        ]
    ]


{-| A working non-breaking space - `&nbsp;`

<https://twitter.com/rtfeldman/status/767263564214120448>

-}
nbsp : String
nbsp =
    "\u{00A0}"


viewDiffLine : List (Html msg) -> List (Html msg) -> Html msg
viewDiffLine a b =
    div []
        [ div [ class "bg-success", style "width" "100%" ]
            [ div [ style "float" "left" ] [ text <| "+" ++ nbsp ]
            , div [ style "float" "left" ] a
            , div [ style "clear" "left" ] []
            ]
        , div [ class "bg-info", style "width" "100%" ]
            [ div [ style "float" "left" ] [ text <| "−" ++ nbsp ]
            , div [ style "float" "left" ] b
            , div [ style "clear" "left" ] []
            ]
        ]


viewTextDiffLine : String -> String -> Html msg
viewTextDiffLine a b =
    if a == b then
        div [] [ text a ]

    else
        viewDiffLine [ text a ] [ text b ]


viewUnitDiffEntry : Session -> Game -> ( Unit, Result ( Decimal, Decimal ) Decimal ) -> List (Html msg)
viewUnitDiffEntry session game ( unit, diff ) =
    let
        lang =
            T.gameToLang session game

        -- in [text <| Unit.name unit ++ " " ++ Decimal.toString count]
        line count =
            [ span [ class "list-label" ]
                [ span [ class "list-icon-resource", class <| "icon-" ++ Unit.name unit ] []
                , lang.html <| T.ViewUnitSidebarUnitName unit.id
                , text ": "
                ]
            , span [] [ lang.html <| T.ViewUnitSidebarCount count ]
            ]
    in
    case diff of
        Ok count ->
            line count

        Err ( count1, count2 ) ->
            [ viewDiffLine (line count1) (line count2) ]
