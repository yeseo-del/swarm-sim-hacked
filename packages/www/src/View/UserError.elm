module View.UserError exposing (view)

import Game exposing (Game)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import PlayFab.Error
import PlayFab.User as User exposing (User)
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import View.Icon
import View.Lang as T exposing (Lang)


view : Session -> Game -> List (Html msg)
view session game =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session
    in
    case Session.userState session of
        RemoteData.Failure err ->
            [ div [ class "alert alert-danger" ]
                (case err of
                    PlayFab.Error.AuthError status json ->
                        [ p []
                            [ a [ class "dropdown-item", Route.href query Route.Login ]
                                [ View.Icon.fas "user-times", text " ", lang.html T.ViewUserErrorLoggedOut ]
                            ]
                        , p [] [ i [] [ lang.html T.ViewUserErrorLoggedOutRemember ] ]
                        ]

                    _ ->
                        [ a [ class "dropdown-item", Route.href query Route.Login ]
                            [ View.Icon.fas "user-times", text " ", code [] [ text <| PlayFab.Error.toString err ] ]
                        ]
                )
            ]

        _ ->
            []
