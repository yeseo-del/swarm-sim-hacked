module View.Header exposing (Msg(..), update, view, viewCss, viewLocalStorageIframe)

import Browser
import Browser.Navigation as Nav
import Fools
import FrameRate exposing (FrameRate)
import Game exposing (Game)
import Game.Achievement
import Game.Theme as Theme exposing (Theme)
import GameData exposing (GameData)
import GameData.Achievement as Achievement exposing (Achievement)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import Page.Achievements exposing (toProgdesc)
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Config as Config
import Session exposing (Session)
import Session.Environment as Environment exposing (Environment)
import Session.ServiceWorker as ServiceWorker
import Time exposing (Posix)
import View.Icon
import View.Lang as T exposing (Lang)


type Msg
    = Refresh
    | AchievementToastClose


update : Msg -> Session -> ( Session, Cmd Msg )
update msg session =
    case msg of
        Refresh ->
            ( session, Nav.reload )

        AchievementToastClose ->
            case Session.game session of
                RemoteData.Success game ->
                    let
                        game1 =
                            Game.clearNewAchievements game
                    in
                    ( Session.setGame False game1 session, Cmd.none )

                _ ->
                    ( session, Cmd.none )


themeHref : Theme -> Attribute msg
themeHref theme =
    case theme of
        Theme.Default ->
            href "/css/bootstrap/bootstrap.min.css"

        _ ->
            href <| "/css/bootswatch/" ++ Theme.toString theme ++ "/bootstrap.min.css"


viewCss : Session -> List (Html msg)
viewCss session =
    let
        theme =
            session |> Session.game |> RemoteData.unwrap Theme.Default Game.theme
    in
    [ H.node "link" [ rel "stylesheet", themeHref theme ] [] ]
        ++ (if Fools.isOn session then
                [ H.node "link" [ rel "stylesheet", href "/css/kittens.css" ] [] ]

            else
                []
           )


{-| Allow JS to communicate with other subdomains via window.postMessage.

This is how elm.swarmsim.com retrieves saved games from www.swarmsim.com.

-}
viewLocalStorageIframe : List (Html msg)
viewLocalStorageIframe =
    [ H.iframe
        [ id "www-localstorage"

        -- , src "./localstorage.html?log"
        -- , src "./localstorage.html"
        -- , src "http://localhost:9000/localstorage.html?log"
        -- , src "http://localhost:9000/localstorage.html"
        -- , src "https://localhost:9001/localstorage.html?log"
        -- , src "https://localhost:9001/localstorage.html"
        -- , src "https://staging.swarmsim.com/localstorage.html?log"
        -- , src "https://staging.swarmsim.com/localstorage.html"
        -- , src "https://www.swarmsim.com/localstorage.html?log"
        , src "https://www.swarmsim.com/localstorage.html"
        , style "display" "none"
        ]
        []
    ]


view : Session -> GameData -> List (Html Msg)
view session gameData =
    let
        lang =
            T.gameDataToLang session gameData

        query =
            session |> Session.query

        fps : Maybe T.Text
        fps =
            Session.frameRate session
                |> Maybe.map
                    (\fr ->
                        T.FrameRate (FrameRate.toFPS fr)
                            { avg = FrameRate.toAverage fr
                            , max = FrameRate.maximum fr
                            }
                    )

        date : String -> String
        date mmdd =
            case ( Session.now session, Session.timezone session ) of
                ( Just now, Just tz ) ->
                    String.fromInt (Time.toYear tz now) ++ "/" ++ mmdd

                _ ->
                    ""
    in
    -- [ H.node "link" [ rel "stylesheet", href "/css/bootstrap/bootstrap.css" ] []
    -- [ H.node "link" [ rel "stylesheet", href "/css/bootswatch/cyborg/bootstrap.css" ] []
    [ div [ class "container header" ]
        [ div [ class "navbar", class "navbar-light", class "bg-light", class "border", class "rounded" ]
            [ div [ class "navbar-brand" ]
                [ a [ class "navbar-brand page-title", Route.href query Route.Home, tabindex 1 ] [ span [] [ lang.html <| T.Header ] ]
                , a [ class "navbar-brand header-version", class "text-muted", Route.href query Route.Changelog ] [ small [] [ text <| Session.version session ] ]
                , viewServiceWorker lang (Session.serviceWorker session)
                , span [ class "navbar-text", class "text-muted" ] [ small [] [ Maybe.Extra.unwrap (text "") lang.html fps ] ]
                ]
            ]
        , code [ class "environment-label" ] <|
            case Session.environment session of
                Environment.Production ->
                    []

                env ->
                    [ text "(environment: ", text <| Environment.toString env, text ")" ]
        , case Fools.status session of
            Fools.Invited ->
                div [ class "alert alert-info" ]
                    [ div [ class "close", attribute "data-dismiss" "alert" ] [ text "×" ]
                    , div [ class "float-right" ] [ text <| date "04/01" ]
                    , p [ title <| lang.str T.ViewFoolsInvitedLol ] [ b [] [ lang.html T.ViewFoolsInvitedTitle ] ]
                    , p [] [ lang.html T.ViewFoolsInvitedBody ]
                    , p [] [ a [ Route.href (Config.insert Config.Fools "on" query) Route.Home ] [ lang.html T.ViewFoolsInvitedLink ] ]
                    ]

            Fools.On ->
                div [ class "alert alert-info" ]
                    [ div [ class "close", attribute "data-dismiss" "alert" ] [ text "×" ]
                    , div [ class "float-right" ] [ text <| date "04/01" ]
                    , p [ title <| lang.str T.ViewFoolsOnLol ] [ b [] [ lang.html T.ViewFoolsOnTitle ] ]
                    , p [] [ lang.html T.ViewFoolsOnBody ]
                    , p [] [ a [ Route.href (Config.insert Config.Fools "invited" query) Route.Home ] [ text "" ] ]
                    ]

            Fools.After ->
                div [ class "alert alert-info" ]
                    [ div [ class "close", attribute "data-dismiss" "alert" ] [ text "×" ]
                    , div [ class "float-right" ] [ text <| date "04/02" ]
                    , p [] [ lang.html T.ViewFoolsAfterTitle ]
                    , p [] [ a [ Route.href (Config.insert Config.Fools "on" query) Route.Home ] [ lang.html T.ViewFoolsAfterLink ] ]
                    ]

            Fools.Off ->
                div [] []
        ]
    ]
        ++ (case Session.game session of
                RemoteData.Success game ->
                    [ div [ class "achievement-toast-container" ] <|
                        case Game.newAchievements game of
                            Nothing ->
                                [ div [ class "achievement-hide" ] [] ]

                            Just new ->
                                [ viewAchievementToast session game new ]
                    ]

                _ ->
                    []
           )


viewAchievementToast : Session -> Game -> ( Posix, Achievement, List Achievement ) -> Html Msg
viewAchievementToast session game ( _, achievement, _ ) =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        progdesc =
            toProgdesc (Game.gameData game)
                (Game.achievementProgress game achievement)

        fade =
            Game.fadeNewAchievement (Session.safeNow game session) game
    in
    -- Bootstrap has a toast component - but I want achievement toasts to look similar to the achievement list page, so I'm cooking up my own toast
    div
        [ class "container alert alert-success achievement-toast"
        , classList [ ( "achievement-show", not fade ), ( "achievement-hide", fade ) ]
        , attribute "role" "status"
        , attribute "aria-live" "polite"
        ]
        [ div [ class "row" ]
            [ div [ class "col-11" ] []
            , div [ class "col-1 close" ]
                [ button [ class "btn btn-link", style "padding" "0", style "margin" "0", onClick AchievementToastClose ] [ text "×" ]
                ]
            ]
        , div [ class "row" ]
            [ div [ class "icon col-2" ] [ View.Icon.fas "trophy" ]
            , div [ class "body col-8" ]
                [ div [] [ small [] [ i [] [ lang.html T.ViewAchievementsEntryUnlocked ] ] ]
                , h3 [] [ a [ class "alert-link", Route.href query Route.Achievements ] [ lang.html <| T.AchievementName achievement.id ] ]
                , div [] [ lang.html <| T.AchievementDesc achievement.id progdesc ]
                , div [] [ small [] [ i [] [ lang.html <| T.AchievementLongDesc achievement.id ] ] ]
                ]
            , div [ class "col-2 points" ] [ text <| "+" ++ String.fromInt achievement.points ]
            ]
        ]


viewServiceWorker : Lang Msg -> Maybe ServiceWorker.Event -> Html Msg
viewServiceWorker lang lastEvent =
    case lastEvent of
        Nothing ->
            span [] []

        Just ServiceWorker.UpdateFound ->
            -- this happens on first run too, when there's no update
            -- available and it's just caching. So, don't show an
            -- "update downloading" message.
            span [] []

        Just ServiceWorker.OfflineInstalled ->
            -- show a "hey you can play offline now" message?
            -- nah, should have docs for that visible all the time
            span [] []

        Just ServiceWorker.UpdateInstalled ->
            button [ class "navbar-brand btn btn-link btn-primary", onClick Refresh ]
                [ View.Icon.fas "wrench", lang.html T.ServiceWorkerUpdateDownloaded ]
