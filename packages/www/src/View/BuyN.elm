module View.BuyN exposing (Msg(..), update, view, viewHelpModal, viewProgress)

import Browser.Navigation as Nav
import Decimal exposing (Decimal)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.BuyOrder
import Game.Order as Order exposing (Order)
import Game.Order.Progress as Progress exposing (OrderProgress, Progress)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Route exposing (Route)
import Session exposing (Session)
import View.Icon
import View.Lang as T exposing (Lang)


type Msg
    = GotInput Item String
    | ClickedBuy Order
    | ClickedAscend Order
    | CostExpanded
    | CostCollapsed


update : Route -> Msg -> Game -> Session -> ( Session, Cmd Msg )
update route msg game0 session =
    case msg of
        GotInput item str ->
            let
                game =
                    Game.setBuyN item str game0
            in
            ( Session.setGame False game session, qsRedirectN session route )

        ClickedBuy order ->
            if order.isPossible then
                case game0 |> clickedBuy session order of
                    Ok ( game, cmd ) ->
                        ( Session.setGame True game session, cmd )

                    Err err ->
                        ( Session.pushError err session, Cmd.none )

            else
                ( session, Cmd.none )

        ClickedAscend order ->
            -- like ClickedBuy, but also redirect
            let
                pushUrl =
                    Route.Home |> Route.pushUrl (Session.nav session) (Session.query session)
            in
            case game0 |> clickedBuy session order of
                Ok ( game, cmd ) ->
                    ( Session.setGame True game session, Cmd.batch [ cmd, pushUrl ] )

                Err err ->
                    ( Session.pushError err session, Cmd.none )

        CostExpanded ->
            let
                game =
                    game0 |> Game.setCostUI Game.FancyCostUI
            in
            ( Session.setGame False game session, Cmd.none )

        CostCollapsed ->
            let
                game =
                    game0 |> Game.setCostUI Game.SimpleCostUI
            in
            ( Session.setGame False game session, Cmd.none )


clickedBuy : Session -> Order -> Game -> Result String ( Game, Cmd Msg )
clickedBuy session order game =
    game
        |> Game.snapshotUnits (Session.safeNow game session)
        |> Game.BuyOrder.buy order
        |> Result.map (Session.persistWrite session)


{-| Redirect to remove `?n=...` from the query string, if necessary.

No redirect if no `?n=...`

-}
qsRedirectN : Session -> Route -> Cmd Msg
qsRedirectN session route =
    case route of
        Route.Unit tab unit ns ->
            case ns.n of
                Nothing ->
                    Cmd.none

                Just n ->
                    Route.unit tab unit |> Route.pushUrl (Session.nav session) (Session.query session)

        _ ->
            Cmd.none


helpModalId =
    "help-modal"


view : Session -> Game -> UnitsSnapshot -> Result String Order -> { tabindex : Int } -> Maybe (Html Msg)
view session game snap orderResult ti =
    case orderResult of
        Err _ ->
            Nothing

        Ok order ->
            Just <|
                let
                    view_ u =
                        if u.onBuy.ascend then
                            viewAscend session game order ti

                        else if u.onBuy.respec |> List.isEmpty |> not then
                            viewRespec session game order ti

                        else
                            viewBuyN session game order ti
                in
                case order.item of
                    Item.Unit u ->
                        view_ u

                    Item.Upgrade u ->
                        view_ u

                    Item.Spell s ->
                        view_ s


viewAscend : Session -> Game -> Order -> { tabindex : Int } -> Html Msg
viewAscend session game order ti =
    let
        lang =
            T.gameToLang session game

        button_ =
            if order.isPossible then
                button
                    [ class "btn btn-success buy-button"
                    , type_ "button"
                    , onClick <| ClickedAscend order
                    , tabindex ti.tabindex
                    ]
                    [ View.Icon.fas "rocket", text " ", lang.html T.ViewUnitAscend ]

            else
                button
                    [ class "btn btn-primary buy-button disabled"
                    , disabled True
                    , tabindex ti.tabindex
                    ]
                    [ View.Icon.fas "rocket", text " ", lang.html T.ViewUnitAscend ]
    in
    div []
        [ p [] [ lang.html T.ViewAscendPlot ]
        , p [] [ lang.html <| T.ViewAscendBuyCost order.count order.cost <| Game.getGameVar game ]
        , div [] (viewAscendCost session game order.item)
        , button_
        ]


viewAscendCost : Session -> Game -> Item -> List (Html msg)
viewAscendCost session game item =
    -- https://getbootstrap.com/docs/4.2/components/progress/
    let
        progress =
            Session.orderProgress (Item.name item) session
    in
    case progress of
        Just [ p ] ->
            case GameData.unitByName (Unit.idToString p.currency) (Game.gameData game) of
                Just currency ->
                    case ( currency.maxCount, p.status ) of
                        ( Just capExpr, Progress.CostNotMet pct mduration ) ->
                            let
                                cap : Decimal
                                cap =
                                    Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) capExpr
                            in
                            if Decimal.lt cap p.cost then
                                -- the cost is above our max energy - fancy progressbar that shows this
                                viewAscendCostExceedsCap session game p { pct = pct, cap = cap }

                            else
                                -- the cost is below our max energy - normal progressbar
                                viewCost2 session game progress

                        _ ->
                            viewCost2 session game progress

                Nothing ->
                    viewCost2 session game progress

        _ ->
            viewCost2 session game progress


viewAscendCostExceedsCap : Session -> Game -> Progress -> { pct : Float, cap : Decimal } -> List (Html msg)
viewAscendCostExceedsCap session game progress { pct, cap } =
    -- fancy progressbar that shows cap
    let
        lang =
            T.gameToLang session game

        percent100 =
            pct * 100

        capCostDiff : Decimal
        capCostDiff =
            Decimal.sub progress.cost cap

        capCostPercent =
            Decimal.div capCostDiff progress.cost
                |> Decimal.toFloat
                |> (*) 100

        capGapPercent =
            100 - percent100 - capCostPercent
    in
    [ div [ class "progress ascend-progress" ]
        [ div
            [ class "progress-bar"
            , style "width" (String.fromFloat percent100 ++ "%")
            , style "text-align" "left"
            , style "z-index" "1"
            , attribute "role" "progressbar"
            , attribute "aria-valuenow" (String.fromFloat percent100)
            , attribute "aria-valuemax" "100"
            ]
            [ lang.html <| T.ViewAscendProgressCapDiff capCostDiff ]
        , div
            [ class "progress-bar bg-danger"
            , style "width" (String.fromFloat capGapPercent ++ "%")
            ]
            []
        , div
            [ class "progress-bar bg-warning"
            , style "width" (String.fromFloat capCostPercent ++ "%")
            ]
            []
        ]
    ]


viewRespec : Session -> Game -> Order -> { tabindex : Int } -> Html Msg
viewRespec session game order ti =
    let
        lang =
            T.gameToLang session game

        button_ =
            if order.isPossible then
                button
                    [ class "btn btn-success buy-button"
                    , onClick <| ClickedBuy order
                    , tabindex ti.tabindex
                    ]
                    [ lang.html <| T.ViewUnitBuyN order.count ]

            else
                button
                    [ class "btn btn-primary buy-button disabled"
                    , disabled True
                    , tabindex ti.tabindex
                    ]
                    [ lang.html <| T.ViewUnitCantBuy ]
    in
    div []
        [ case Game.costUI game of
            Game.SimpleCostUI ->
                p [] (viewCost lang order False ++ [ button [ class "btn btn-link", onClick CostExpanded ] [ View.Icon.fas "chevron-down" ] ])

            Game.FancyCostUI ->
                div []
                    (viewCost lang order True
                        ++ [ button [ class "btn btn-link", onClick CostCollapsed ] [ View.Icon.fas "chevron-up" ] ]
                        ++ viewCost2 session game (Session.orderProgress (Item.name order.item) session)
                    )
        , button_
        ]


viewBuyN : Session -> Game -> Order -> { tabindex : Int } -> Html Msg
viewBuyN session game order ti =
    let
        lang =
            T.gameToLang session game

        ( inputText, request ) =
            Game.buyN order.item game

        isRequestValid =
            case order.status of
                Err _ ->
                    False

                Ok _ ->
                    True

        buyButton =
            if order.isPossible then
                button
                    [ class "btn btn-secondary buy-button"

                    -- , onClick <| ClickedBuy order
                    , type_ "submit"
                    , tabindex -1
                    ]
                    [ lang.html <| T.ViewUnitBuyN order.count ]

            else
                button
                    [ class "btn btn-outline-secondary buy-button disabled"
                    , type_ "button"
                    , disabled True
                    , tabindex -1
                    ]
                    [ lang.html <| T.ViewUnitCantBuy ]

        selectorBtn =
            div
                [ class "btn btn-outline-secondary dropdown-toggle"
                , type_ "button"
                , attribute "data-toggle" "dropdown"
                , attribute "aria-haspopup" "true"
                ]
                []

        selectorOptions =
            [ "1%", "25%", "100%", "1" ]

        selectorMenu =
            div [ class "dropdown-menu" ]
                ((inputText
                    -- inputText goes first so form submit (enter key) cooperates
                    :: (selectorOptions |> List.filter ((/=) inputText))
                    |> List.map
                        (\val ->
                            button
                                [ class "dropdown-item"

                                -- don't submit the surrounding form, we're only changing the selection
                                , type_ "button"
                                , classList [ ( "active", val == inputText ) ]
                                , onClick <| GotInput order.item val
                                ]
                                [ text val ]
                        )
                 )
                    ++ [ button
                            [ class "dropdown-item"
                            , type_ "button"
                            , attribute "data-toggle" "modal"
                            , attribute "data-target" ("#" ++ helpModalId)
                            ]
                            [ View.Icon.fas "question-circle", text " ", lang.html T.ViewBuyNFormatLink ]
                       ]
                )

        textInput =
            input
                [ type_ "text"
                , class "form-control"
                , classList [ ( "is-invalid", not isRequestValid ) ]
                , value inputText
                , onInput <| GotInput order.item
                , tabindex ti.tabindex
                ]
                []
    in
    div []
        [ case Game.costUI game of
            Game.SimpleCostUI ->
                p [] (viewCost lang order False ++ [ button [ class "btn btn-link", onClick CostExpanded ] [ View.Icon.fas "chevron-down" ] ])

            Game.FancyCostUI ->
                div []
                    (viewCost lang order True
                        ++ [ button [ class "btn btn-link", onClick CostCollapsed ] [ View.Icon.fas "chevron-up" ] ]
                        ++ viewCost2 session game (Session.orderProgress (Item.name order.item) session)
                    )
        , H.form
            [ class "input-group"
            , onSubmit <| ClickedBuy order
            ]
            [ div [ class "dropdown input-group-prepend" ] [ selectorBtn, selectorMenu ]

            -- careful: textInput's surrounding elements must be consistent,
            -- or you'll lose focus while editing
            , textInput
            , div [ class "input-group-append" ] [ buyButton ]
            ]
        ]


viewCost : Lang msg -> Order -> Bool -> List (Html msg)
viewCost lang order min =
    let
        -- cost displays a minimum of one
        ( count, cost ) =
            if Decimal.lt (Item.resToDec order.count) Decimal.one then
                ( Ok 1, order.costNext )

            else
                ( order.count, order.cost )
    in
    if min then
        [ lang.html <| T.ViewCostMin count (Item.id order.item) ]

    else
        [ case order.item of
            Item.Unit u ->
                lang.html <| T.ViewUnitBuyCost count u.id cost

            Item.Upgrade u ->
                lang.html <| T.ViewUnitUpgradeBuyCost order.costNext

            Item.Spell s ->
                lang.html <| T.ViewSpellBuyCost count s.id cost
        ]


viewCost2 : Session -> Game -> Maybe OrderProgress -> List (Html msg)
viewCost2 session game progress =
    let
        lang =
            T.gameToLang session game
    in
    case progress of
        Nothing ->
            [ View.Icon.loading ]

        Just ps ->
            ps
                |> List.map
                    (\p ->
                        case p.status of
                            Progress.CostMet pct ->
                                -- div [] [ Debug.toString ( p.currency, pct, True ) |> text ]
                                let
                                    pctStr =
                                        (if pct > 0.95 then
                                            100

                                         else
                                            pct * 100
                                        )
                                            |> String.fromFloat
                                in
                                div [ class "progress" ]
                                    [ div
                                        [ class "progress-bar bg-success"
                                        , style "width" (pctStr ++ "%")
                                        , style "text-align" "left"
                                        , attribute "role" "progressbar"
                                        , attribute "aria-valuenow" pctStr
                                        , attribute "aria-valuemax" "100"
                                        ]
                                        [ lang.html <| T.ViewCostMet p pct
                                        ]
                                    ]

                            Progress.CostNotMet pct duration ->
                                -- div [] [ Debug.toString ( p.currency, pct, False ) |> text ]
                                let
                                    pctStr =
                                        (if pct > 0.99 then
                                            100

                                         else
                                            pct * 100
                                        )
                                            |> String.fromFloat

                                    tailStr =
                                        (if pct > 0.99 then
                                            0

                                         else
                                            (1 - pct) * 100
                                        )
                                            |> String.fromFloat

                                    route : Maybe Route
                                    route =
                                        GameData.unitByName (Unit.idToString p.currency) (Game.gameData game)
                                            |> Maybe.andThen
                                                (\u ->
                                                    case u.cost of
                                                        [] ->
                                                            -- don't link to unbuyable units
                                                            Nothing

                                                        _ ->
                                                            Just <|
                                                                Route.Unit (Tab.idToString u.tab)
                                                                    u.slug
                                                                    { n = Just <| "@" ++ Decimal.toString p.cost }
                                                )
                                in
                                div
                                    [ class "progress" ]
                                    [ div
                                        [ class "progress-bar"
                                        , style "width" (pctStr ++ "%")
                                        , style "text-align" "left"
                                        , style "z-index" "1"
                                        , attribute "role" "progressbar"
                                        , attribute "aria-valuenow" pctStr
                                        , attribute "aria-valuemax" "100"
                                        ]
                                        [ (case route of
                                            Nothing ->
                                                span []

                                            Just r ->
                                                a [ Route.href (Session.query session) r ]
                                          )
                                            [ lang.html <| T.ViewCostNotMet p pct <| Maybe.map Progress.remaining duration ]
                                        ]
                                    , div
                                        [ class "progress-bar bg-danger"
                                        , style "width" (tailStr ++ "%")
                                        ]
                                        []
                                    ]
                    )


viewHelpModal : T.Lang msg -> Html msg
viewHelpModal lang =
    -- https://getbootstrap.com/docs/4.2/components/modal/
    let
        example n =
            -- TODO link examples
            -- a [ href <| "?n=" ++ Url.percentEncode n ] [ text <| "\"" ++ n ++ "\"" ]
            text <| "\"" ++ n ++ "\""
    in
    div [ class "modal fade", id helpModalId, tabindex -1, attribute "role" "dialog", attribute "aria-hidden" "true" ]
        [ div [ class "modal-dialog", attribute "role" "document" ]
            [ div [ class "modal-content" ]
                [ div [ class "modal-header" ]
                    [ h5 [ class "modal-title" ] [ View.Icon.fas "question-circle", text " ", lang.html T.ViewBuyNFormatTitle ]
                    , button [ class "close", attribute "data-dismiss" "modal", attribute "aria-label" "close" ] [ text "×" ]
                    ]
                , div [ class "modal-body" ]
                    [ ul [ class "list-group list-group-flush" ]
                        [ li [ class "list-group-item" ] [ example "10%", T.text " buys as many as possible with up to 10% of your money." ]
                        , li [ class "list-group-item" ] [ example "123", T.text " buys 123 ", i [] [ text "before" ], text " considering twin upgrades, or fails to buy anything. If you have 1 twin upgrade, this will buy 246." ]
                        , li [ class "list-group-item" ] [ example "=123", T.text " buys 123 or more ", i [] [ text "after" ], text " considering twin upgrades, rounded up, or fails to buy anything. If you have 1 twin upgrade, this will buy 124." ]
                        , li [ class "list-group-item" ] [ example "@123", T.text " buys the number needed to have a total of 123. If you already have 23, this will buy 100." ]
                        , li [ class "list-group-item" ]
                            [ T.text "Numbers with suffixes work - "
                            , example "23e9"
                            , s []
                                [ T.text ", "
                                , example "23 billion"
                                , T.text ", or "
                                , example "23b"
                                , T.text "."
                                ]
                            ]
                        ]
                    ]
                , div [ class "modal-footer" ]
                    [ button [ class "btn btn-primary", attribute "data-dismiss" "modal", attribute "aria-label" "close" ] [ lang.html T.ViewBuyNFormatClose ]
                    ]
                ]
            ]
        ]


viewProgress : { a | percent : Float, duration : Maybe Duration } -> Html msg
viewProgress progress =
    -- https://getbootstrap.com/docs/4.2/components/progress/
    let
        percent =
            progress.percent * 100 |> String.fromFloat
    in
    div [ class "progress" ]
        [ div
            [ class "progress-bar"
            , style "width" (percent ++ "%")
            , attribute "role" "progressbar"
            , attribute "aria-valuenow" percent
            , attribute "aria-valuemax" "100"
            ]
            [ progress.percent * 100 |> floor |> String.fromInt |> text
            , "%" |> text
            , case progress.duration of
                Just d ->
                    " - " ++ Duration.toString d |> text

                Nothing ->
                    "" |> text
            ]
        ]
