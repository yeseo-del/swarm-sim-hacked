module View.DescriptionVars exposing (Field(..), evalField, evalLabel, evalVar, unwrapField, unwrapLabel, unwrapVar, unwrapVar0)

{-| Parse stat-expressions for use in unit descriptions.

This is how we get the numbers in unit descriptions - expansion base/total %s, for example

-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Cost as Cost exposing (Cost)
import GameData.Item as Item exposing (Item)
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import List.Extra
import Parser.Stat


type Field
    = Prod
    | Cost String
    | Count
    | Velocity
    | MaxCount
    | OnBuyAddUnits String
    | OnBuyMulUnits String
    | OnBuyAddTime
    | OnBuyRandomMinimum
    | OnBuyRandomChance
    | OnBuyRandomCount
    | OnBuyAddUnitsCooldownDuration String
    | OnBuyAddUnitsCooldownRemaining String
    | OnBuyAddUnitsCooldownCount String
    | OnBuyRespecRate


getExpr : Game -> Item -> Field -> Result String Parser.Stat.Expr
getExpr game item field =
    case field of
        Cost name ->
            item
                |> Item.cost
                |> List.Extra.find (Tuple.first >> Unit.idToString >> (==) name)
                |> Maybe.map (Tuple.second >> Cost.toExpr)
                |> Result.fromMaybe ("no such item.cost: " ++ name)

        Prod ->
            case item of
                Item.Unit unit ->
                    unit.prod
                        |> Maybe.map Tuple.second
                        |> Result.fromMaybe ("unit has no production: " ++ Item.name item)

                _ ->
                    Err <| "non-units cannot have production: " ++ Item.name item

        Velocity ->
            case item of
                Item.Unit unit ->
                    game
                        |> Game.units
                        |> UnitsSnapshot.velocity unit.id
                        |> Parser.Stat.parseDecimal
                        |> Ok

                _ ->
                    Err <| "non-units cannot have velocity: " ++ Item.name item

        Count ->
            Game.itemCount (Item.id item) game
                |> Parser.Stat.parseDecimal
                |> Ok

        MaxCount ->
            case item of
                Item.Unit unit ->
                    unit.maxCount
                        |> Result.fromMaybe ("unit has no maxCount: " ++ Unit.name unit)

                _ ->
                    Err <| "non-units cannot have maxcount: " ++ Item.name item

        OnBuyAddUnits name ->
            (Item.onBuy item).addUnitsByChild
                |> Dict.get name
                |> Result.fromMaybe ("no OnBuy.AddUnits: " ++ name)

        OnBuyMulUnits name ->
            (Item.onBuy item).mulUnitsByChild
                |> Dict.get name
                |> Result.fromMaybe ("no OnBuy.MulUnits: " ++ name)

        OnBuyAddTime ->
            (Item.onBuy item).addTime
                |> List.head
                |> Result.fromMaybe "no OnBuy.AddTime"

        OnBuyRandomMinimum ->
            (Item.onBuy item).addRandomUnits
                |> List.head
                |> Maybe.map (.minimum >> Parser.Stat.parseInt)
                |> Result.fromMaybe "no OnBuy.Random"

        OnBuyRandomChance ->
            (Item.onBuy item).addRandomUnits
                |> List.head
                |> Maybe.map .chance
                |> Result.fromMaybe "no OnBuy.Random"

        OnBuyRandomCount ->
            (Item.onBuy item).addRandomUnits
                |> List.head
                |> Maybe.map .count
                |> Result.fromMaybe "no OnBuy.Random"

        OnBuyAddUnitsCooldownCount name ->
            (Item.onBuy item).addUnitsCooldownByChild
                |> Dict.get name
                |> Maybe.map Tuple.first
                |> Result.fromMaybe ("no OnBuy.Cooldown: " ++ name)

        OnBuyAddUnitsCooldownDuration name ->
            (Item.onBuy item).addUnitsCooldownByChild
                |> Dict.get name
                |> Maybe.map (Tuple.second >> .val >> Duration.toMillis >> Parser.Stat.parseInt)
                |> Result.fromMaybe ("no OnBuy.Cooldown: " ++ name)

        OnBuyAddUnitsCooldownRemaining name ->
            let
                -- TODO this is wrong, need session or unitssnapshot for accurate time
                now =
                    Game.snapshotted game
            in
            (Item.onBuy item).addUnitsCooldownByChild
                |> Dict.get name
                |> Maybe.map (\( _, cooldown ) -> Game.cooldownRemaining now cooldown game |> Duration.toMillis |> Parser.Stat.parseInt)
                |> Result.fromMaybe ("no OnBuy.Cooldown: " ++ name)

        OnBuyRespecRate ->
            (Item.onBuy item).respec
                |> List.head
                |> Maybe.map ((\( _, rate, _ ) -> rate) >> Parser.Stat.parseFloat)
                |> Result.fromMaybe "no OnBuy.Respec"


eval : Game -> String -> Field -> Result String Parser.Stat.EvalContext
eval game itemName field =
    case game |> Game.gameData |> GameData.itemByName itemName of
        Nothing ->
            Err <| "no such item: " ++ itemName

        Just item ->
            getExpr game item field
                |> Result.map (Parser.Stat.evalFullWithDefault Decimal.zero (Game.getGameVar game))


evalField : Game -> String -> Field -> Result String Decimal
evalField game itemName field =
    eval game itemName field |> Result.map .out


unwrapField : Decimal -> Game -> String -> Field -> Decimal
unwrapField default game itemName field =
    evalField game itemName field
        |> Result.withDefault default


evalLabel : Game -> String -> Field -> String -> Result String Decimal
evalLabel game itemName field labelName =
    eval game itemName field
        |> Result.andThen
            (.exports
                >> Dict.get labelName
                >> Result.fromMaybe ("no such label: " ++ labelName)
            )


unwrapLabel : Decimal -> Game -> String -> Field -> String -> Decimal
unwrapLabel default game itemName field labelName =
    evalLabel game itemName field labelName
        |> Result.withDefault default


type alias Var =
    { itemName : String, field : Field, label : Maybe String }


parseVar : String -> Result String Var
parseVar name =
    let
        fromTail tail var0 =
            case tail of
                [] ->
                    Ok <| var0 Nothing

                label :: [] ->
                    Ok <| var0 <| Just label

                _ ->
                    Err <| "invalid var: " ++ name
    in
    case String.split "." name of
        itemName :: "prod" :: tail ->
            Var itemName Prod |> fromTail tail

        itemName :: "cost" :: cost :: tail ->
            Var itemName (Cost cost) |> fromTail tail

        itemName :: "velocity" :: tail ->
            Var itemName Velocity |> fromTail tail

        itemName :: "maxCount" :: tail ->
            Var itemName MaxCount |> fromTail tail

        itemName :: "add" :: unitName :: tail ->
            Var itemName (OnBuyAddUnits unitName) |> fromTail tail

        itemName :: "mul" :: unitName :: tail ->
            Var itemName (OnBuyMulUnits unitName) |> fromTail tail

        itemName :: "time" :: tail ->
            Var itemName OnBuyAddTime |> fromTail tail

        itemName :: "random" :: "minimum" :: tail ->
            Var itemName OnBuyRandomMinimum |> fromTail tail

        itemName :: "random" :: "chance" :: tail ->
            Var itemName OnBuyRandomChance |> fromTail tail

        itemName :: "random" :: "count" :: tail ->
            Var itemName OnBuyRandomCount |> fromTail tail

        itemName :: "cooldown" :: unitName :: "duration" :: tail ->
            Var itemName (OnBuyAddUnitsCooldownDuration unitName) |> fromTail tail

        itemName :: "cooldown" :: unitName :: "remaining" :: tail ->
            Var itemName (OnBuyAddUnitsCooldownRemaining unitName) |> fromTail tail

        itemName :: "cooldown" :: unitName :: "count" :: tail ->
            Var itemName (OnBuyAddUnitsCooldownCount unitName) |> fromTail tail

        itemName :: "respec" :: "rate" :: tail ->
            Var itemName OnBuyRespecRate |> fromTail tail

        itemName :: "count" :: tail ->
            Var itemName Count |> fromTail tail

        _ ->
            Err <| "invalid var: " ++ name


evalVar : Game -> String -> Result String Decimal
evalVar game =
    parseVar
        >> Result.andThen
            (\{ itemName, field, label } ->
                case label of
                    Nothing ->
                        evalField game itemName field

                    Just l ->
                        evalLabel game itemName field l
            )


unwrapVar : Decimal -> Game -> String -> Decimal
unwrapVar default game =
    parseVar
        >> Result.map
            (\{ itemName, field, label } ->
                case label of
                    Nothing ->
                        unwrapField default game itemName field

                    Just l ->
                        unwrapLabel default game itemName field l
            )
        >> Result.withDefault default


unwrapVar0 : Game -> String -> Decimal
unwrapVar0 =
    unwrapVar Decimal.zero
