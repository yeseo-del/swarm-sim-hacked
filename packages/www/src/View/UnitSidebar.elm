module View.UnitSidebar exposing (wrap)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Route exposing (Route)
import Route.Feature as Feature
import Select
import Session exposing (Session)
import View.Device
import View.Icon
import View.Lang as T exposing (Lang)


wrap : (Route -> msg) -> Select.STab -> Item -> Session -> Game -> UnitsSnapshot -> List (Html msg) -> List (Html msg)
wrap clickMsg stab item session game snapshot content =
    if Feature.isActive Feature.MobileUI (Session.query session) then
        [ div [ class "row" ]
            [ div [ class "col-12 col-lg-9 col-md-8 order-2" ]
                content
            , div [ class "col-12 col-lg-3 col-md-4 order-1" ]
                [ View.Device.onlyDesktop <| viewSidebar clickMsg stab item session game snapshot content ]
            ]
        ]

    else
        [ div [ class "row" ]
            [ div [ class "col-lg-9 col-md-8 col-sm-7 order-2", class "col-xs-12 order-xs-1" ]
                content
            , div [ class "col-lg-3 col-md-4 col-sm-5 order-1", class "col-xs-12 order-xs-1" ]
                (viewSidebar clickMsg stab item session game snapshot content)
            ]
        ]


viewSidebar : (Route -> msg) -> Select.STab -> Item -> Session -> Game -> UnitsSnapshot -> List (Html msg) -> List (Html msg)
viewSidebar clickMsg stab item session game snapshot content =
    let
        units =
            Select.unitCountsByTab stab game snapshot

        spells =
            Select.spellsByTab stab game snapshot

        achieves =
            case stab of
                Select.AllTab ->
                    True

                Select.NormalTab tab ->
                    tab.showAchievements
    in
    [ table [ class "table sidebar", class "table-hover" ]
        [ tbody []
            ((if achieves && (Game.achievementPoints game).val > 0 then
                [ viewAchievementsEntry clickMsg session game ]

              else
                []
             )
                ++ (spells |> List.map (viewSpellEntry clickMsg stab item session game snapshot))
                ++ (units |> List.map (viewUnitEntry clickMsg stab item session game snapshot))
            )
        ]
    ]


viewAchievementsEntry : (Route -> msg) -> Session -> Game -> Html msg
viewAchievementsEntry clickMsg session game =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        points =
            Game.achievementPoints game
    in
    tr [ onClick <| clickMsg Route.Achievements, style "cursor" "pointer" ]
        [ td [ class "upgradable-icon" ] []
        , td [ class "item-name" ]
            [ a [ Route.href query Route.Achievements ]
                [ span [ class "float-left", class "list-label" ]
                    [ span [ class "list-icon-resource icon-achievements" ] []
                    , lang.html <| T.ViewUnitSidebarAchievementPoints
                    ]
                , span [ class "float-right" ] [ lang.html <| T.ViewUnitSidebarCount <| Decimal.fromInt points.val ]
                ]
            ]
        ]


viewSpellEntry : (Route -> msg) -> Select.STab -> Item -> Session -> Game -> UnitsSnapshot -> Spell -> Html msg
viewSpellEntry clickMsg stab active session game snapshot spell =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab
    in
    -- sadly, we can't make an actual `<a href...>` cover the whole table row
    tr
        [ onClick <| clickMsg <| Route.Spell tabSlug spell.slug
        , style "cursor" "pointer"
        , classList [ ( "table-active", Spell.name spell == Item.name active ) ]
        ]
        [ td [ class "upgradable-icon" ] [{- no upgrade indicators for spells -}]
        , td [ class "item-name" ]
            [ a [ Route.href query <| Route.Spell tabSlug spell.slug ]
                [ span [ class "float-left", class "list-label" ]
                    [ span [ class "list-icon-resource", class <| "icon-" ++ Spell.name spell ] []
                    , lang.html <| T.ViewUnitSidebarSpellName spell.id
                    , text " "
                    ]
                , span [ class "float-right" ] [{- no visible count for spells -}]
                ]
            ]
        ]


viewUnitEntry : (Route -> msg) -> Select.STab -> Item -> Session -> Game -> UnitsSnapshot -> ( Unit, Decimal ) -> Html msg
viewUnitEntry clickMsg stab active session game snapshot ( unit, count ) =
    let
        isWatchTriggered =
            Select.isUnitSidebarWatchTriggered unit game snapshot

        isAscendTriggered =
            Select.isUnitSidebarAscendTriggered unit game snapshot

        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab
    in
    -- sadly, we can't make an actual `<a href...>` cover the whole table row
    tr
        [ onClick <| clickMsg <| Route.unit tabSlug unit.slug
        , style "cursor" "pointer"
        , classList [ ( "table-active", Unit.name unit == Item.name active ) ]
        ]
        [ td [ class "upgradable-icon" ]
            ((if isWatchTriggered then
                [ View.Icon.upgradable ]

              else
                []
             )
                ++ (if isAscendTriggered then
                        [ View.Icon.fas "rocket" ]

                    else
                        []
                   )
            )
        , td [ class "item-name" ]
            [ a [ Route.href query <| Route.unit tabSlug unit.slug ]
                [ span [ class "float-left", class "list-label" ]
                    [ span [ class "list-icon-resource", class <| "icon-" ++ Unit.name unit ] []
                    , lang.html <| T.ViewUnitSidebarUnitName unit.id
                    , text " "
                    ]
                , span [ class "float-right" ] [ lang.html <| T.ViewUnitSidebarCount count ]
                ]
            ]
        ]
