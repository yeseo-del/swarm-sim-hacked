module View.Description.Territory exposing (view)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Session exposing (Session)
import View.Description.Territory.PieChart as PieChart
import View.DescriptionVars
import View.Icon
import View.Lang as T exposing (Lang)


view : Session -> Game -> Unit -> Html msg
view session game unit =
    let
        lang =
            T.gameToLang session game

        snap =
            Game.units game

        prods : List ( Unit, Decimal )
        prods =
            GameData.units (Game.gameData game)
                |> List.filterMap (\u -> Maybe.map (Tuple.pair u) u.prod)
                |> List.filter (\( u, ( p, _ ) ) -> Unit.idToString p == "territory" && UnitsSnapshot.isUnitVisible u.id snap)
                |> List.map
                    (\( u, ( _, p ) ) ->
                        ( u
                        , let
                            each =
                                Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) p

                            count =
                                UnitsSnapshot.count u.id snap
                          in
                          Decimal.mul each count
                        )
                    )

        total : Decimal
        total =
            prods |> List.map Tuple.second |> Decimal.sum

        percents : List ( Unit, Decimal, Float )
        percents =
            prods
                |> List.map
                    (\( u, prod ) ->
                        ( u
                        , prod
                        , Decimal.div prod (Decimal.max Decimal.one total) |> Decimal.toFloat
                        )
                    )
                |> List.filter (\( _, _, pct ) -> pct > 0.01)
                |> List.sortBy (\( _, _, pct ) -> -pct)
    in
    div []
        [ lang.html <| T.UnitDesc (View.DescriptionVars.evalVar game) unit.id
        , ul [ class "legend" ]
            (percents
                |> List.indexedMap
                    (\index ( u, prod, percent ) ->
                        li [ class ("entry entry-" ++ String.fromInt index) ]
                            [ span [ class "dot" ] [ View.Icon.fas "circle" ]
                            , lang.html <| T.DescTerritoryLegendEntry u.id percent
                            ]
                    )
            )
        , PieChart.view (percents |> List.map (\( u, _, percent ) -> ( lang.str <| T.DescTerritoryLegendEntry u.id percent, percent )))
        ]
