module View.Description.Cocoon exposing (capHref, view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot
import GameData exposing (GameData)
import GameData.Tab.Id as TabId
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Route exposing (Route)
import Session exposing (Session)
import View.DescriptionVars
import View.Lang as T exposing (Lang)


view : Session -> Game -> Unit -> Html msg
view session game unit =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session
    in
    div []
        [ p [] [ lang.html T.DescCocoon1 ]
        , p [] [ lang.html <| T.DescCocoon2 query ]
        , p [] [ lang.html <| T.DescCocoon3 query (capHref game) (View.DescriptionVars.evalVar game) ]
        ]


capHref : Game -> Result String Route
capHref game =
    View.DescriptionVars.evalVar game "clonelarvae.add.larva.cap"
        |> Result.map (\c -> Route.Unit "larva" "cocoon" { n = Just <| "@" ++ Decimal.toString c })
