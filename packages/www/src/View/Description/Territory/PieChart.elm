module View.Description.Territory.PieChart exposing (view)

{-| Based on <https://hackernoon.com/a-simple-pie-chart-in-svg-dbdd653b6936>
-}

import Svg as S exposing (..)
import Svg.Attributes as A exposing (..)


view : List ( String, Turns ) -> Svg msg
view labeledTurns =
    let
        labels =
            List.map Tuple.first labeledTurns

        turns_ =
            List.map Tuple.second labeledTurns

        slices_ =
            turns_ |> List.map turns |> slices

        labeledSlices : List ( String, Slice )
        labeledSlices =
            List.map2 Tuple.pair labels slices_
    in
    svg [ width "300", height "300", viewBox "-1.2 -1.2 2.4 2.4" ]
        (labeledSlices |> List.indexedMap (\i ( l, s ) -> viewArc ("pie-" ++ String.fromInt i) l s))


type alias Turns =
    Float


type Radians
    = Radians Float


{-| A slice of the pie chart. Start-angle and slice-size.

    Slice angleStart angleDistanceFromStart

    angleEnd = angleStart + angleDistanceFromStart

-}
type Slice
    = Slice Radians Radians


turns : Turns -> Radians
turns =
    Basics.turns >> Radians


{-| Generate a list of slices from a list of slice-sizes.
-}
slices : List Radians -> List Slice
slices increments =
    let
        -- A slice's start-angle is the sum of all earlier slice sizes.
        totalN : Int -> Radians
        totalN n =
            increments
                |> List.take n
                |> List.map (\(Radians r) -> r)
                |> List.sum
                |> Radians
    in
    increments |> List.indexedMap (\n -> Slice (totalN n))


viewArc : String -> String -> Slice -> Svg msg
viewArc class_ label (Slice (Radians angle0) (Radians dAngle)) =
    let
        ( x0, y0 ) =
            fromPolar ( 1, angle0 )

        ( x1, y1 ) =
            fromPolar ( 1, angle0 + dAngle )

        largeArc =
            if dAngle > Basics.turns 0.5 then
                "1"

            else
                "0"

        d =
            -- Starting point
            [ "M"
            , String.fromFloat x0
            , String.fromFloat y0

            -- Arc to ending point
            , "A 1 1 0"
            , largeArc
            , "1"
            , String.fromFloat x1
            , String.fromFloat y1

            -- Line to middle
            , "L 0 0"
            ]
                |> String.join " "
    in
    S.path [ A.d d, class class_ ]
        [ S.title [] [ text label ] ]
