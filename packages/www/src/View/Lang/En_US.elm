module View.Lang.En_US exposing (formatter)

import DateFormat.Relative
import Decimal exposing (Decimal)
import Duration exposing (Duration)
import FormatNumber
import FormatNumber.Locales
import Game.NumberFormat as NumberFormat exposing (NumberFormat)
import Game.VelocityUnit as VelocityUnit exposing (VelocityUnit)
import Iso8601
import Locale exposing (Locale)
import NumberSuffix2 as NumberSuffix
import NumberSuffix2.Round
import Romanize
import Round
import Time exposing (Posix)
import View.LangFormatter exposing (Formatter)


formatter : NumberFormat -> Formatter
formatter numberFormat =
    { locale = Locale.En_US
    , decimal = decimal numberFormat
    , smallDecimal = smallDecimalFormatter decimal numberFormat
    , decimalShort = decimalShort numberFormat
    , smallDecimalShort = smallDecimalFormatter decimalShort numberFormat
    , int = int numberFormat
    , fullInt = fullInt numberFormat
    , percent = percent numberFormat
    , percent1 = percent1 numberFormat
    , date = date
    , reldate = reldate
    , roman = romanize
    , list = list
    , money = money numberFormat
    , shortDuration = Duration.toStringDHMS
    , longDuration = longDuration
    , timerDuration = Duration.toString
    }


romanize : Int -> String
romanize n =
    if n == 0 then
        ""

    else
        " " ++ Romanize.romanize (1 + n)


longDuration : Duration -> String
longDuration dur =
    DateFormat.Relative.relativeTime (Time.millisToPosix <| Duration.toMillis dur) (Time.millisToPosix 0)
        |> String.replace " ago" ""


list : List String -> String
list vals =
    case vals |> List.reverse of
        [] ->
            "nothing"

        [ c1 ] ->
            c1

        [ c2, c1 ] ->
            c1 ++ " and " ++ c2

        last :: heads ->
            (heads |> List.reverse |> String.join ", ") ++ ", and " ++ last


smallDecimalFormatter : (NumberFormat -> Decimal -> String) -> NumberFormat -> Decimal -> String
smallDecimalFormatter defaultFormatter nf d =
    if Decimal.lte d (Decimal.fromInt 100) then
        FormatNumber.format FormatNumber.Locales.usLocale <| Decimal.toFloat d

    else
        defaultFormatter nf d


decimal : NumberFormat -> Decimal -> String
decimal nf d =
    let
        config =
            NumberFormat.toConfig nf
    in
    NumberSuffix.formatSigExp { config | sigfigs = 5, stringFromFloat = NumberSuffix2.Round.round Round.round } (Decimal.significand d) (Decimal.exponent d)


decimalShort : NumberFormat -> Decimal -> String
decimalShort nf d =
    let
        config =
            NumberFormat.toShortConfig nf
    in
    NumberSuffix.formatSigExp { config | stringFromFloat = NumberSuffix2.Round.round Round.round } (Decimal.significand d) (Decimal.exponent d)


int : NumberFormat -> Int -> String
int nf =
    let
        config =
            NumberFormat.toConfig nf
    in
    NumberSuffix.formatInt config


fullInt : NumberFormat -> Int -> String
fullInt nf =
    let
        config =
            NumberFormat.toConfig nf
    in
    NumberSuffix.formatInt { config | minSuffixExp = 999999999999 }


percent : NumberFormat -> Float -> String
percent nf pct =
    int nf (round (pct * 100)) ++ "%"


percent1 : NumberFormat -> Float -> String
percent1 nf pct =
    pct - 1 |> percent nf


date : Posix -> String
date =
    -- TODO this is ugly
    Iso8601.fromTime


reldate : Posix -> Posix -> String
reldate at now =
    -- TODO this is ugly
    Iso8601.fromTime at


money : NumberFormat -> Int -> String
money nf cents =
    -- TODO probably shouldn't conflate language with currency.
    -- Everything is paid in USD (Paypal) or Kreds (Kongregate), no matter the language.
    let
        pre =
            cents // 100

        post =
            cents |> modBy 100
    in
    "$" ++ int nf pre ++ "." ++ String.fromInt post
