module View.KeyCombo exposing (Msg, update, view)

import Game exposing (Game)
import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import KeyCombo exposing (KeyCombo)
import Session exposing (Session)
import View.Icon
import View.Lang as T


type Msg
    = Close
    | Noop


update : Msg -> Session -> Session
update msg session =
    case msg of
        Close ->
            Session.setKeyCombosHelp False session

        Noop ->
            session


view : Session -> GameData -> (() -> KeyCombo.Bindings km) -> List (Html Msg)
view session gd getBindings =
    if Session.keyCombosHelp session then
        let
            lang =
                T.gameDataToLang session gd

            bindings =
                getBindings () |> KeyCombo.names
        in
        -- Manually render the open modal - display:block and modal-backdrop.
        -- This is not how you're supposed to do it according to the docs, but
        -- it seems to work, and avoids ports and a bootstrap-JS call.
        [ div
            [ class "modal modal-open show"
            , style "display" "block"
            , tabindex -1
            , attribute "role" "dialog"
            , attribute "aria-hidden" "true"
            , onClick Close
            ]
            [ div [ class "modal-dialog keycombo", attribute "role" "document" ]
                [ div
                    [ class "modal-content"

                    -- Closing when the background is clicked is a bit tricky.
                    -- `onClick` on the `modal-backdrop` does not work.
                    -- `onClick` on the `modal` itself does - it catches all
                    -- clicks for both the dialog and the background.
                    -- We don't want to close when the dialog itself is
                    -- clicked, though - just the background - so this
                    -- `stopPropagation` replaces the close with a no-op.
                    , stopPropagationOn "click" <| D.succeed ( Noop, True )
                    ]
                    [ div [ class "modal-header" ]
                        [ h5 [ class "modal-title" ]
                            [ View.Icon.fas "keyboard"
                            , text " "
                            , lang.html T.ViewTabHotkeys
                            ]
                        , button [ class "close", onClick Close, attribute "aria-label" "close" ] [ text "×" ]
                        ]
                    , div [ class "modal-body" ]
                        [ dl [] (bindings |> List.map (viewBinding lang) |> List.concat) ]
                    , div [ class "modal-footer" ] []
                    ]
                ]
            ]
        , div [ class "modal-backdrop show" ] []
        ]

    else
        []


viewBinding : T.Lang msg -> ( KeyCombo, String ) -> List (Html msg)
viewBinding lang ( c, name ) =
    [ dt [] [ text <| KeyCombo.toString c ]
    , dd [] [ lang.html <| T.KeyCombo name ]
    ]
