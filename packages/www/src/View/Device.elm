module View.Device exposing (Device(..), onlyDesktop, onlyDesktopCls, onlyDesktopInline, onlyDevice, onlyMobile, onlyMobileCls, onlyMobileInline)

import Html exposing (..)
import Html.Attributes exposing (..)


type Device
    = Desktop
    | Mobile


onlyDevice : Device -> List (Html msg) -> Html msg
onlyDevice d =
    case d of
        Desktop ->
            onlyDesktop

        Mobile ->
            onlyMobile


onlyMobileCls : Html.Attribute msg
onlyMobileCls =
    class "d-block d-md-none"


onlyMobile : List (Html msg) -> Html msg
onlyMobile =
    div [ onlyMobileCls ]


onlyMobileInline : List (Html msg) -> Html msg
onlyMobileInline =
    span [ class "d-inline d-md-none" ]


onlyDesktopCls : Html.Attribute msg
onlyDesktopCls =
    class "d-none d-md-block"


onlyDesktop : List (Html msg) -> Html msg
onlyDesktop =
    div [ onlyDesktopCls ]


onlyDesktopInline : List (Html msg) -> Html msg
onlyDesktopInline =
    span [ class "d-none d-md-inline " ]
