module View.TabNav exposing (Msg, bulkUpgrade, update, view, viewFromGame, viewMoreMenuPage)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.BuyOrder
import Game.OfflineSync as OfflineSync
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import Parser.Stat
import PlayFab.Error
import PlayFab.User as User exposing (User)
import Ports.Kongregate
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Feature as Feature
import Route.QueryDict as QueryDict exposing (QueryDict)
import Select
import Session exposing (Session)
import Session.Iframe as Iframe exposing (Iframe)
import Time exposing (Posix)
import View.Device
import View.Icon
import View.Lang as T exposing (Lang)


type Msg
    = Undo
    | KongregateShowRegistrationBox
    | BulkUpgradeAll Int
    | BulkUpgradeCheapest Int
    | OpenKeyComboHelp


update : Msg -> Session -> ( Session, Cmd msg )
update msg session =
    case msg of
        Undo ->
            Session.undo session

        KongregateShowRegistrationBox ->
            ( session, Ports.Kongregate.kongregateShowRegistrationBox () )

        BulkUpgradeAll n ->
            if n == 0 then
                ( session, Cmd.none )

            else
                bulkUpgrade 1 session

        BulkUpgradeCheapest n ->
            if n == 0 then
                ( session, Cmd.none )

            else
                bulkUpgrade 0.25 session

        OpenKeyComboHelp ->
            ( Session.setKeyCombosHelp True session, Cmd.none )


bulkUpgrade : Float -> Session -> ( Session, Cmd msg )
bulkUpgrade pct session =
    case Session.game session of
        RemoteData.Success game0 ->
            let
                now =
                    Session.safeNow game0 session

                game =
                    game0 |> Game.snapshotUnits now

                items : List Item
                items =
                    Select.bulkBuyableItems pct game (Game.units game)
            in
            List.foldl (buyBulkUpgradeItem now pct) game items
                |> Session.persistWrite session
                |> Tuple.mapFirst (\g -> Session.setGame True g session)

        _ ->
            ( session, Cmd.none )


filterResult : err -> (val -> Bool) -> Result err val -> Result err val
filterResult err filterFn res =
    case Result.map filterFn res of
        Ok False ->
            Err err

        _ ->
            res


bulkUpgradeItemOrder : Float -> Item -> Game -> Result String Order
bulkUpgradeItemOrder pct item game =
    Select.itemWatchedOrder pct game (Game.units game) item
        |> filterResult "order not possible" .isPossible


buyBulkUpgradeItem : Posix -> Float -> Item -> Game -> Game
buyBulkUpgradeItem now pct item game0 =
    -- One of the bulk upgrade orders isn't possible. Maybe we ran out
    -- of money after an earlier order. Fine, skip it - silent failure
    -- is fine here.
    let
        game =
            Game.snapshotUnits now game0
    in
    bulkUpgradeItemOrder pct item game
        |> Result.andThen (\o -> Game.BuyOrder.buy o game)
        |> Result.withDefault game


viewFromGame : Maybe Tab.Id -> Session -> Game -> List (Html Msg)
viewFromGame activeTab session game =
    view activeTab session game <| Session.safeUnits game session


view : Maybe Tab.Id -> Session -> Game -> UnitsSnapshot -> List (Html Msg)
view activeTab session game snap =
    let
        getLastUnit =
            Game.flipLastUnitByTab game

        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabs =
            game
                |> Game.gameData
                |> GameData.tabs
                |> List.filter (\t -> UnitsSnapshot.isTabVisible t.id snap)
    in
    if Feature.isActive Feature.MobileUI query then
        [ View.Device.onlyDesktop
            [ ul [ class "nav nav-tabs" ]
                (List.map (viewTab View.Device.Desktop getLastUnit lang query activeTab game snap) tabs
                    ++ [ viewMoreTab View.Device.Desktop session game snap ]
                )
            ]
        , View.Device.onlyMobile
            [ ul [ class "nav nav-tabs nav-fill" ]
                (List.map (viewTab View.Device.Mobile getLastUnit lang query activeTab game snap) tabs
                    ++ [ viewMoreTab View.Device.Mobile session game snap ]
                )
            ]
        ]

    else
        [ ul [ class "nav nav-tabs" ]
            (List.map (viewTab View.Device.Desktop getLastUnit lang query activeTab game snap) tabs
                ++ [ viewMoreTab View.Device.Desktop session game snap ]
            )
        ]


viewTab : View.Device.Device -> (Tab.Id -> Route) -> Lang msg -> QueryDict -> Maybe Tab.Id -> Game -> UnitsSnapshot -> Tab -> Html msg
viewTab device lastUnit lang query activeTab game snap tab =
    let
        isActive : Bool
        isActive =
            Just tab.id == activeTab

        isWatchTriggered =
            Select.isTabWatchTriggered tab game snap

        isAscendTriggered =
            Select.isTabAscendTriggered tab game snap

        route : Route
        route =
            if isActive then
                Route.Tab (Tab.name tab)

            else
                lastUnit tab.id

        count : Decimal
        count =
            UnitsSnapshot.count tab.unitId snap

        count2 : Maybe Decimal
        count2 =
            -- this is for premutagen
            tab.secondaryUnitId |> Maybe.map (\id2 -> UnitsSnapshot.count id2 snap)

        cap : Maybe Decimal
        cap =
            GameData.unitByName (Unit.idToString tab.unitId) (Game.gameData game)
                |> Maybe.andThen .maxCount
                |> Maybe.map (\capExpr -> Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) capExpr)

        capPct : Maybe Float
        capPct =
            Maybe.map (\cap_ -> Decimal.div count cap_ |> Decimal.toFiniteFloat |> Maybe.withDefault 1)
                cap

        watchIcon =
            (if isWatchTriggered then
                [ text " ", View.Icon.fas "arrow-alt-circle-up", text " " ]

             else
                []
            )
                ++ (if isAscendTriggered then
                        [ text " ", View.Icon.fas "rocket", text " " ]

                    else
                        []
                   )
    in
    case device of
        View.Device.Mobile ->
            li [ class "nav-item mobile-tab-nav" ]
                [ a [ class "nav-link", classList [ ( "active", isActive ) ], Route.href query route ]
                    [ span
                        [ class "tab-icon-resource"
                        , class <| "tab-icon-" ++ Tab.name tab
                        , class <| "icon-" ++ Tab.name tab
                        ]
                        []
                    , div [ class "mobile-tab-name" ] (watchIcon ++ [ lang.html <| T.ViewTabMobileName tab.unitId ])
                    , div [] [ lang.html <| T.ViewTabMobileCount count ]
                    , div [] [ lang.html <| T.ViewTabMobileSecondary count { capPct = capPct, count2 = count2 } ]
                    ]
                ]

        View.Device.Desktop ->
            li [ class "nav-item" ]
                [ a [ class "nav-link", classList [ ( "active", isActive ) ], Route.href query route ]
                    ([ span
                        [ class "tab-icon-resource"
                        , class <| "tab-icon-" ++ Tab.name tab
                        , class <| "icon-" ++ Tab.name tab
                        ]
                        []
                     , lang.html <| T.ViewTabCount tab.unitId count { capPct = capPct, count2 = count2 }
                     ]
                        ++ watchIcon
                    )
                ]


viewMoreTab : View.Device.Device -> Session -> Game -> UnitsSnapshot -> Html Msg
viewMoreTab device session game snap =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session
    in
    case device of
        View.Device.Desktop ->
            -- Desktop gets a full dropdown menu
            li [ class "nav-item" ]
                -- https://getbootstrap.com/docs/4.2/components/dropdowns/
                [ div [ class "dropdown" ]
                    [ button
                        [ class "nav-link dropdown-toggle btn btn-link"
                        , id "more-tab"
                        , attribute "data-toggle" "dropdown"
                        , attribute "role" "button"
                        , attribute "aria-haspopup" "true"
                        ]
                        [ lang.html T.ViewTabMore ]
                    , div
                        [ class "dropdown-menu"
                        , attribute "aria-labelledby" "more-tab"
                        ]
                        -- Nested list of buttons - groups are visually separated by dividers
                        (viewMoreDropdownEntries session game snap)
                    ]
                ]

        View.Device.Mobile ->
            -- mobile gets a link. too easy to screw up dropdown direction, etc.
            li [ class "nav-item mobile-tab-nav" ]
                [ a [ class "nav-link", Route.href query Route.MoreMenu ]
                    -- &nbsp: force newlines to fill tab height
                    -- https://twitter.com/rtfeldman/status/767263564214120448?lang=en
                    [ div [] [ text "\u{00A0}" ]
                    , div [] [ lang.html T.ViewTabMore ]
                    , div [] [ text "\u{00A0}" ]
                    ]
                ]


viewMoreMenuPage : Session -> Game -> List (Html Msg)
viewMoreMenuPage session game =
    let
        snap =
            Session.safeUnits game session
    in
    view Nothing session game snap
        ++ [ div [] (viewMoreDropdownEntries session game snap) ]


viewMoreDropdownEntries : Session -> Game -> UnitsSnapshot -> List (Html Msg)
viewMoreDropdownEntries session game snap =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        -- https://docs.google.com/forms/d/1LeLkFT_b0awkTbuRr8JYCETOyXSAHqOc0uFY7MveKuQ/edit
        feedbackUrl =
            "https://docs.google.com/forms/d/e/1FAIpQLSfffiBB3W9PIoVW-dV1v-2LggyjL4Iws5xezM_B3pw4ci4EJw/viewform?usp=pp_url&entry.1808064759="
                ++ (Session.persistExport session |> Maybe.withDefault "(empty)")

        buyAllList =
            Select.bulkBuyableItems 1 game snap

        buyCheapestList =
            Select.bulkBuyableItems 0.25 game snap
    in
    [ [ button
            [ class "dropdown-item"
            , disabled (List.length buyAllList == 0)
            , onClick <| BulkUpgradeAll <| List.length buyAllList
            , title <| String.join ", " <| List.map (lang.str << T.ItemName Decimal.one << Item.id) buyAllList
            ]
            [ View.Icon.fas "arrow-alt-circle-up", text " ", lang.html <| T.ViewTabBuyAll <| List.length buyAllList ]
      , button
            [ class "dropdown-item"
            , disabled (List.length buyCheapestList == 0)
            , onClick <| BulkUpgradeCheapest <| List.length buyCheapestList
            , title <| String.join ", " <| List.map (lang.str << T.ItemName Decimal.one << Item.id) buyCheapestList
            ]
            [ View.Icon.far "arrow-alt-circle-up", text " ", lang.html <| T.ViewTabBuyCheapest <| List.length buyCheapestList ]
      , case Session.undoDuration session of
            Nothing ->
                button [ class "dropdown-item", disabled True ] [ View.Icon.fas "undo", text " ", lang.html T.ViewTabUndo ]

            Just duration ->
                button [ class "dropdown-item", onClick Undo ] [ View.Icon.fas "undo", text " ", lang.html <| T.ViewTabUndoDuration duration ]
      ]
    , viewUserButtons session game
    , [ a [ class "dropdown-item", Route.href query Route.Options ] [ View.Icon.fas "cog", text " ", lang.html T.ViewTabOptions ]
      , a [ class "dropdown-item", Route.href query Route.Achievements ] [ View.Icon.fas "trophy", text " ", lang.html T.ViewTabAchievements ]
      , a [ class "dropdown-item", Route.href query Route.Statistics ] [ View.Icon.fas "chart-bar", text " ", lang.html T.ViewTabStatistics ]
      , a [ class "dropdown-item", Route.href query Route.Changelog ] [ View.Icon.fas "book", text " ", lang.html T.ViewTabChangelog ]
      , a [ class "dropdown-item", href "https://www.reddit.com/r/swarmsim", target "_blank" ] [ View.Icon.fab "reddit", text " ", lang.html T.ViewTabCommunity ]
      , a [ class "dropdown-item", href feedbackUrl, target "_blank" ] [ View.Icon.fas "comment", text " ", lang.html T.ViewTabFeedback ]
      , a [ class "dropdown-item", href feedbackUrl, target "_blank" ] [ View.Icon.fas "bug", text " ", lang.html T.ViewTabBugReport ]
      , a [ class "dropdown-item", Route.href query <| Route.Tab "all" ] [ View.Icon.far "list-alt", text " ", lang.html T.ViewTabAllUnits ]

      -- showing key combos to mobile users makes no sense. It's based on screen
      -- size, but I'm willing to ignore weird cases like someone with a keyboard
      -- connected to their phone.
      , button [ class "dropdown-item", View.Device.onlyDesktopCls, onClick OpenKeyComboHelp ] [ View.Icon.fas "keyboard", text " ", lang.html T.ViewTabHotkeys ]
      ]
    , if Feature.isActive Feature.Debug query then
        [ a [ class "dropdown-item", Route.href query Route.Debug ] [ View.Icon.fas "tools", text " ", text " Debugging tools" ]
        ]

      else
        []
    , if Feature.isActive Feature.Translator query then
        [ a [ class "dropdown-item", Route.href query Route.Translator ] [ View.Icon.fas "language", text " ", text " Translating tools" ]
        ]

      else
        []
    ]
        |> List.filter (List.isEmpty >> not)
        |> List.intersperse [ div [ class "dropdown-divider" ] [] ]
        |> List.concat


viewUserButtons : Session -> Game -> List (Html Msg)
viewUserButtons session game =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session
    in
    case Session.iframe session of
        Iframe.WWW ->
            case Session.userState session of
                RemoteData.NotAsked ->
                    []

                RemoteData.Loading ->
                    [ a [ class "dropdown-item", Route.href query Route.Login ] [ View.Icon.loading, text " ", lang.html T.ViewTabNavUserLoading ] ]

                RemoteData.Failure err ->
                    [ a [ class "dropdown-item", Route.href query Route.Login, title <| PlayFab.Error.toString err ]
                        [ View.Icon.fas "user-times", text " ", lang.html T.ViewTabNavUserError ]
                    ]

                RemoteData.Success Nothing ->
                    [ a [ class "dropdown-item", Route.href query Route.Login ] [ View.Icon.fas "user", text " ", lang.html T.ViewTabLogin ]
                    , a [ class "dropdown-item", Route.href query Route.Register ] [ View.Icon.fas "user-plus", text " ", lang.html T.ViewTabRegister ]
                    ]

                RemoteData.Success (Just ({ user } as ustate)) ->
                    case ustate.sync of
                        Err conflict ->
                            [ a [ class "dropdown-item", Route.href query Route.User ] [ View.Icon.fas "user-times", text " ", lang.html T.ViewTabNavUserConflict ] ]

                        Ok sync ->
                            -- TODO show when remote save was last written
                            let
                                prefix =
                                    case User.role user of
                                        User.AdminRole ->
                                            [ View.Icon.fas "user-lock", text " (admin) " ]

                                        User.PlayerRole ->
                                            [ View.Icon.fas "user", text " " ]
                            in
                            [ a [ class "dropdown-item", Route.href query Route.User ] (prefix ++ [ text <| User.displayName (Session.iframe session) user ]) ]

        Iframe.Kongregate kong ->
            let
                guest =
                    [ a [ class "dropdown-item", href "#", onClick KongregateShowRegistrationBox ] [ View.Icon.fas "user-plus", text " ", lang.html T.ViewTabNavUserKongLogin ] ]
            in
            case kong.user of
                Just user ->
                    if user.isGuest then
                        guest

                    else
                        [ a [ class "dropdown-item", Route.href query Route.User ] [ View.Icon.fas "user", text " ", text user.username ] ]

                _ ->
                    guest
