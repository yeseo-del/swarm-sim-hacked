module View.Tutorial exposing (view)

import Game exposing (Game)
import Game.Tutorial as Tutorial
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)
import View.Lang as T exposing (Lang)


view : Session -> Game -> List (Html msg)
view session game =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        units =
            Session.safeUnits game session
    in
    case Tutorial.step { undo = Session.undoState session, game = game, units = units } of
        Just step ->
            [ div [ class "alert alert-info" ]
                -- [ div [ class "float-right close" ] [ button [ class "btn btn-link" ] [ text "×" ] ]
                [ lang.html <| T.ViewTutorial query step
                ]
            ]

        Nothing ->
            []
