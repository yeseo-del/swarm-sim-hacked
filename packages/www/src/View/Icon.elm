module View.Icon exposing (fab, far, fas, loading, notAsked, upgradable)

import Html as H exposing (..)
import Html.Attributes as A exposing (..)



-- Fontawesome: https://fontawesome.com/icons?d=gallery
-- https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use
--
-- Another free icon set, not yet imported: https://octicons.github.com/
-- Original swarmsim used glyphicons, but they're nonfree now


fas : String -> Html msg
fas name =
    i [ class "fas", class ("fa-" ++ name) ] []


far : String -> Html msg
far name =
    i [ class "far", class ("fa-" ++ name) ] []


fab : String -> Html msg
fab name =
    i [ class "fab", class ("fa-" ++ name) ] []


upgradable : Html msg
upgradable =
    fas "arrow-alt-circle-up"


notAsked : Html msg
notAsked =
    fas "spinner"


loading : Html msg
loading =
    fas "spinner fa-spin"
