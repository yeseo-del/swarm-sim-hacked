module View.Lang exposing (Lang, Text(..), gameDataToLang, gameToLang, init, localeToLang, sessionToLang, text)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Fools exposing (Fools)
import Game exposing (Game)
import Game.HatchableRate
import Game.NumberFormat as NumberFormat exposing (NumberFormat)
import Game.Order as Order
import Game.Order.Progress exposing (Progress)
import Game.Theme as Theme exposing (Theme)
import Game.Tutorial as Tutorial
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Game.VelocityUnit as VelocityUnit exposing (VelocityUnit)
import GameData exposing (GameData)
import GameData.Achievement as Achievement
import GameData.Cost as Cost exposing (Cost)
import GameData.Item as Item exposing (resToDec)
import GameData.Item.Id as ItemId
import GameData.Lang
import GameData.Spell as Spell
import GameData.Tab as Tab
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade
import Html as H
import Html.Attributes as A
import Iso8601
import Locale exposing (Locale(..))
import Markdown
import Maybe.Extra
import MessageFormat exposing (Message)
import NumberSuffix
import Parser.Tutorial
import RemoteData exposing (RemoteData)
import Result.Extra
import Route exposing (Route)
import Route.QueryDict as QueryDict exposing (QueryDict)
import Session exposing (Session)
import Time exposing (Posix)
import View.Lang.En_US
import View.LangFormatter exposing (Formatter)


type Text
    = Title
    | Header
    | FrameRate Int { max : Duration, avg : Duration }
    | ServiceWorkerUpdateDownloaded
    | DebugDecimal Decimal
    | UnitDesc (String -> Result String Decimal) Unit.Id
    | UnitLol Unit.Id
    | UpgradeDesc (String -> Result String Decimal) Upgrade.Id
    | UpgradeLol Upgrade.Id
    | SpellDesc (String -> Result String Decimal) Spell.Id
    | SpellLol Spell.Id
    | ItemName Decimal Item.Id
    | AchievementName Achievement.Id
    | AchievementDesc Achievement.Id (Maybe ( Item.Id, Decimal ))
    | AchievementLongDesc Achievement.Id
    | ViewErrorTitle
    | ViewErrorTime { now : Posix, at : Posix }
    | ViewErrorTimeUnknown
    | ViewErrorMore Int
    | ViewErrorCloseButton
    | ViewErrorGameLoad
    | ViewErrorGameExport
    | ViewErrorGameBug
    | ViewErrorGameReportButton
    | ViewErrorGameResetButton
    | ViewUnitSidebarUnitName Unit.Id
    | ViewUnitSidebarSpellName Spell.Id
    | ViewUnitSidebarAchievementPoints
    | ViewUnitSidebarCount Decimal
    | ViewTabVelocity Unit Decimal
    | ViewUnitHeader Unit.Id
    | ViewUnitCount Decimal Unit.Id
    | ViewUnitCappedCount Decimal Decimal Unit.Id
    | ViewUnitProdEach UnitsSnapshot.Production
    | ViewUnitProdTotal UnitsSnapshot.Production
    | ViewUnitBuyCost (Result Decimal Int) Unit.Id (List ( Unit.Id, Decimal ))
    | ViewUnitUpgradesTitle
    | ViewUnitUpgradeHeader Decimal Upgrade.Id
    | ViewUnitUpgradeBuyCost (List ( Unit.Id, Decimal ))
    | ViewUnitBuyN (Result Decimal Int)
    | ViewUnitCantBuy
    | ViewUnitAscend
    | ViewUnitVelocity Unit.Id Decimal
    | ViewUnitHatchableRate Unit.Id Game.HatchableRate.Output
    | ViewSpellHeader Spell.Id
    | ViewSpellBuyCost (Result Decimal Int) Spell.Id (List ( Unit.Id, Decimal ))
    | ViewAscendPlot
    | ViewAscendBuyCost (Result Decimal Int) (List ( Unit.Id, Decimal )) (String -> Maybe Decimal)
    | ViewAscendProgressCapDiff Decimal
    | ViewTabCount Unit.Id Decimal { capPct : Maybe Float, count2 : Maybe Decimal }
    | ViewTabMobileName Unit.Id
    | ViewTabMobileCount Decimal
    | ViewTabMobileSecondary Decimal { capPct : Maybe Float, count2 : Maybe Decimal }
    | ViewTabMore
    | ViewTabBuyAll Int
    | ViewTabBuyCheapest Int
    | ViewTabUndo
    | ViewTabUndoDuration Duration
    | ViewTabOptions
    | ViewTabAchievements
    | ViewTabStatistics
    | ViewTabChangelog
    | ViewTabCommunity
    | ViewTabFeedback
    | ViewTabBugReport
    | ViewTabAllUnits
    | ViewTabLogin
    | ViewTabRegister
    | ViewTabHotkeys
    | ViewTabNavUserLoading
    | ViewTabNavUserError
    | ViewTabNavUserConflict
    | ViewTabNavUserKongLogin
    | ViewOptionsTitle
    | ViewOptionsExport
    | ViewOptionsNotYetSaved
    | ViewOptionsResetSaveLink
    | ViewOptionsResetSaveWarning
    | ViewOptionsResetSaveCancel
    | ViewOptionsResetSaveConfirm
    | ViewOptionsHelpTranslate
    | ViewOptionsLocale Locale
    | ViewOptionsTheme Theme
    | ViewOptionsNumberFormat NumberFormat
    | ViewOptionsVelocityUnit VelocityUnit
    | ViewOptionsToggleFps Bool
    | ViewOptionsToggleDebug Bool
    | ViewOptionsAnalyticsDesc
    | ViewOptionsAnalyticsOptOutLink
    | ViewOptionsAnalyticsPrivacyLink
    | ViewStatisticsTitle
    | ViewStatisticsHeaderName
    | ViewStatisticsHeaderEntryCreated
    | ViewStatisticsHeaderClicks
    | ViewStatisticsHeaderNum
    | ViewStatisticsHeaderTwinNum
    | ViewStatisticsClicks Int
    | ViewStatisticsEntryCreated Duration
    | ViewStatisticsDecimal Decimal
    | ViewStatisticsHeaderSaveSize
    | ViewStatisticsSaveSize (Maybe Int)
    | ViewStatisticsHeaderCreated
    | ViewStatisticsItemName Item.Id
    | ViewStatisticsCreated { now : Posix, created : Posix }
    | ViewAchievementsTitle Int
    | ViewAchievementsProgress Float
    | ViewAchievementsEntryDate { now : Posix, at : Posix }
    | ViewAchievementsEntryProgress Float Decimal Decimal
    | ViewAchievementsEntryMasked
    | ViewAchievementsEntryUnlocked
    | ViewAchievementsBonus Float
    | ViewAchievementsOptionsVisible
    | ViewAchievementsOptionsVisibleEarned
    | ViewAchievementsOptionsVisibleUnearned
    | ViewAchievementsOptionsVisibleMasked
    | ViewAchievementsOptionsSort
    | ViewAchievementsOptionsSortDefault
    | ViewAchievementsOptionsSortCompletion
    | ViewAchievementsOptionsSortReversed
    | ViewAchievementsOptionsSortNotReversed
    | ViewCostMin (Result Decimal Int) Item.Id
    | ViewCostMet Progress Float
    | ViewCostNotMet Progress Float (Maybe Duration)
    | ViewFoolsInvitedLol
    | ViewFoolsInvitedTitle
    | ViewFoolsInvitedBody
    | ViewFoolsInvitedLink
    | ViewFoolsOnLol
    | ViewFoolsOnTitle
    | ViewFoolsOnBody
    | ViewFoolsOnLink
    | ViewFoolsAfterTitle
    | ViewFoolsAfterLink
    | ViewConflictTitle
    | ViewConflictDesc
    | ViewConflictExportLocal
    | ViewConflictExportRemote
    | ViewConflictGameCreated Posix
    | ViewConflictGameEncoded Posix
    | ViewConflictButtonLocal
    | ViewConflictButtonLogout
    | ViewConflictButtonRemote
    | ViewLoginTitle
    | ViewLoginFieldEmail
    | ViewLoginFieldPassword
    | ViewLoginFieldRememberMe
    | ViewLoginSubmit
    | ViewLoginLinkRegister
    | ViewLoginLinkResetPassword
    | ViewRegisterTitle
    | ViewRegisterFieldEmail
    | ViewRegisterFieldPassword
    | ViewRegisterFieldPassword2
    | ViewRegisterFieldRememberMe
    | ViewRegisterSubmit
    | ViewRegisterLinkLogin
    | ViewUserAnonymous
    | ViewUserAnonymousLogin
    | ViewUserAnonymousRegister
    | ViewUserTitle
    | ViewUserDescPush1
    | ViewUserDescPush2 Duration
    | ViewUserPushSuccess Duration
    | ViewUserPushFailure Duration
    | ViewUserPushNone
    | ViewUserPushError String
    | ViewUserPushReady
    | ViewUserPushNotReady Duration Float
    | ViewUserButtonForcePush
    | ViewUserButtonLogout
    | ViewUserButtonChangePassword
    | ViewResetPasswordTitle
    | ViewResetPasswordFieldEmail
    | ViewResetPasswordSubmit
    | ViewWelcomeBackTitle
    | ViewWelcomeBack Duration
    | ViewWelcomeBackLol
    | ViewWelcomeBackEntry Unit.Id Decimal
    | ViewWelcomeBackClose
    | ViewTutorial QueryDict Tutorial.Step
    | ViewMtxPaypalButton { count : Int, cost : Int }
    | ViewMtxKongButton { count : Int, cost : Int }
    | ViewChangelogTitle
    | ViewChangelogSource
    | ViewChangelogScarybee
    | ViewNotFound
    | ViewMtxHistoryTitle
    | ViewMtxHistoryColKongId
    | ViewMtxHistoryColOrder
    | ViewMtxHistoryColAdded
    | ViewMtxHistoryColDate
    | ViewMtxHistoryColPlayFabId
    | ViewMtxHistoryColPayPalTx
    | ViewMtxHistoryAddedYes
    | ViewMtxHistoryAddedNo
    | ViewMtxShopConvert
    | ViewMtxShopHistory
    | ViewMtxShopConfirmed
    | ViewMtxShopPaypalTx String
    | ViewMtxShopBuyKreds
    | ViewMtxShopSupport
    | ViewMtxShopLogin
    | ViewEnergyLinkCrystals
    | ViewBuyNFormatLink
    | ViewBuyNFormatTitle
    | ViewBuyNFormatClose
    | ViewUserErrorLoggedOut
    | ViewUserErrorLoggedOutRemember
    | DescMutantMeatProd Unit.Id Decimal
    | DescTerritoryLegendEntry Unit.Id Float
    | DescHatcheryCrystals Item.Id { cooldown : Duration, count : Decimal }
    | DescHatcheryMutagen Item.Id { chance : Float, count : Decimal }
    | DescHatcheryMutagenLol
    | DescHatcheryMutagenUnlock Item.Id { count : Decimal, threshold : Decimal }
    | DescCocoon1
    | DescCocoon2 QueryDict
    | DescCocoon3 QueryDict (Result String Route) (String -> Result String Decimal)
    | DescCloneLarvae1 (String -> Result String Decimal)
    | DescCloneLarvae2 QueryDict (Result String Route) (String -> Result String Decimal)
    | DescCloneLarvae3 (String -> Result String Decimal)
    | KeyCombo String


type alias Lang msg =
    { locale : Locale
    , rstr : Text -> Result String String
    , rhtml : Text -> Result String (H.Html msg)
    , str : Text -> String
    , html : Text -> H.Html msg
    }


init : Locale -> (Text -> Result String String) -> (Text -> Result String (H.Html msg)) -> Lang msg
init locale str html =
    Lang locale str html (str >> silentText identity) (html >> silentText H.text)


silentText : (String -> a) -> Result String a -> a
silentText mapErr res =
    case res of
        Ok val ->
            val

        Err err ->
            mapErr <| "???" ++ err ++ "???"


{-| Context from the game used in many messages that rarely changes.

For example, formatting options and upgrades.

-}
type alias GameContext =
    { upgrades : Dict String Decimal
    , numberFormat : NumberFormat
    , velocityUnit : VelocityUnit
    , velocitySecs : Float
    }


velocityUnit : String -> GameContext -> VelocityUnit
velocityUnit item ctx =
    case ( item, ctx.velocityUnit ) of
        ( "energy", VelocityUnit.Swarmwarp ) ->
            VelocityUnit.Second

        _ ->
            ctx.velocityUnit


velocitySecs : String -> GameContext -> Float
velocitySecs item ctx =
    case ( item, ctx.velocityUnit ) of
        ( "energy", VelocityUnit.Swarmwarp ) ->
            1

        _ ->
            ctx.velocitySecs


localeToLang : GameData -> Fools -> Maybe GameContext -> Maybe GameData.Lang.Entry -> Locale -> Lang msg
localeToLang gameData fools ctx translator locale =
    let
        numberFormat =
            ctx |> Maybe.Extra.unwrap NumberFormat.default .numberFormat

        f =
            { en_us = View.Lang.En_US.formatter numberFormat
            }
    in
    case locale of
        Locale.En_US ->
            init locale
                (toString (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx)
                (toHtml (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx)

        Locale.En_Reverse ->
            init locale
                (toString (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx
                    >> Result.map String.reverse
                )
                (toHtml (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx
                    >> Result.map (\c -> H.span [ A.class "lang-backwards" ] [ c ])
                )

        Locale.En_UpsideDown ->
            init locale
                (toString (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx)
                (toHtml (gameData |> GameData.lang translator |> .en_us) f.en_us fools gameData ctx
                    >> Result.map (\c -> H.span [ A.class "lang-upside-down" ] [ c ])
                )


gameContext : Game -> GameContext
gameContext game =
    { upgrades = Game.upgradesDict game
    , numberFormat = Game.numberFormat game
    , velocityUnit = Game.velocityUnit game
    , velocitySecs = Game.velocityUnitSeconds game |> Result.withDefault 1
    }


gameToLang : Session -> Game -> Lang msg
gameToLang session game =
    localeToLang (Game.gameData game)
        (Fools.status session)
        (Just <| gameContext game)
        (Session.translatorEntry session)
        (Game.locale game |> Maybe.withDefault Locale.En_US)


gameDataToLang : Session -> GameData -> Lang msg
gameDataToLang session gd =
    let
        locale : Locale
        locale =
            session
                |> Session.game
                |> RemoteData.toMaybe
                |> Maybe.andThen Game.locale
                |> Maybe.withDefault Locale.En_US
    in
    localeToLang gd (Fools.status session) Nothing (Session.translatorEntry session) locale


sessionToLang : Session -> Result String (Lang msg)
sessionToLang session =
    Session.gameData session |> Result.map (gameDataToLang session)


toString : Dict String Message -> Formatter -> Fools -> GameData -> Maybe GameContext -> Text -> Result String String
toString messages format fools gameData ctx txt =
    let
        eval : String -> (String -> Result String String) -> Result String String
        eval key getVar =
            Dict.get key messages
                |> Result.fromMaybe ("no such message: " ++ key)
                |> Result.andThen (MessageFormat.format getVar)

        listVars : List ( String, String ) -> String -> Result String String
        listVars =
            Dict.fromList >> dictVars

        dictVars : Dict String String -> String -> Result String String
        dictVars dict =
            \key -> Dict.get key dict |> Result.fromMaybe ("no such var: " ++ key)

        emptyVars : String -> Result String String
        emptyVars =
            always <| Err "no vars"

        foolsVar =
            ( "fools"
            , if fools == Fools.On then
                "1"

              else
                "0"
            )

        suffixes : (String -> Result String String) -> String -> Result String String
        suffixes vars fullname =
            case vars fullname of
                Ok val ->
                    Ok val

                Err err ->
                    case String.split "." fullname of
                        name :: "roman" :: [] ->
                            case ctx of
                                Just { upgrades } ->
                                    case GameData.upgradeByName name gameData of
                                        Nothing ->
                                            Err err

                                        Just _ ->
                                            upgrades
                                                |> Dict.get name
                                                |> Maybe.andThen (Decimal.floor >> Result.toMaybe)
                                                |> Maybe.withDefault 0
                                                |> format.roman
                                                |> Ok

                                Nothing ->
                                    Err err

                        _ ->
                            Err err

        suffixedUnitLabel : Decimal -> Unit.Id -> Result String String
        suffixedUnitLabel n unitId =
            eval ("unit." ++ Unit.idToString unitId ++ ".label") <|
                suffixes <|
                    listVars
                        [ ( "n", n |> format.decimal )
                        , foolsVar
                        , ( "suffix", "1" )
                        ]

        unitLabel : Decimal -> Unit.Id -> Result String String
        unitLabel n unitId =
            eval ("unit." ++ Unit.idToString unitId ++ ".label") <|
                listVars
                    [ ( "n", n |> format.decimal )
                    , foolsVar
                    , ( "suffix", "0" )
                    ]

        upgradeLabel : Upgrade.Id -> Result String String
        upgradeLabel upgradeId =
            eval ("upgrade." ++ Upgrade.idToString upgradeId ++ ".label") <| listVars [ foolsVar ]

        spellLabel : Decimal -> Spell.Id -> Result String String
        spellLabel n spellId =
            eval ("spell." ++ Spell.idToString spellId ++ ".label") <|
                listVars
                    [ ( "n", n |> format.decimal )
                    , foolsVar
                    ]

        itemLabel : Decimal -> Item.Id -> Result String String
        itemLabel n itemId =
            case itemId of
                ItemId.UnitId uid ->
                    unitLabel n uid

                ItemId.UpgradeId uid ->
                    upgradeLabel uid

                ItemId.SpellId uid ->
                    spellLabel n uid

        suffixedItemLabel : Decimal -> Item.Id -> Result String String
        suffixedItemLabel n itemId =
            case itemId of
                ItemId.UnitId uid ->
                    suffixedUnitLabel n uid

                ItemId.UpgradeId uid ->
                    upgradeLabel uid

                ItemId.SpellId uid ->
                    spellLabel n uid

        unitListItem : Unit.Id -> Decimal -> Result String String
        unitListItem id n =
            Result.andThen
                (\label ->
                    eval "unit.count" <|
                        listVars
                            [ ( "n", n |> format.decimal )
                            , ( "label", label )
                            ]
                )
                (unitLabel n id)

        unitList : List ( Unit.Id, Decimal ) -> Result String String
        unitList =
            List.map (\( id, d ) -> unitListItem id d) >> Result.Extra.combine >> Result.map format.list

        descriptionFormat : (String -> Result String Decimal) -> String -> Result String String
        descriptionFormat varsFn0 name =
            let
                varsFn name_ =
                    case name_ of
                        "fools" ->
                            Ok <|
                                if fools == Fools.On then
                                    Decimal.one

                                else
                                    Decimal.zero

                        _ ->
                            varsFn0 name_

                var chunks fmt =
                    chunks |> List.reverse |> String.join "." |> varsFn |> Result.map fmt
            in
            case String.split "." name |> List.reverse of
                "decimal" :: chunks ->
                    var chunks format.decimal

                "smallDecimal" :: chunks ->
                    var chunks format.smallDecimal

                "percent1" :: chunks ->
                    var chunks (Decimal.toFloat >> format.percent1)

                "percent" :: chunks ->
                    var chunks (Decimal.toFloat >> format.percent)

                "longDuration" :: chunks ->
                    var chunks (Decimal.toFloat >> Duration.fromSecs >> format.longDuration)

                "shortDuration" :: chunks ->
                    var chunks (Decimal.toFloat >> Duration.fromSecs >> format.shortDuration)

                _ ->
                    varsFn name |> Result.map format.decimal

        velocityLabel : VelocityUnit -> Result String String
        velocityLabel df =
            eval ("velocityUnit." ++ VelocityUnit.toString df) emptyVars
    in
    case txt of
        DebugDecimal d ->
            Ok <| format.decimal d

        Title ->
            eval "title" emptyVars

        Header ->
            eval "header" emptyVars

        FrameRate fps { max, avg } ->
            eval "frameRate" <|
                listVars
                    [ ( "fps", fps |> format.int )
                    , ( "max", max |> Duration.toMillis |> format.int )
                    , ( "avg", avg |> Duration.toMillis |> format.int )
                    ]

        ServiceWorkerUpdateDownloaded ->
            eval "updateNow" emptyVars

        UnitDesc varsFn unitId ->
            eval ("unit." ++ Unit.idToString unitId ++ ".desc") (descriptionFormat varsFn)

        UpgradeDesc varsFn upgradeId ->
            eval ("upgrade." ++ Upgrade.idToString upgradeId ++ ".desc") (descriptionFormat varsFn)

        SpellDesc varsFn spellId ->
            eval ("spell." ++ Spell.idToString spellId ++ ".desc") (descriptionFormat varsFn)

        UnitLol unitId ->
            eval ("unit." ++ Unit.idToString unitId ++ ".lol") emptyVars

        UpgradeLol upgradeId ->
            eval ("upgrade." ++ Upgrade.idToString upgradeId ++ ".lol") emptyVars

        SpellLol spellId ->
            eval ("spell." ++ Spell.idToString spellId ++ ".lol") emptyVars

        ItemName n itemId ->
            itemLabel n itemId

        AchievementName achieveId ->
            eval ("achievement." ++ Achievement.idToString achieveId ++ ".label") emptyVars

        AchievementDesc achieveId (Just ( itemId, threshold )) ->
            let
                key =
                    "achievement." ++ Item.idToString itemId ++ ".desc"

                type_ =
                    case itemId of
                        ItemId.UnitId _ ->
                            "unit"

                        ItemId.UpgradeId _ ->
                            "upgrade"

                        ItemId.SpellId _ ->
                            "spell"
            in
            Result.andThen
                (\vars ->
                    if Dict.member key messages then
                        eval key <| listVars vars

                    else
                        eval ("achievement.desc." ++ type_) <| listVars vars
                )
                (itemLabel threshold itemId
                    |> Result.map
                        (\label ->
                            [ ( "label", label )
                            , ( "threshold", threshold |> format.decimal )
                            ]
                        )
                )

        AchievementDesc achieveId mItem ->
            Result.andThen
                (\varList ->
                    eval ("achievement." ++ Achievement.idToString achieveId ++ ".desc") <| listVars varList
                )
                (case mItem of
                    Just ( itemId, threshold ) ->
                        itemLabel threshold itemId
                            |> Result.map
                                (\label ->
                                    [ ( "label", label )
                                    , ( "threshold", threshold |> format.decimal )
                                    ]
                                )

                    Nothing ->
                        Ok []
                )

        AchievementLongDesc achieveId ->
            eval ("achievement." ++ Achievement.idToString achieveId ++ ".longdesc") emptyVars
                -- longdesc is optional, often missing. that's allowed.
                |> Result.withDefault ""
                |> Ok

        -- view.error.*
        ViewErrorTitle ->
            eval "view.error.title" emptyVars

        ViewErrorTime { at, now } ->
            -- reldate at now ++ "; " ++ date at
            eval "view.error.time" <| listVars [ ( "at", at |> format.date ) ]

        ViewErrorTimeUnknown ->
            eval "view.error.time.unknown" emptyVars

        ViewErrorMore n ->
            eval "view.error.more" <| listVars [ ( "n", n |> format.int ) ]

        ViewErrorCloseButton ->
            eval "view.error.closeButton" emptyVars

        ViewErrorGameLoad ->
            eval "view.error.game.load" emptyVars

        ViewErrorGameExport ->
            eval "view.error.game.export" emptyVars

        ViewErrorGameBug ->
            eval "view.error.game.bug" emptyVars

        ViewErrorGameReportButton ->
            eval "view.error.game.reportButton" emptyVars

        ViewErrorGameResetButton ->
            eval "view.error.game.resetButton" emptyVars

        -- view.unitSidebar.*
        ViewUnitSidebarUnitName id ->
            suffixedUnitLabel Decimal.one id

        ViewUnitSidebarSpellName id ->
            spellLabel Decimal.one id

        ViewUnitSidebarAchievementPoints ->
            eval "view.unitSidebar.achievementPoints" emptyVars

        ViewUnitSidebarCount d ->
            Ok <| format.decimalShort d

        ViewTabVelocity unit v ->
            -- energy should show decimals, but most other units - including a
            -- single drone early-game - should not
            let
                formatter =
                    if Decimal.lt v Decimal.one then
                        format.smallDecimalShort

                    else
                        format.decimalShort

                vs =
                    Maybe.Extra.unwrap 1 (velocitySecs (Unit.name unit)) ctx
            in
            Result.andThen
                (\vu ->
                    eval "view.tab.velocity" <|
                        listVars
                            [ ( "velocity", v |> Decimal.mulFloat vs |> formatter )

                            -- , ( "velocityUnit", vu )
                            , ( "velocityUnit", Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.name unit)) ctx |> VelocityUnit.toString )
                            ]
                )
                (velocityLabel <| Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.name unit)) ctx)

        -- view.[unit|spell].*
        ViewUnitHeader unitId ->
            suffixedUnitLabel Decimal.one unitId

        ViewUnitCount n unitId ->
            Result.andThen
                (\label ->
                    eval "view.unit.count" <|
                        listVars
                            [ ( "n", n |> format.decimal )
                            , ( "name", unitId |> Unit.idToString )
                            , ( "label", label )
                            ]
                )
                (suffixedUnitLabel n unitId)

        ViewUnitCappedCount n cap unitId ->
            Result.andThen
                (\label ->
                    eval "view.unit.count.capped" <|
                        listVars
                            [ ( "n", n |> format.decimal )
                            , ( "cap", cap |> format.decimal )
                            , ( "name", unitId |> Unit.idToString )
                            , ( "label", label )
                            ]
                )
                (suffixedUnitLabel n unitId)

        ViewUnitProdEach prod ->
            -- energy should show decimals, but most other units - including a
            -- single drone early-game - should not
            let
                formatter =
                    if Decimal.lt prod.base Decimal.one then
                        format.smallDecimal

                    else
                        format.decimal

                vs =
                    Maybe.Extra.unwrap 1 (velocitySecs (Unit.idToString prod.produces.id)) ctx
            in
            Result.map2
                (\label vu ->
                    eval "view.unit.prod.each" <|
                        listVars
                            [ ( "label", label )
                            , ( "velocityUnit", vu )
                            , ( "each", prod.each |> Decimal.mulFloat vs |> formatter )
                            , ( "bonus", prod.bonus |> format.smallDecimal )
                            ]
                )
                (unitLabel prod.each prod.produces.id)
                (velocityLabel <| Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.idToString prod.produces.id)) ctx)
                |> Result.andThen identity

        ViewUnitProdTotal prod ->
            -- energy should show decimals, but most other units - including a
            -- single drone early-game - should not
            let
                formatter =
                    if Decimal.lt prod.base Decimal.one then
                        format.smallDecimal

                    else
                        format.decimal

                vs =
                    Maybe.Extra.unwrap 1 (velocitySecs (Unit.idToString prod.produces.id)) ctx
            in
            Result.map2
                (\label vu ->
                    eval "view.unit.prod.total" <|
                        listVars
                            [ ( "label", label )
                            , ( "velocityUnit", vu )
                            , ( "total", prod.total |> Decimal.mulFloat vs |> formatter )
                            , ( "bonus", prod.bonus |> format.decimal )
                            ]
                )
                (unitLabel prod.total prod.produces.id)
                (velocityLabel <| Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.idToString prod.produces.id)) ctx)
                |> Result.andThen identity

        ViewUnitBuyCost n0 unitId costs0 ->
            Result.map2
                (\label costs ->
                    eval "view.unit.buyCost" <|
                        listVars
                            [ ( "label", label )
                            , ( "costs", costs )
                            , ( "n", n0 |> resToDec |> format.decimal )
                            , ( "name", unitId |> Unit.idToString )
                            ]
                )
                (suffixedUnitLabel (resToDec n0) unitId)
                (unitList costs0)
                |> Result.andThen identity

        ViewUnitUpgradesTitle ->
            eval "view.unit.upgrades.title" emptyVars

        ViewUnitUpgradeHeader n upgradeId ->
            Result.andThen
                (\label ->
                    eval "view.unit.upgrade.header" <|
                        listVars
                            [ ( "label", label )
                            , ( "n", n |> format.decimal )
                            ]
                )
                (upgradeLabel upgradeId)

        ViewUnitUpgradeBuyCost costs0 ->
            Result.andThen
                (\costs ->
                    eval "view.unit.upgrade.buyCost" <| listVars [ ( "costs", costs ) ]
                )
                (unitList costs0)

        ViewUnitBuyN n0 ->
            eval "view.unit.buyN" <| listVars [ ( "n", n0 |> resToDec |> format.decimal ) ]

        ViewUnitCantBuy ->
            eval "view.unit.cantBuy" emptyVars

        ViewUnitAscend ->
            eval "view.unit.ascend" emptyVars

        ViewUnitVelocity unitId velocity ->
            let
                vs =
                    Maybe.Extra.unwrap 1 (velocitySecs (Unit.idToString unitId)) ctx
            in
            Result.map2
                (\label vu ->
                    eval "view.unit.velocity" <|
                        listVars
                            [ ( "velocity", velocity |> Decimal.mulFloat vs |> format.decimal )
                            , ( "label", label )
                            , ( "velocityUnit", vu )
                            ]
                )
                (unitLabel Decimal.zero unitId)
                (velocityLabel <| Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.idToString unitId)) ctx)
                |> Result.andThen identity

        ViewUnitHatchableRate unitId { rate, percentPerCost } ->
            let
                entry : Unit.Id -> Float -> Result String String
                entry id percent =
                    Result.andThen
                        (\label ->
                            eval "view.unit.hatchableRate.percent" <|
                                listVars
                                    [ ( "percent", percent |> format.percent )
                                    , ( "label", label )
                                    ]
                        )
                        (unitLabel Decimal.one id)

                list : List ( Unit.Id, Float ) -> Result String String
                list =
                    List.map (\( id, pct ) -> entry id pct) >> Result.Extra.combine >> Result.map format.list

                vs =
                    Maybe.Extra.unwrap 1 (velocitySecs (Unit.idToString unitId)) ctx
            in
            Result.map3
                (\label percents vu ->
                    eval "view.unit.hatchableRate" <|
                        listVars
                            [ ( "rate", rate |> Decimal.mulFloat vs |> format.decimal )
                            , ( "label", label )
                            , ( "velocityUnit", vu )
                            , ( "percentList", percents )
                            ]
                )
                (unitLabel Decimal.zero unitId)
                (list percentPerCost)
                (velocityLabel <| Maybe.Extra.unwrap VelocityUnit.default (velocityUnit (Unit.idToString unitId)) ctx)
                |> Result.andThen identity

        ViewSpellHeader spellId ->
            spellLabel Decimal.one spellId

        ViewSpellBuyCost n0 spellId costs0 ->
            Result.map2
                (\label costs ->
                    eval "view.spell.buyCost" <|
                        listVars
                            [ ( "label", label )
                            , ( "costs", costs )
                            , ( "n", n0 |> resToDec |> format.decimal )
                            , ( "name", spellId |> Spell.idToString )
                            ]
                )
                (spellLabel (resToDec n0) spellId)
                (unitList costs0)
                |> Result.andThen identity

        -- view.ascend.*
        ViewAscendPlot ->
            eval "view.ascend.plot" emptyVars

        ViewAscendBuyCost n0 costs0 getVar ->
            Result.map
                (\costs ->
                    let
                        vars =
                            listVars
                                [ ( "costs", costs )
                                , ( "n", n0 |> resToDec |> format.decimal )
                                ]
                    in
                    eval "view.unit.buyCost.ascend" <|
                        -- ugh, so messy below
                        \key ->
                            case vars key of
                                Ok val ->
                                    Ok val

                                Err err ->
                                    getVar key
                                        |> Maybe.withDefault Decimal.zero
                                        |> format.decimal
                                        |> Ok
                )
                (unitList costs0)
                |> Result.andThen identity

        ViewAscendProgressCapDiff diff ->
            eval "view.ascend.progress.capDiff" <| listVars [ ( "diff", diff |> format.decimal ) ]

        -- view.tab.*
        ViewTabCount unitId n p ->
            Result.andThen
                (\label ->
                    case ( p.capPct, p.count2 ) of
                        ( Just capPct, _ ) ->
                            eval "view.tabnav.count.capped" <|
                                listVars
                                    [ ( "label", label )
                                    , ( "n", n |> format.decimalShort )
                                    , ( "capPct", capPct |> format.percent )
                                    ]

                        ( Nothing, Just n2 ) ->
                            eval "view.tabnav.count.precount" <|
                                listVars
                                    [ ( "label", label )
                                    , ( "n", n |> format.decimalShort )
                                    , ( "n2", n2 |> format.decimalShort )
                                    ]

                        _ ->
                            eval "view.tabnav.count.uncapped" <|
                                listVars
                                    [ ( "label", label )
                                    , ( "n", n |> format.decimalShort )
                                    ]
                )
                (unitLabel n unitId)

        ViewTabMobileName unitId ->
            unitLabel Decimal.one unitId

        ViewTabMobileCount n ->
            Ok <| format.decimalShort n

        ViewTabMobileSecondary n p ->
            case ( p.capPct, p.count2 ) of
                ( Just capPct, _ ) ->
                    eval "view.tabnav.count.capped" <|
                        listVars
                            [ ( "label", "" )
                            , ( "n", "" )
                            , ( "capPct", capPct |> format.percent )
                            ]

                ( Nothing, Just n2 ) ->
                    eval "view.tabnav.count.precount" <|
                        listVars
                            [ ( "label", "" )
                            , ( "n", "" )
                            , ( "n2", n2 |> format.decimalShort )
                            ]

                _ ->
                    -- &nbsp;
                    -- https://twitter.com/rtfeldman/status/767263564214120448?lang=en
                    Ok "\u{00A0}"

        ViewTabMore ->
            eval "view.tabnav.more" emptyVars

        ViewTabBuyAll n ->
            eval "view.tabnav.buyAll" <| listVars [ ( "n", n |> format.int ) ]

        ViewTabBuyCheapest n ->
            eval "view.tabnav.buyCheapest" <| listVars [ ( "n", n |> format.int ) ]

        ViewTabUndo ->
            eval "view.tabnav.undo" emptyVars

        ViewTabUndoDuration dur ->
            eval "view.tabnav.undo.duration" <|
                listVars
                    [ ( "duration", dur |> Duration.toSecs |> floor |> format.int )
                    ]

        ViewTabOptions ->
            eval "view.tabnav.options" emptyVars

        ViewTabAchievements ->
            eval "view.tabnav.achievements" emptyVars

        ViewTabStatistics ->
            eval "view.tabnav.statistics" emptyVars

        ViewTabChangelog ->
            eval "view.tabnav.changelog" emptyVars

        ViewTabCommunity ->
            eval "view.tabnav.community" emptyVars

        ViewTabFeedback ->
            eval "view.tabnav.feedback" emptyVars

        ViewTabBugReport ->
            eval "view.tabnav.bugReport" emptyVars

        ViewTabAllUnits ->
            eval "view.tabnav.allUnits" emptyVars

        ViewTabLogin ->
            eval "view.tabnav.login" emptyVars

        ViewTabRegister ->
            eval "view.tabnav.register" emptyVars

        ViewTabHotkeys ->
            eval "view.tabnav.hotkeys" emptyVars

        ViewTabNavUserLoading ->
            eval "view.tabnav.user.loading" emptyVars

        ViewTabNavUserError ->
            eval "view.tabnav.user.error" emptyVars

        ViewTabNavUserConflict ->
            eval "view.tabnav.user.conflict" emptyVars

        ViewTabNavUserKongLogin ->
            eval "view.tabnav.user.konglogin" emptyVars

        -- view.options.*
        ViewOptionsTitle ->
            eval "view.options.title" emptyVars

        ViewOptionsExport ->
            eval "view.options.export" emptyVars

        ViewOptionsNotYetSaved ->
            eval "view.options.export.empty" emptyVars

        ViewOptionsResetSaveLink ->
            eval "view.options.reset.link" emptyVars

        ViewOptionsResetSaveWarning ->
            eval "view.options.reset.warning" emptyVars

        ViewOptionsResetSaveCancel ->
            eval "view.options.reset.cancel" emptyVars

        ViewOptionsResetSaveConfirm ->
            eval "view.options.reset.confirm" emptyVars

        ViewOptionsHelpTranslate ->
            eval "view.options.reset.helpTranslate" emptyVars

        ViewOptionsLocale locale ->
            eval ("locale." ++ Locale.toString locale) emptyVars

        ViewOptionsTheme theme ->
            eval "view.options.theme" <| listVars [ ( "theme", Theme.toString theme ) ]

        ViewOptionsNumberFormat nf ->
            eval ("numberFormat." ++ NumberFormat.toString nf) emptyVars

        ViewOptionsVelocityUnit df ->
            eval ("view.options.velocityUnit." ++ VelocityUnit.toString df) emptyVars

        ViewOptionsToggleDebug nextDebug ->
            if nextDebug then
                eval "view.options.debug.enable" emptyVars

            else
                eval "view.options.debug.disable" emptyVars

        ViewOptionsToggleFps nextFps ->
            if nextFps then
                eval "view.options.fps.enable" emptyVars

            else
                eval "view.options.fps.disable" emptyVars

        ViewOptionsAnalyticsDesc ->
            eval "view.options.analytics.desc" emptyVars

        ViewOptionsAnalyticsOptOutLink ->
            eval "view.options.analytics.optOutLink" emptyVars

        ViewOptionsAnalyticsPrivacyLink ->
            eval "view.options.analytics.privacyLink" emptyVars

        -- view.statistics.*
        ViewStatisticsTitle ->
            eval "view.statistics.title" emptyVars

        ViewStatisticsHeaderName ->
            eval "view.statistics.label.name" emptyVars

        ViewStatisticsHeaderEntryCreated ->
            eval "view.statistics.label.created" emptyVars

        ViewStatisticsHeaderClicks ->
            eval "view.statistics.label.clicks" emptyVars

        ViewStatisticsHeaderNum ->
            eval "view.statistics.label.rawCount" emptyVars

        ViewStatisticsHeaderTwinNum ->
            eval "view.statistics.label.twinCount" emptyVars

        ViewStatisticsEntryCreated duration ->
            Ok <| format.shortDuration duration

        ViewStatisticsClicks n ->
            Ok <| format.int n

        ViewStatisticsDecimal d ->
            Ok <| format.decimal d

        ViewStatisticsHeaderSaveSize ->
            eval "view.statistics.label.persist.size" emptyVars

        ViewStatisticsSaveSize mn ->
            case mn of
                Nothing ->
                    eval "view.statistics.persist.size.empty" emptyVars

                Just n ->
                    eval "view.statistics.persist.size" <| listVars [ ( "n", n |> format.int ) ]

        ViewStatisticsHeaderCreated ->
            eval "view.statistics.label.persist.created" emptyVars

        ViewStatisticsCreated { now, created } ->
            eval "view.statistics.persist.created" <|
                listVars
                    [ ( "ago", format.shortDuration <| Duration.since { before = created, after = now } )
                    , ( "created", Iso8601.fromTime created )
                    ]

        ViewStatisticsItemName id ->
            itemLabel Decimal.one id

        -- view.achievements.*
        ViewAchievementsTitle points ->
            eval "view.achievements.title" <| listVars [ ( "points", points |> format.int ) ]

        ViewAchievementsProgress pct ->
            eval "view.achievements.progress" <| listVars [ ( "pct", pct |> format.percent ) ]

        ViewAchievementsEntryDate { at, now } ->
            eval "view.achievements.entry.earned" <| listVars [ ( "at", format.reldate at now ) ]

        ViewAchievementsEntryProgress pct count total ->
            eval "view.achievements.entry.progress" <|
                listVars
                    [ ( "pct", pct |> format.percent )
                    , ( "count", count |> format.decimal )
                    , ( "total", total |> format.decimal )
                    ]

        ViewAchievementsEntryMasked ->
            eval "view.achievements.entry.masked" emptyVars

        ViewAchievementsEntryUnlocked ->
            eval "view.achievements.entry.unlocked" emptyVars

        ViewAchievementsBonus bonus ->
            eval "view.achievements.bonus" <|
                listVars
                    [ ( "bonus", bonus |> format.percent1 )
                    ]

        ViewAchievementsOptionsVisible ->
            eval "view.achievements.options.visible" emptyVars

        ViewAchievementsOptionsVisibleEarned ->
            eval "view.achievements.options.visible.earned" emptyVars

        ViewAchievementsOptionsVisibleUnearned ->
            eval "view.achievements.options.visible.unearned" emptyVars

        ViewAchievementsOptionsVisibleMasked ->
            eval "view.achievements.options.visible.masked" emptyVars

        ViewAchievementsOptionsSort ->
            eval "view.achievements.options.order" emptyVars

        ViewAchievementsOptionsSortDefault ->
            eval "view.achievements.options.order.default" emptyVars

        ViewAchievementsOptionsSortCompletion ->
            eval "view.achievements.options.order.completion" emptyVars

        ViewAchievementsOptionsSortReversed ->
            eval "view.achievements.options.reverse.true" emptyVars

        ViewAchievementsOptionsSortNotReversed ->
            eval "view.achievements.options.reverse.false" emptyVars

        ViewCostMin n0 itemId ->
            Result.map
                (\label ->
                    eval "view.cost.min" <|
                        listVars
                            [ ( "label", label )
                            , ( "n", n0 |> resToDec |> format.decimal )
                            , ( "name", itemId |> Item.idToString )
                            ]
                )
                (suffixedItemLabel (resToDec n0) itemId)
                |> Result.andThen identity

        ViewCostMet { currency, cost } pct ->
            Result.andThen
                (\label ->
                    eval "view.cost.met" <|
                        listVars
                            [ ( "currency", label )
                            , ( "cost", cost |> format.decimal )
                            , ( "percent", pct |> format.percent )
                            ]
                )
                (unitLabel cost currency)

        ViewCostNotMet { currency, cost } pct duration ->
            Result.andThen
                (\label ->
                    case duration of
                        Nothing ->
                            eval "view.cost.notmet" <|
                                listVars
                                    [ ( "currency", label )
                                    , ( "cost", cost |> format.decimal )
                                    , ( "percent", pct |> format.percent )
                                    ]

                        Just d ->
                            eval "view.cost.notmet.duration" <|
                                listVars
                                    [ ( "currency", label )
                                    , ( "cost", cost |> format.decimal )
                                    , ( "percent", pct |> format.percent )
                                    , ( "duration", d |> format.shortDuration )
                                    ]
                )
                (unitLabel cost currency)

        ViewFoolsInvitedLol ->
            eval "view.fools.invited.lol" <| emptyVars

        ViewFoolsInvitedTitle ->
            eval "view.fools.invited.title" <| emptyVars

        ViewFoolsInvitedBody ->
            eval "view.fools.invited.body" <| emptyVars

        ViewFoolsInvitedLink ->
            eval "view.fools.invited.link" <| emptyVars

        ViewFoolsOnLol ->
            eval "view.fools.on.lol" <| emptyVars

        ViewFoolsOnTitle ->
            eval "view.fools.on.title" <| emptyVars

        ViewFoolsOnBody ->
            eval "view.fools.on.body" <| emptyVars

        ViewFoolsOnLink ->
            eval "view.fools.on.link" <| emptyVars

        ViewFoolsAfterTitle ->
            eval "view.fools.after.title" <| emptyVars

        ViewFoolsAfterLink ->
            eval "view.fools.after.link" <| emptyVars

        ViewConflictTitle ->
            eval "view.conflict.title" <| emptyVars

        ViewConflictDesc ->
            eval "view.conflict.desc" <| emptyVars

        ViewConflictExportLocal ->
            eval "view.conflict.export.local" <| emptyVars

        ViewConflictExportRemote ->
            eval "view.conflict.export.remote" <| emptyVars

        ViewConflictGameCreated created ->
            eval "view.conflict.game.created" <|
                listVars
                    [ ( "created", Iso8601.fromTime created )
                    ]

        ViewConflictButtonLocal ->
            eval "view.conflict.button.local" <| emptyVars

        ViewConflictButtonLogout ->
            eval "view.conflict.button.logout" <| emptyVars

        ViewConflictButtonRemote ->
            eval "view.conflict.button.remote" <| emptyVars

        ViewConflictGameEncoded encoded ->
            eval "view.conflict.game.encoded" <|
                listVars
                    [ ( "encoded", Iso8601.fromTime encoded )
                    ]

        ViewLoginTitle ->
            eval "view.login.title" <| emptyVars

        ViewLoginFieldEmail ->
            eval "view.login.field.email" <| emptyVars

        ViewLoginFieldPassword ->
            eval "view.login.field.password" <| emptyVars

        ViewLoginFieldRememberMe ->
            eval "view.login.field.rememberme" <| emptyVars

        ViewLoginSubmit ->
            eval "view.login.submit" <| emptyVars

        ViewLoginLinkRegister ->
            eval "view.login.link.register" <| emptyVars

        ViewLoginLinkResetPassword ->
            eval "view.login.link.resetpassword" <| emptyVars

        ViewRegisterTitle ->
            eval "view.register.title" <| emptyVars

        ViewRegisterFieldEmail ->
            eval "view.register.field.email" <| emptyVars

        ViewRegisterFieldPassword ->
            eval "view.register.field.password" <| emptyVars

        ViewRegisterFieldPassword2 ->
            eval "view.register.field.password2" <| emptyVars

        ViewRegisterFieldRememberMe ->
            eval "view.register.field.rememberme" <| emptyVars

        ViewRegisterSubmit ->
            eval "view.register.submit" <| emptyVars

        ViewRegisterLinkLogin ->
            eval "view.register.link.login" <| emptyVars

        ViewUserAnonymous ->
            eval "view.user.anonymous" <| emptyVars

        ViewUserAnonymousLogin ->
            eval "view.user.anonymous.login" <| emptyVars

        ViewUserAnonymousRegister ->
            eval "view.user.anonymous.register" <| emptyVars

        ViewUserTitle ->
            eval "view.user.title" <| emptyVars

        ViewUserDescPush1 ->
            eval "view.user.desc.push1" <| emptyVars

        ViewUserDescPush2 dur ->
            eval "view.user.desc.push2" <|
                listVars
                    [ ( "duration", format.timerDuration dur )
                    ]

        ViewUserPushSuccess dur ->
            eval "view.user.push.success" <|
                listVars
                    [ ( "succeeded", format.timerDuration dur )
                    ]

        ViewUserPushFailure dur ->
            eval "view.user.push.failure" <|
                listVars
                    [ ( "requested", format.timerDuration dur )
                    ]

        ViewUserPushNone ->
            eval "view.user.push.none" <| emptyVars

        ViewUserPushError err ->
            eval "view.user.push.error" <|
                listVars
                    [ ( "error", err )
                    ]

        ViewUserPushReady ->
            eval "view.user.push.cooldown.ready" <| emptyVars

        ViewUserPushNotReady dur pct ->
            eval "view.user.push.cooldown.notready" <|
                listVars
                    [ ( "requested", format.timerDuration dur )
                    , ( "percent", format.percent pct )
                    ]

        ViewUserButtonForcePush ->
            eval "view.user.button.forcepush" <| emptyVars

        ViewUserButtonLogout ->
            eval "view.user.button.logout" <| emptyVars

        ViewUserButtonChangePassword ->
            eval "view.user.button.password" <| emptyVars

        ViewResetPasswordTitle ->
            eval "view.resetpassword.title" <| emptyVars

        ViewResetPasswordFieldEmail ->
            eval "view.resetpassword.field.email" <| emptyVars

        ViewResetPasswordSubmit ->
            eval "view.resetpassword.submit" <| emptyVars

        ViewWelcomeBackTitle ->
            eval "view.welcomeback.title" emptyVars

        ViewWelcomeBack dur ->
            eval "view.welcomeback.body" <|
                listVars
                    [ ( "duration", format.longDuration dur )
                    ]

        ViewWelcomeBackLol ->
            eval "view.welcomeback.lol" <| emptyVars

        ViewWelcomeBackClose ->
            eval "view.welcomeback.close" <| emptyVars

        ViewWelcomeBackEntry unit count ->
            Result.andThen
                (\label ->
                    eval "view.welcomeback.entry" <|
                        listVars
                            [ ( "label", label )
                            , ( "count", format.decimal count )
                            ]
                )
                (unitLabel count unit)

        ViewTutorial query step ->
            let
                unitUrl : String -> Maybe Route
                unitUrl name =
                    GameData.unitByName name gameData
                        |> Maybe.map (\u -> Route.unit (Tab.idToString u.tab) (Unit.name u))

                getVar : String -> Result String String
                getVar var =
                    case String.split "." var of
                        [ "url", unit ] ->
                            unitUrl unit
                                |> Maybe.map (Route.toString query)
                                |> Result.fromMaybe ("no such unit: " ++ unit)

                        _ ->
                            Err <| "unknown var: " ++ var
            in
            eval ("view.tutorial." ++ Tutorial.toString step) getVar

        ViewMtxPaypalButton { cost, count } ->
            eval "view.mtx.paypal.button" <|
                listVars
                    [ ( "cost", format.money cost )
                    , ( "count", format.fullInt count )
                    ]

        ViewMtxKongButton { cost, count } ->
            eval "view.mtx.kong.button" <|
                listVars
                    [ ( "cost", "![][/images/kred_single.png]" ++ format.fullInt cost )
                    , ( "count", format.fullInt count )
                    ]

        ViewChangelogTitle ->
            eval "view.changelog.title" emptyVars

        ViewChangelogSource ->
            eval "view.changelog.source" emptyVars

        ViewChangelogScarybee ->
            eval "view.changelog.scarybee" emptyVars

        ViewNotFound ->
            eval "view.notfound" emptyVars

        ViewMtxHistoryTitle ->
            eval "view.mtxhistory.title" emptyVars

        ViewMtxHistoryColKongId ->
            eval "view.mtxhistory.col.kongid" emptyVars

        ViewMtxHistoryColOrder ->
            eval "view.mtxhistory.col.order" emptyVars

        ViewMtxHistoryColAdded ->
            eval "view.mtxhistory.col.added" emptyVars

        ViewMtxHistoryColDate ->
            eval "view.mtxhistory.col.date" emptyVars

        ViewMtxHistoryColPlayFabId ->
            eval "view.mtxhistory.col.playfabid" emptyVars

        ViewMtxHistoryColPayPalTx ->
            eval "view.mtxhistory.col.paypaltx" emptyVars

        ViewMtxHistoryAddedYes ->
            eval "view.mtxhistory.added.yes" emptyVars

        ViewMtxHistoryAddedNo ->
            eval "view.mtxhistory.added.no" emptyVars

        ViewMtxShopConvert ->
            eval "view.mtxshop.convert" emptyVars

        ViewMtxShopHistory ->
            eval "view.mtxshop.history" emptyVars

        ViewMtxShopConfirmed ->
            eval "view.mtxshop.confirmed" emptyVars

        ViewMtxShopPaypalTx tx ->
            eval "view.mtxshop.paypaltx" <| listVars [ ( "tx", tx ) ]

        ViewMtxShopBuyKreds ->
            eval "view.mtxshop.buykreds" emptyVars

        ViewMtxShopSupport ->
            eval "view.mtxshop.support" emptyVars

        ViewMtxShopLogin ->
            eval "view.mtxshop.login" emptyVars

        ViewEnergyLinkCrystals ->
            eval "view.energy.linkcrystals" emptyVars

        ViewBuyNFormatLink ->
            eval "view.buyn.format.link" emptyVars

        ViewBuyNFormatTitle ->
            eval "view.buyn.format.title" emptyVars

        ViewBuyNFormatClose ->
            eval "view.buyn.format.close" emptyVars

        ViewUserErrorLoggedOut ->
            eval "view.usererror.loggedout" emptyVars

        ViewUserErrorLoggedOutRemember ->
            eval "view.usererror.loggedout.remember" emptyVars

        DescMutantMeatProd unit val ->
            Result.andThen
                (\label ->
                    eval "description.mutantmeat.prod" <|
                        listVars
                            [ ( "label", label )
                            , ( "val", format.percent1 <| Decimal.toFloat val )
                            ]
                )
                (unitLabel Decimal.infinity unit)

        DescTerritoryLegendEntry unit pct ->
            Result.andThen
                (\label ->
                    eval "description.territory.legend.entry" <|
                        listVars
                            [ ( "label", label )
                            , ( "percent", pct |> format.percent )
                            ]
                )
                (unitLabel Decimal.one unit)

        DescHatcheryMutagen item { chance, count } ->
            Result.andThen
                (\label ->
                    eval "description.hatchery.mutagen" <|
                        listVars
                            [ ( "chance", chance |> format.percent )
                            , ( "count", count |> format.decimal )
                            , ( "label", label )
                            ]
                )
                (itemLabel Decimal.one item)

        DescHatcheryMutagenLol ->
            eval "description.hatchery.mutagen.lol" emptyVars

        DescHatcheryMutagenUnlock item { count, threshold } ->
            Result.andThen
                (\label ->
                    eval "description.hatchery.mutagen.unlock" <|
                        listVars
                            [ ( "count", count |> format.decimal )
                            , ( "threshold", threshold |> format.decimal )
                            , ( "label", label )
                            ]
                )
                (itemLabel count item)

        DescHatcheryCrystals item { cooldown, count } ->
            Result.andThen
                (\label ->
                    eval
                        ("description.hatchery.crystal."
                            ++ (if cooldown == Duration.zero then
                                    "now"

                                else
                                    "cooldown"
                               )
                        )
                    <|
                        listVars
                            [ ( "cooldown", cooldown |> format.longDuration )
                            , ( "count", count |> format.decimal )
                            , ( "label", label )
                            ]
                )
                (itemLabel Decimal.one item)

        DescCocoon1 ->
            eval "unit.cocoon.desc1" emptyVars

        DescCocoon2 query ->
            eval "unit.cocoon.desc2" <|
                listVars
                    [ ( "url.clonelarvae", Route.toString query <| Route.Spell "energy" "clone-larvae" )
                    ]

        DescCocoon3 query capHref varsFn ->
            eval "unit.cocoon.desc3" <|
                \name ->
                    if name == "url.cocoon" then
                        capHref |> Result.map (Route.toString query)

                    else
                        descriptionFormat varsFn name

        DescCloneLarvae1 varsFn ->
            eval "spell.clonelarvae.desc1" <| descriptionFormat varsFn

        DescCloneLarvae2 query capHref varsFn ->
            eval "spell.clonelarvae.desc2" <|
                \name ->
                    if name == "url.cocoon" then
                        capHref |> Result.map (Route.toString query)

                    else
                        descriptionFormat varsFn name

        DescCloneLarvae3 varsFn ->
            eval "spell.clonelarvae.desc3" <| descriptionFormat varsFn

        KeyCombo name ->
            eval ("keyCombo." ++ name) emptyVars


markdownDefaultOptions =
    Markdown.defaultOptions


{-| Text that hasn't yet been translated, but should be.

When building a new page, inline strings are just _easier_ to start with than
all the translation boilerplate. Use this instead of Html.text during that early
stage. This will ease searching when it's time to add translations.

-}
text : String -> H.Html msg
text =
    H.text


toHtml : Dict String Message -> Formatter -> Fools -> GameData -> Maybe GameContext -> Text -> Result String (H.Html msg)
toHtml messages format fools gameData ctx txt =
    case txt of
        ViewFoolsOnBody ->
            toString messages format fools gameData ctx txt
                |> Result.map (Markdown.toHtml [])

        ViewTutorial _ _ ->
            toString messages format fools gameData ctx txt
                |> Result.andThen Parser.Tutorial.parse
                |> Result.map (Parser.Tutorial.eval >> H.div [])

        ViewMtxKongButton _ ->
            toString messages format fools gameData ctx txt
                -- this isn't a tutorial, but a quick hack to make the kreds image work
                |> Result.andThen Parser.Tutorial.parse
                |> Result.map (Parser.Tutorial.evalSpan >> H.span [])

        ViewChangelogScarybee ->
            toString messages format fools gameData ctx txt
                |> Result.map (Markdown.toHtml [])

        DescCocoon2 _ ->
            toString messages format fools gameData ctx txt
                |> Result.andThen Parser.Tutorial.parse
                |> Result.map (Parser.Tutorial.evalSpan >> H.span [])

        DescCocoon3 _ _ _ ->
            toString messages format fools gameData ctx txt
                |> Result.andThen Parser.Tutorial.parse
                |> Result.map (Parser.Tutorial.evalSpan >> H.span [])

        DescCloneLarvae2 _ _ _ ->
            toString messages format fools gameData ctx txt
                |> Result.andThen Parser.Tutorial.parse
                |> Result.map (Parser.Tutorial.evalSpan >> H.span [])

        -- support linebreaks in descriptions
        UnitDesc _ id ->
            -- unit.lol is required
            Result.map2
                (\desc lol ->
                    H.div [ A.title lol ]
                        (desc |> String.split "\n" |> List.map (\s -> H.p [ A.class <| "desc-" ++ Unit.idToString id ] [ H.text s ]))
                )
                (toString messages format fools gameData ctx txt)
                (toString messages format fools gameData ctx <| UnitLol id)

        UpgradeDesc _ id ->
            -- upgrade.lol is optional
            Result.map
                (\desc ->
                    H.div (UpgradeLol id |> toString messages format fools gameData ctx |> Result.Extra.unwrap [] (\lol -> [ A.title lol ]))
                        (desc |> String.split "\n" |> List.map (\s -> H.p [ A.class <| "desc-" ++ Upgrade.idToString id ] [ H.text s ]))
                )
                (toString messages format fools gameData ctx txt)

        SpellDesc _ id ->
            -- spell.lol is required
            Result.map2
                (\desc lol ->
                    H.div [ A.title lol ]
                        (desc |> String.split "\n" |> List.map (\s -> H.p [ A.class <| "desc-" ++ Spell.idToString id ] [ H.text s ]))
                )
                (toString messages format fools gameData ctx txt)
                (toString messages format fools gameData ctx <| SpellLol id)

        _ ->
            -- all other translations require no fancy html
            txt |> toString messages format fools gameData ctx |> Result.map H.text
