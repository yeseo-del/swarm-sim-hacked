module View.Error exposing (Msg(..), update, viewGameError, viewSessionError)

import Game exposing (Game)
import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)
import Time exposing (Posix)
import View.Icon
import View.Lang as T exposing (Lang)
import View.TabNav


type Msg
    = IgnoreSessionError
    | ResetSavedGame


update : Msg -> Session -> ( Session, Cmd Msg )
update msg session =
    case msg of
        IgnoreSessionError ->
            ( Session.dropError session, Cmd.none )

        ResetSavedGame ->
            case ( Session.gameData session, Session.now session ) of
                ( Ok gd, Just now ) ->
                    ( session |> Session.setGame True (Game.empty gd now), Cmd.none )

                _ ->
                    ( session, Cmd.none )


viewSessionError : Session -> GameData -> String -> Maybe Posix -> List (Html Msg)
viewSessionError session gd message mtime =
    let
        lang =
            T.gameDataToLang session gd

        query =
            session |> Session.query

        time =
            Maybe.map2 (\now at -> T.ViewErrorTime { now = now, at = at })
                (Session.now session)
                mtime
                |> Maybe.withDefault T.ViewErrorTimeUnknown

        moreErrors =
            (Session.errors session |> List.length) - 1
    in
    -- https://getbootstrap.com/docs/4.2/components/modal/
    [ div [ class "error-container alert alert-danger", attribute "role" "alert" ]
        [ div [ class "container" ]
            [ button [ class "close", attribute "aria-label" "close", onClick IgnoreSessionError ]
                [ span [ attribute "aria-hidden" "true" ] [ text "×" ] ]
            , h1 [ class "alert-heading" ] [ lang.html T.ViewErrorTitle ]
            , p [] [ lang.html time ]
            , p [] <|
                if moreErrors <= 0 then
                    []

                else
                    [ lang.html <| T.ViewErrorMore moreErrors ]
            , pre [] [ text message ]
            , button [ class "btn btn-danger", attribute "aria-label" "close", onClick IgnoreSessionError ] [ lang.html T.ViewErrorCloseButton ]
            ]
        ]
    ]


errorReportUrl : Session -> String -> String
errorReportUrl session message =
    "https://docs.google.com/forms/d/e/1FAIpQLSfffiBB3W9PIoVW-dV1v-2LggyjL4Iws5xezM_B3pw4ci4EJw/viewform?usp=pp_url&entry.318271744="
        ++ message
        ++ "&entry.1808064759="
        ++ (Session.persistExport session |> Maybe.withDefault "(empty)")


viewGameError : Session -> GameData -> String -> List (Html Msg)
viewGameError session gd message =
    let
        lang =
            T.gameDataToLang session gd

        query =
            session |> Session.query

        persistExport =
            case Session.persistExport session of
                Nothing ->
                    "(unknown)"

                Just str ->
                    str
    in
    -- https://getbootstrap.com/docs/4.2/components/modal/
    [ div [ class "error-container alert alert-danger", attribute "role" "alert" ]
        [ div [ class "container" ]
            [ h1 [ class "alert-heading" ] [ lang.html T.ViewErrorTitle ]
            , p [] [ lang.html T.ViewErrorGameLoad ]
            , p [] [ lang.html T.ViewErrorGameExport, input [ readonly True, value persistExport ] [] ]
            , p []
                [ lang.html T.ViewErrorGameBug
                , a [ class "btn btn-info", target "_blank", href (errorReportUrl session message) ] [ lang.html T.ViewErrorGameReportButton ]
                ]
            , pre [] [ text message ]
            , button [ class "btn btn-danger", attribute "aria-label" "close", onClick ResetSavedGame ] [ lang.html T.ViewErrorGameResetButton ]
            ]
        ]
    ]
