module View.Description exposing (view)

{-| Any special-case unit/spell description where a line in the spreadsheet's lang tab is insufficient.

Descriptions where we need custom html, heavy branching, etc.

These descriptions are one of very few places where Elm code is coupled to units
named in the spreadsheet. We try very hard to avoid this, but doing it here is
better than dramatically expanding the template language for the spreadsheets.

-}

import Game exposing (Game)
import GameData.Item as Item exposing (Item)
import Html as H exposing (..)
import Session exposing (Session)
import View.Description.CloneLarvae
import View.Description.Cocoon
import View.Description.Energy
import View.Description.Hatchery
import View.Description.MutantMeat
import View.Description.Territory


view : Session -> Game -> Item -> Maybe (Html msg)
view session game item =
    case ( Item.name item, item ) of
        ( "mutantmeat", _ ) ->
            Just <| View.Description.MutantMeat.view session game

        ( "energy", Item.Unit u ) ->
            Just <| View.Description.Energy.view session game u

        ( "territory", Item.Unit u ) ->
            Just <| View.Description.Territory.view session game u

        ( "hatchery", Item.Unit u ) ->
            Just <| View.Description.Hatchery.view session game u

        ( "expansion", Item.Upgrade u ) ->
            Just <| View.Description.Hatchery.viewExpansion session game u

        ( "cocoon", Item.Unit u ) ->
            Just <| View.Description.Cocoon.view session game u

        ( "clonelarvae", Item.Spell s ) ->
            Just <| View.Description.CloneLarvae.view session game s

        _ ->
            Nothing
