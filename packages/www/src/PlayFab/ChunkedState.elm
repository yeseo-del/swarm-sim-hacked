module PlayFab.ChunkedState exposing (decoder, encoder, encoderNextKey)

{-| Decode some chunked json, like large saved game states.

for `chunkedStateDecoder "state" ["Value"]`:

  - {state: {Value: ...}} is required.
  - {state1: {Value: ...}} is optional. Same for state2, ...
  - All chunks are concatenated.

-}

import Json.Decode as D
import Json.Encode as E
import String.Extra


type alias EncoderConfig =
    { prefix : String, size : Int }


encoder : EncoderConfig -> String -> E.Value
encoder { prefix, size } =
    String.Extra.break size
        >> List.indexedMap (\i chunk -> ( prefix ++ encoderSuffix i, E.string chunk ))
        >> E.object


encoderSuffix : Int -> String
encoderSuffix i =
    if i == 0 then
        ""

    else
        String.fromInt i


{-| Get the name of the first key that `encoder` _won't_ encode.

PlayFab.updateUserData should delete this key, so we don't accidentally decode it if the saved state shrinks.

-}
encoderNextKey_ : EncoderConfig -> String -> String
encoderNextKey_ { prefix, size } value =
    let
        numChunks =
            toFloat (String.length value)
                / toFloat size
                |> ceiling
                |> max 1
    in
    prefix ++ String.fromInt numChunks


encoderNextKey : EncoderConfig -> String -> ( E.Value, String )
encoderNextKey config value =
    ( encoder config value, encoderNextKey_ config value )


decoder : String -> List String -> D.Decoder String
decoder prefix nested =
    let
        loop1 : Int -> D.Decoder String
        loop1 i =
            D.at ((prefix ++ String.fromInt i) :: nested) D.string

        loop : Int -> List String -> D.Decoder (List String)
        loop i chunks =
            D.oneOf
                [ loop1 i |> D.andThen (\chunk -> D.lazy (\_ -> loop (i + 1) (chunk :: chunks)))
                , D.succeed chunks
                ]
    in
    D.at (prefix :: nested) D.string
        |> D.andThen (\s -> loop 1 [ s ])
        |> D.map (List.reverse >> String.join "")
