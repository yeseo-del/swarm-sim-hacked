module PlayFab exposing
    ( User
    , clearRememberId
    , emailLogin
    , emailRegister
    , errorField
    , inventory
    , kongregateLogin
    , paypalNotify
    , refreshAuth
    , rememberIdLogin
    , resetPassword
    , setRememberId
    , userFromTicket
    , writeState
    )

import Dict exposing (Dict)
import Http
import Json.Decode as D
import Json.Encode as E
import PlayFab.ChunkedState as ChunkedState
import PlayFab.Error as Error exposing (Error(..), FormErrorData)
import PlayFab.Inventory as Inventory exposing (Inventory)
import PlayFab.TitleId as TitleId exposing (TitleId)
import PlayFab.User as User
import RemoteData exposing (RemoteData)
import Session exposing (Session)
import Session.Iframe as Iframe
import Session.Iframe.Kongregate as Kongregate


{-| `PlayFab.User` is a convenient type alias
-}
type alias User =
    User.User


userDecoderConfig =
    User.decoderConfig


emailLogin : TitleId -> { req | email : String, password : String } -> (Result Error User -> msg) -> Cmd msg
emailLogin id req toMsg =
    -- https://api.playfab.com/documentation/client/method/LoginWithEmailAddress
    Http.post
        { url = TitleId.host id ++ "/Client/LoginWithEmailAddress"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", id |> TitleId.encode )
                    , ( "Email", req.email |> E.string )
                    , ( "Password", req.password |> E.string )
                    , ( "InfoRequestParameters", infoRequestParameters )
                    ]
        , expect =
            expectJson toMsg (User.decoder userDecoderConfig)
        }


rememberIdLogin : TitleId -> User.RememberId -> (Result Error User -> msg) -> Cmd msg
rememberIdLogin id rememberId toMsg =
    -- https://api.playfab.com/documentation/client/method/LoginWithCustomID
    Http.post
        { url = TitleId.host id ++ "/Client/LoginWithCustomId"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", id |> TitleId.encode )
                    , ( "CustomId", rememberId |> User.rememberIdToFullString |> E.string )
                    , ( "InfoRequestParameters", infoRequestParameters )
                    , ( "CreateAccount", False |> E.bool )
                    ]
        , expect =
            expectJson toMsg (User.decoder userDecoderConfig)
        }


emailRegister : TitleId -> { req | email : String, password : String } -> (Result Error User -> msg) -> Cmd msg
emailRegister id req toMsg =
    -- https://api.playfab.com/documentation/client/method/RegisterPlayFabUser
    Http.post
        { url = TitleId.host id ++ "/Client/RegisterPlayFabUser"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", id |> TitleId.encode )
                    , ( "RequireBothUsernameAndEmail", False |> E.bool )
                    , ( "Email", req.email |> E.string )
                    , ( "Password", req.password |> E.string )
                    , ( "InfoRequestParameters", infoRequestParameters )
                    ]
        , expect = expectJson toMsg (User.decoder { userDecoderConfig | email = Just req.email })
        }


kongregateLogin : TitleId -> Kongregate.UserId -> Kongregate.GameAuthToken -> (Result Error User -> msg) -> Cmd msg
kongregateLogin id kongId kongAuthToken toMsg =
    -- https://api.playfab.com/documentation/client/method/LoginWithKongregate
    Http.post
        { url = TitleId.host id ++ "/Client/LoginWithKongregate"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", id |> TitleId.encode )
                    , ( "KongregateId", kongId |> Kongregate.userIdToString |> E.string )
                    , ( "AuthTicket", kongAuthToken |> Kongregate.gameAuthTokenToFullString |> E.string )
                    , ( "CreateAccount", True |> E.bool )
                    , ( "InfoRequestParameters", infoRequestParameters )
                    ]
        , expect =
            expectJson toMsg (User.decoder userDecoderConfig)
        }


userFromTicket : TitleId -> User.SessionTicket -> (Result Error User -> msg) -> Cmd msg
userFromTicket id ticket toMsg =
    -- https://api.playfab.com/documentation/client/method/GetAccountInfo
    -- https://api.playfab.com/documentation/client/method/GetPlayerCombinedInfo
    Http.request
        { method = "POST"
        , url = TitleId.host id ++ "/Client/GetPlayerCombinedInfo"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "InfoRequestParameters", infoRequestParameters )
                    ]
        , expect = expectJson toMsg (User.decoder { userDecoderConfig | ticket = Just ticket })
        , headers =
            [ Http.header "X-Authentication" (ticket |> User.ticketToString)
            ]
        , timeout = Nothing
        , tracker = Nothing
        }


infoRequestParameters =
    E.object
        [ ( "GetUserData", True |> E.bool )
        , ( "GetUserReadOnlyData", True |> E.bool )
        , ( "GetUserAccountInfo", True |> E.bool )
        ]


resetPassword : TitleId -> { req | email : String } -> (Result Error () -> msg) -> Cmd msg
resetPassword id req toMsg =
    -- https://api.playfab.com/documentation/client/method/SendAccountRecoveryEmail
    Http.post
        { url = TitleId.host id ++ "/Client/SendAccountRecoveryEmail"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", id |> TitleId.encode )
                    , ( "Email", req.email |> E.string )
                    ]
        , expect =
            expectJson toMsg (D.succeed ())
        }


setRememberId : TitleId -> User -> (Result Error User -> msg) -> Cmd msg
setRememberId id user toMsg =
    case User.rememberId user of
        Just rid ->
            Cmd.none

        Nothing ->
            -- https://api.playfab.com/documentation/client/method/LinkCustomID
            let
                rid : User.RememberId
                rid =
                    User.generateRememberId user
            in
            Http.request
                { method = "POST"
                , url = TitleId.host id ++ "/Client/LinkCustomId"
                , body =
                    Http.jsonBody <|
                        E.object
                            [ ( "TitleId", id |> TitleId.encode )
                            , ( "CustomId", rid |> User.rememberIdToFullString |> E.string )
                            ]
                , expect =
                    expectJson toMsg (user |> User.setRememberId (Just rid) |> D.succeed)
                , headers =
                    [ Http.header "X-Authentication" (user |> User.sessionTicket |> User.ticketToString)
                    ]
                , timeout = Nothing
                , tracker = Nothing
                }


clearRememberId : TitleId -> User -> (Result Error User -> msg) -> Cmd msg
clearRememberId id user toMsg =
    case User.rememberId user of
        Nothing ->
            Cmd.none

        Just rid ->
            Http.request
                { method = "POST"
                , url = TitleId.host id ++ "/Client/UnlinkCustomId"
                , body =
                    Http.jsonBody <|
                        E.object
                            [ ( "TitleId", id |> TitleId.encode )
                            , ( "CustomId", rid |> User.rememberIdToFullString |> E.string )
                            ]
                , expect =
                    expectJson toMsg (user |> User.setRememberId Nothing |> D.succeed)
                , headers =
                    [ Http.header "X-Authentication" (user |> User.sessionTicket |> User.ticketToString)
                    ]
                , timeout = Nothing
                , tracker = Nothing
                }


inventory : TitleId -> User -> (Result Error Inventory -> msg) -> Cmd msg
inventory titleId user toMsg =
    Http.request
        { method = "POST"
        , url = TitleId.host titleId ++ "/Client/GetUserInventory"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", titleId |> TitleId.encode )
                    ]
        , expect =
            expectJson toMsg (D.at [ "data", "Inventory" ] <| D.list Inventory.decoder)
        , headers =
            [ Http.header "X-Authentication" (user |> User.sessionTicket |> User.ticketToString)
            ]
        , timeout = Nothing
        , tracker = Nothing
        }


{-| Notify Playfab for Paypal PDT payments.

Paypal IPN also does this, but PDT does it when the user returns from the paypal
page, immediately (but only once).

<https://github.com/swarmsim/swarm-playfab-cloudscript/blob/master/main.js>

-}
paypalNotify : TitleId -> User -> String -> (Result Error String -> msg) -> Cmd msg
paypalNotify titleId user paypalTxnId toMsg =
    Http.request
        { method = "POST"
        , url = TitleId.host titleId ++ "/Client/ExecuteCloudScript"
        , body =
            Http.jsonBody <|
                E.object
                    [ ( "TitleId", titleId |> TitleId.encode )
                    , ( "FunctionName", "paypalNotify" |> E.string )
                    , ( "FunctionParameter", [ ( "tx", paypalTxnId |> E.string ) ] |> E.object )
                    ]
        , expect =
            expectJson toMsg
                (D.at [ "data", "FunctionResult" ]
                    (D.map2 Tuple.pair
                        (D.field "state" D.string)
                        (D.field "itemInstanceId" D.string)
                    )
                    |> D.andThen
                        (\( state, itemInstanceId ) ->
                            if state == "success" then
                                D.succeed itemInstanceId

                            else
                                D.fail <| "paypalNotify non-success state: " ++ state
                        )
                )
        , headers =
            [ Http.header "X-Authentication" (user |> User.sessionTicket |> User.ticketToString)
            ]
        , timeout = Nothing
        , tracker = Nothing
        }


{-| If we have a rememberId or kongregate user, log in again.

The playfab session expires every 24 hours. For long-running sessions, we'll
need to refresh it periodically so the user isn't logged out, when possible.

-}
refreshAuth : Session -> (Result Error User -> msg) -> Cmd msg
refreshAuth session toMsg =
    let
        titleId =
            TitleId.fromSession session
    in
    case Session.iframe session of
        Iframe.WWW ->
            case Session.user session of
                RemoteData.Success (Just user) ->
                    case User.rememberId user of
                        Just rid ->
                            rememberIdLogin titleId rid toMsg

                        _ ->
                            Cmd.none

                _ ->
                    Cmd.none

        Iframe.Kongregate kong ->
            case kong.user of
                Nothing ->
                    Cmd.none

                Just user ->
                    if user.isGuest then
                        Cmd.none

                    else
                        kongregateLogin titleId user.userId user.gameAuthToken toMsg


expectJson : (Result Error ok -> msg) -> D.Decoder ok -> Http.Expect msg
expectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \res ->
            case res of
                Http.BadUrl_ url ->
                    Err <| UnexpectedError <| "bad url: " ++ url

                Http.Timeout_ ->
                    Err <| TransientError "timeout"

                Http.NetworkError_ ->
                    Err <| TransientError "network error"

                Http.BadStatus_ metadata body ->
                    Err <|
                        case metadata.statusCode of
                            400 ->
                                parseFormError body

                            401 ->
                                AuthError metadata.statusCode body

                            403 ->
                                parseFormError body

                            _ ->
                                UnexpectedError <| "bad status: " ++ String.fromInt metadata.statusCode ++ "\n\n" ++ body

                Http.GoodStatus_ metadata body ->
                    body
                        -- |> Debug.log "playfab-http"
                        |> D.decodeString decoder
                        |> Result.mapError (\err -> UnexpectedError <| "body decode error: " ++ D.errorToString err)


parseFormError : String -> Error
parseFormError body =
    case D.decodeString formErrorFromJson body of
        Ok details ->
            FormError details

        Err err ->
            UnexpectedError <| D.errorToString err


formErrorFromJson : D.Decoder FormErrorData
formErrorFromJson =
    D.map2 FormErrorData
        (D.field "errorMessage" D.string)
        (D.list D.string
            |> D.dict
            |> D.field "errorDetails"
            |> D.maybe
            |> D.map (Maybe.withDefault Dict.empty)
        )


errorField : Maybe Error -> String -> List String
errorField error field =
    case error of
        Just (FormError formError) ->
            case Dict.get field formError.fields of
                Just errs ->
                    errs

                _ ->
                    []

        _ ->
            []


writeState : Session -> Maybe String -> (Result Error () -> msg) -> Cmd msg
writeState session =
    case Session.user session of
        RemoteData.Success (Just user) ->
            writeState_ (TitleId.fromSession session) (User.sessionTicket user)

        _ ->
            \_ _ -> Cmd.none


writeState_ : TitleId -> User.SessionTicket -> Maybe String -> (Result Error () -> msg) -> Cmd msg
writeState_ id ticket mstate toMsg =
    -- https://api.playfab.com/documentation/Client/method/UpdateUserData
    Http.request
        { method = "POST"
        , url = TitleId.host id ++ "/Client/UpdateUserData"
        , body =
            Http.jsonBody <|
                E.object <|
                    case mstate of
                        Just state ->
                            let
                                -- playfab's max key size is 10k, so split into multiple keys
                                ( data, nextKey ) =
                                    ChunkedState.encoderNextKey { prefix = User.stateKey, size = 10000 } state
                            in
                            [ ( "Data", data )
                            , ( "KeysToRemove", E.list E.string [ nextKey ] )
                            ]

                        Nothing ->
                            [ ( "KeysToRemove"
                              , E.list E.string [ User.stateKey ]
                              )
                            ]
        , expect = expectJson toMsg (D.succeed ())
        , headers =
            [ Http.header "X-Authentication" (ticket |> User.ticketToString)
            ]
        , timeout = Nothing
        , tracker = Nothing
        }
