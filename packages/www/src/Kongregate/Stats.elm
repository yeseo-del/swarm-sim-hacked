module Kongregate.Stats exposing (Stats, collect, encoder)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.Statistics as Statistics
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Json.Encode as E


type alias Stats =
    { hatcheries : Int
    , expansions : Int
    , gameComplete : Int
    , mutationsUnlocked : Int
    , achievementPoints : Int
    , minutesToFirstNexus : Maybe Int
    , minutesToFifthNexus : Maybe Int
    , minutesToFirstAscension : Maybe Int
    }


collect : Game -> UnitsSnapshot -> Stats
collect game units =
    -- Debug.log "stats.collect" <|
    { hatcheries = UnitsSnapshot.countName "hatchery" units |> Decimal.toFloat |> round
    , expansions = Game.upgradeCountName "expansion" game |> Decimal.toFloat |> round
    , gameComplete = UnitsSnapshot.countName "ascension" units |> Decimal.toFloat |> round
    , mutationsUnlocked =
        List.map (\n -> Game.upgradeCountName n game |> Decimal.toFloat |> round |> min 1)
            [ "mutatehatchery"
            , "mutatebat"
            , "mutateclone"
            , "mutateswarmwarp"
            , "mutaterush"
            , "mutateeach"
            , "mutatefreq"
            , "mutatenexus"
            , "mutatearmy"
            , "mutatemeat"
            ]
            |> List.sum
    , achievementPoints = (Game.achievementPoints game).val
    , minutesToFirstNexus = elapsedFirstMinutes "nexus1" (Game.statistics game).byUpgrade
    , minutesToFifthNexus = elapsedFirstMinutes "nexus5" (Game.statistics game).byUpgrade
    , minutesToFirstAscension = elapsedFirstMinutes "ascension" (Game.statistics game).byUnit
    }


elapsedFirstMinutes : String -> Dict String Statistics.Entry -> Maybe Int
elapsedFirstMinutes name dict =
    Dict.get name dict |> Maybe.map (.elapsedFirst >> Duration.toMinutes >> ceiling)


encoder : Stats -> E.Value
encoder s =
    E.object <|
        [ ( "Hatcheries", s.hatcheries |> E.int )
        , ( "Expansions", s.expansions |> E.int )
        , ( "GameComplete", s.gameComplete |> E.int )
        , ( "Mutations Unlocked", s.mutationsUnlocked |> E.int )
        , ( "Achievement Points", s.achievementPoints |> E.int )
        ]
            ++ List.filterMap (\( key, val ) -> val |> Maybe.map (\v -> ( key, E.int v )))
                [ ( "Minutes to First Nexus", s.minutesToFirstNexus )
                , ( "Minutes to Fifth Nexus", s.minutesToFifthNexus )
                , ( "Minutes to First Ascension", s.minutesToFirstAscension )
                ]
