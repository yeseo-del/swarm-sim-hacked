module Kongregate.Inventory exposing (Id, Inventory, Item, decoder, idToString, itemDecoder)

import GameData.Mtx.Id as MtxId
import Json.Decode as D


type Id
    = Id Int


idToString : Id -> String
idToString (Id id) =
    String.fromInt id


type alias Item =
    -- Item instance id; unique across kongregate
    { id : Id

    -- kongregate item.identifier. mtx id that they bought, ex. "crystal000"
    , identifier : MtxId.Id

    -- Display name
    , name : String
    }


itemDecoder : D.Decoder Item
itemDecoder =
    D.map3 Item
        (D.field "id" <| D.map Id D.int)
        (D.field "identifier" MtxId.decoder)
        (D.field "name" D.string)


type alias Inventory =
    List Item


decoder : D.Decoder Inventory
decoder =
    D.list itemDecoder
        -- Kongregate items, sadly, do not have an order date/creation date.
        -- Ids appear to be a serial int, and the list is sorted oldest-first.
        -- I'd rather see newest-first, and newest-first matches playfab.
        |> D.map List.reverse
