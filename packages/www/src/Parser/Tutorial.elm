module Parser.Tutorial exposing (Expr, eval, evalSpan, parse)

{-| A slightly-customized almost-subset of markdown used in tutorial text.

The elm/markdown module, unfortunately, has some problems for my case:

  - markdown links do a full reload of the page
  - can't embed custom html (like the upgrade icon).
    `sanitize=False` to embed raw HTML works, but makes links unclickable.
    (It rebuilds the markup every time the model changes - up to 60fps - which
    breaks clicks.)

-}

import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import List.Extra
import Parser exposing ((|.), (|=), Parser)
import Result.Extra
import View.Icon


type alias Expr =
    List LineExpr


type alias LineExpr =
    List Elem


type Elem
    = Text String
    | Link { href : String, body : LineExpr }
    | Icon { alt : String, name : String }
    | Bold LineExpr


parse : String -> Result String Expr
parse =
    Parser.run parser
        -- >> Result.mapError Debug.toString
        >> Result.mapError Parser.deadEndsToString


parser : Parser Expr
parser =
    Parser.succeed identity
        |= lines
        |. Parser.end


lines : Parser Expr
lines =
    Parser.sequence
        { start = ""
        , end = ""
        , separator = "\n"
        , spaces = Parser.succeed ()
        , item = line
        , trailing = Parser.Optional
        }
        |> Parser.andThen
            (\es ->
                if es == [] then
                    Parser.problem "empty tutorial body"

                else
                    Parser.succeed es
            )


line : Parser LineExpr
line =
    expression topElement


expression : Parser Elem -> Parser LineExpr
expression element =
    element
        |> Parser.andThen
            (\first ->
                Parser.loop [ first ] <|
                    \revElements ->
                        Parser.oneOf
                            [ Parser.succeed (\elem -> Parser.Loop <| elem :: revElements)
                                |= element
                            , Parser.succeed ()
                                |> Parser.map (\_ -> Parser.Done <| List.reverse revElements)
                            ]
            )


topElement : Parser Elem
topElement =
    Parser.oneOf
        [ Parser.succeed Bold
            |. Parser.symbol "*"
            |= Parser.lazy (\_ -> expression nestedElement)
            |. Parser.symbol "*"
        , Parser.succeed (\body href -> Link { href = href, body = body })
            |. Parser.symbol "["
            |= Parser.lazy (\_ -> expression nestedElement)
            -- |= (Parser.chompWhile (\c -> c /= ' ' && c /= ']') |> Parser.getChompedString |> Parser.map (Text >> List.singleton))
            |. Parser.symbol "]"
            |. Parser.symbol "("
            |. Parser.spaces
            |= (Parser.chompWhile (\c -> c /= ')') |> Parser.getChompedString)
            |. Parser.spaces
            |. Parser.symbol ")"
        , iconElement
        , textElement
        ]


nestedElement : Parser Elem
nestedElement =
    Parser.oneOf
        [ iconElement
        , textElement
        ]


iconElement : Parser Elem
iconElement =
    Parser.symbol "!"
        |> Parser.andThen
            (\_ ->
                Parser.oneOf
                    [ Parser.succeed (\alt name -> Icon { alt = alt, name = name })
                        |. Parser.symbol "["
                        |. Parser.spaces
                        |= (Parser.chompWhile (\c -> c /= ' ' && c /= ']') |> Parser.getChompedString)
                        |. Parser.spaces
                        |. Parser.symbol "]"
                        |. Parser.symbol "["
                        |. Parser.spaces
                        |= (Parser.chompWhile (\c -> c /= ' ' && c /= ']') |> Parser.getChompedString)
                        |. Parser.spaces
                        |. Parser.symbol "]"
                    , Parser.succeed (Text "!")
                    ]
            )


textElement : Parser Elem
textElement =
    Parser.succeed identity
        |= (Parser.chompWhile (\c -> c /= '\n' && c /= '*' && c /= '[' && c /= ']' && c /= '!') |> Parser.getChompedString)
        |> Parser.andThen
            (\t ->
                if t == "" then
                    Parser.problem "empty tutorial body"

                else
                    Parser.succeed <| Text t
            )


eval : Expr -> List (Html msg)
eval =
    List.map (evalLine >> p [])


evalSpan : Expr -> List (Html msg)
evalSpan =
    List.map (evalLine >> span [])


evalLine : LineExpr -> List (Html msg)
evalLine =
    List.map evalElem


evalElem : Elem -> Html msg
evalElem elem =
    case elem of
        Text str ->
            text str

        Link { href, body } ->
            a [ A.href href ] (evalLine body)

        Icon { alt, name } ->
            if String.startsWith "/" name then
                img [ class "img-icon", src name ] []

            else
                View.Icon.fas name

        Bold body ->
            b [] (evalLine body)
