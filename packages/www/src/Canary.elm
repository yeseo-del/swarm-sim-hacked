module Canary exposing (Canary, Persist, Roll, create, decoder, encoder, isActive)

import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import Random exposing (Generator)



-- guarantees I'd like to see:
-- 1) 0-1 canaries per feature; 1 feature per canary
-- 2) guarantee all canaries are rolled after serialization
--
-- canary enum/list: (1); not (2)
-- canary records: (2); sorta (1) (type system can't guarantee it, but logic can)


type Canary
    = Canary Float


create : Float -> Canary
create =
    Canary


isActive : Canary -> Roll -> Bool
isActive (Canary threshold) roll =
    roll >= 1 - threshold


type alias Roll =
    Float


type alias Persist =
    { -- Add entries for each canary!
      mtx : Roll

    -- this one goes last
    , seed : Random.Seed
    }


createPersist : Int -> Persist
createPersist seed0 =
    persistFromDict (Random.initialSeed seed0) Dict.empty


persistFromDict : Random.Seed -> Dict String Roll -> Persist
persistFromDict seed dict =
    let
        ( persist, seed1 ) =
            Random.step (genPersist dict) seed
    in
    persist seed1


{-| Look up an existing canary roll, or generate a new one
-}
genRoll : Dict String Float -> String -> Generator Roll
genRoll dict name =
    Random.float 0 1 |> Random.map (\roll -> Dict.get name dict |> Maybe.withDefault roll)


genPersist : Dict String Float -> Generator (Random.Seed -> Persist)
genPersist dict =
    Random.map Persist
        (genRoll dict "mtx")


encoder : Persist -> E.Value
encoder p =
    E.object
        [ ( "seed", p.seed |> Random.step (Random.int Random.minInt Random.maxInt) |> Tuple.first |> E.int )
        , ( "mtx", p.mtx |> E.float )
        ]


decoder : D.Decoder Persist
decoder =
    D.map2 persistFromDict
        (D.field "seed" (D.int |> D.map Random.initialSeed))
        (D.dict D.float)
