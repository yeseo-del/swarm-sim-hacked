// quick one-time script to map from spreadsheet json to human-writable json
const fs = require('fs').promises

fs
.readFile('./en-us.old.json')
.then(JSON.parse)
.then(i => {
  let o = {}
  for (let entry of Object.values(i['lang.en_us'])) {
    o[entry.key] = entry.message
  }
  return o
})
.then(json => JSON.stringify(json, null, 2))
// .then(console.log)
.then(txt => fs.writeFile('./en-us.json', txt))
