module NumberSuffix2.Config exposing
    ( Config
    , scientificConfig
    , standardConfig
    , suffixAlphabetic
    , suffixEngineering
    , suffixLongScale
    , suffixLongScaleShort
    , suffixScientific
    , suffixStandard
    , suffixStandardShort
    )

import Array exposing (Array)
import NumberSuffix2.Locale as Locale exposing (Locale)
import NumberSuffixData


{-| Configure how numbers are formatted.

`getSuffix` returns a suffix, given a digit count for the number. See the [suffix functions](#suffixStandard) below.

`sigfigs` is the number of significant figures shown.

Below `minSuffix`, a comma-separated number is shown instead of a suffixed number.

-}
type alias Config =
    { locale : Locale
    , getSuffix : Int -> String
    , suffixDivisor : Int
    , sigfigs : Int
    , minSuffixExp : Int
    , stringFromFloat : Int -> Float -> String
    }


{-| Default formatting configuration.

By default, we use standard suffixes, US locale, 3 significant figures,
thousands grouping (suffixDivisor=3), and show no suffixes for values below 100,000.

-}
standardConfig : Config
standardConfig =
    { locale = Locale.usLocale
    , getSuffix = suffixStandard
    , suffixDivisor = 3
    , sigfigs = 3
    , minSuffixExp = 5
    , stringFromFloat = always String.fromFloat
    }


{-| Scientific notation formatting configuration.

    import NumberSuffix2 exposing (format)

    config : Config
    config = scientificConfig

    format config 1.0e3 --> "1,000"
    format config 1.0e6 --> "1.00e6"
    format config 1.0e7 --> "1.00e7"
    format config 1.0e8 --> "1.00e8"
    format config 1.0e9 --> "1.00e9"

-}
scientificConfig : Config
scientificConfig =
    { locale = Locale.usLocale
    , getSuffix = suffixScientific
    , suffixDivisor = 1
    , sigfigs = 3
    , minSuffixExp = 5
    , stringFromFloat = always String.fromFloat
    }



-- SUFFIXES


getListSuffix : Array String -> Int -> String
getListSuffix suffixes digits =
    case Array.get (digits // 3) suffixes of
        Just s ->
            s

        Nothing ->
            suffixEngineering digits


{-| Standard suffixes; the default.

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixStandard }
    -- `config = standardConfig` would work too; this is the default

    format config 1e3 --> "1,000"
    format config 1e5 --> "100 thousand"
    format config 1e6 --> "1.00 million"
    format config 1e9 --> "1.00 billion"
    format config 1e12 --> "1.00 trillion"
    format config 1e15 --> "1.00 quadrillion"

-}
suffixStandard : Int -> String
suffixStandard =
    getListSuffix NumberSuffixData.standard


{-| Abbreviated standard suffixes.

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixStandardShort }

    format config 1e3 --> "1,000"
    format config 1e5 --> "100K"
    format config 1e6 --> "1.00M"
    format config 1e9 --> "1.00B"
    format config 1e12 --> "1.00T"
    format config 1e15 --> "1.00Qa"

-}
suffixStandardShort : Int -> String
suffixStandardShort =
    getListSuffix NumberSuffixData.standardShort


{-| Long-scale suffixes.

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixLongScale }

    format config 1e3 --> "1,000"
    format config 1e5 --> "100 thousand"
    format config 1e6 --> "1.00 million"
    format config 1e9 --> "1.00 milliard"
    format config 1e12 --> "1.00 billion"
    format config 1e15 --> "1.00 billiard"

-}
suffixLongScale : Int -> String
suffixLongScale =
    getListSuffix NumberSuffixData.longScale


{-| Abbreviated long-scale suffixes. (Sorry about the odd name.)

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixLongScaleShort }

    format config 1e3 --> "1,000"
    format config 1e5 --> "100K"
    format config 1e6 --> "1.00M"
    format config 1e9 --> "1.00Md"
    format config 1e12 --> "1.00B"
    format config 1e15 --> "1.00Bd"

-}
suffixLongScaleShort : Int -> String
suffixLongScaleShort =
    getListSuffix NumberSuffixData.longScaleShort


suffixScientific : Int -> String
suffixScientific digits =
    if digits <= 3 then
        ""

    else
        "e" ++ String.fromInt digits


{-| Engineering notation.

Unlike scientific notation, engineering notation numbers are always divisible by 3.

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixEngineering }

    format config 1e3 --> "1,000"
    format config 1e5 --> "100E3"
    format config 1e6 --> "1.00E6"
    format config 1e7 --> "10.0E6"
    format config 1e8 --> "100E6"
    format config 1e9 --> "1.00E9"

-}
suffixEngineering : Int -> String
suffixEngineering digits =
    if digits <= 3 then
        ""

    else
        "E" ++ String.fromInt (digits // 3 * 3)


{-| Alphabetic suffixes.

    import NumberSuffix2 exposing (format)

    config : Config
    config = { standardConfig | getSuffix = suffixAlphabetic }

    format config 1e3 --> "1,000"
    format config 1e5 --> "100K"
    format config 1e6 --> "1.00M"
    format config 1e9 --> "1.00B"
    format config 1e12 --> "1.00T"
    format config 1e15 --> "1.00aa"
    format config 1e18 --> "1.00ab"

-}
suffixAlphabetic : Int -> String
suffixAlphabetic =
    getListSuffix NumberSuffixData.alphabetic
