module NumberSuffix2.Parsed exposing (Parsed, parseFloat, parseInt, parseSigExp, parseString)

import NumberSuffix2.Config as Config exposing (Config)
import NumberSuffix2.Locale as Locale exposing (Locale)


type alias Parsed =
    { digits : String
    , exp : Int
    , neg : Bool
    }


error : Parsed
error =
    { digits = ""
    , exp = 0
    , neg = False
    }


parseString : Locale -> String -> Maybe Parsed
parseString loc s =
    Maybe.map normalize <|
        case String.split "e" s of
            [ sig ] ->
                parseSignificand loc sig

            [ sig, exp ] ->
                Maybe.map2 (\p e -> { p | exp = p.exp + e })
                    (parseSignificand loc sig)
                    (String.toInt exp)

            _ ->
                Nothing


normalize : Parsed -> Parsed
normalize p =
    let
        digits =
            p.digits |> trimZeros
    in
    { p
        | digits = digits
        , neg =
            if digits == "0" then
                False

            else
                p.neg
    }


trimLeadingZeros : String -> String
trimLeadingZeros s =
    case String.uncons s of
        Just ( '0', "" ) ->
            -- don't trim the last zero
            s

        Just ( '0', digits ) ->
            trimLeadingZeros digits

        _ ->
            s


{-| Trim leading and trailing zeros
-}
trimZeros : String -> String
trimZeros =
    trimLeadingZeros >> String.reverse >> trimLeadingZeros >> String.reverse


{-| Trim leading negative/positive signs.
-}
trimSign : Locale -> String -> String
trimSign loc s =
    if String.startsWith loc.negativePrefix s && String.endsWith loc.negativeSuffix s then
        s |> String.dropLeft (String.length loc.negativePrefix) |> String.dropRight (String.length loc.negativeSuffix)

    else if String.startsWith loc.positivePrefix s && String.endsWith loc.positiveSuffix s then
        s |> String.dropLeft (String.length loc.positivePrefix) |> String.dropRight (String.length loc.positiveSuffix)

    else
        s


parseSignificand : Locale -> String -> Maybe Parsed
parseSignificand loc s =
    Maybe.map
        (\exp ->
            { digits = s |> trimSign loc |> String.filter Char.isDigit
            , neg = String.startsWith loc.negativePrefix s && String.endsWith loc.negativeSuffix s
            , exp = exp
            }
        )
        (case s |> trimSign loc |> String.split loc.decimalSeparator of
            [ int ] ->
                Just <| String.length (int |> trimLeadingZeros |> String.filter Char.isDigit) - 1

            [ int, _ ] ->
                Just <| String.length (int |> trimLeadingZeros |> String.filter Char.isDigit) - 1

            _ ->
                Nothing
        )


parseFloat : Config -> Float -> Parsed
parseFloat cfg =
    cfg.stringFromFloat cfg.sigfigs
        >> parseString cfg.locale
        -- our parser can parse any float - below should never happen
        >> Maybe.withDefault error


parseInt : Config -> Int -> Parsed
parseInt cfg =
    toFloat >> parseFloat cfg


parseSigExp : Config -> Float -> Int -> Parsed
parseSigExp cfg sig exp =
    let
        parsedSig =
            parseFloat cfg sig
    in
    { parsedSig | exp = parsedSig.exp + exp }
