Changelog
=========

---

#### 2019-08-18
- Added tools to help translate Swarm Simulator into other languages. [Can you help?](/translator)

#### 2019-08-14
- Fixed a bug that sometimes incorrectly displayed numbers like "1.0e6" as "0.1e7".

#### 2019-08-12
- Hatchable-rates now correctly account for twins.

#### 2019-08-08
- You can now toggle the selected unit's hatchable-rate ("You can hatch 420 drones per second" text, from the old "show advanced unit data").

#### 2019-08-07
- Converting crystals to energy now correctly increases your maximum energy.
  - ...except for crystals spent _in the beta_, _before_ this bugfix. Sorry.
  - Crystals spent in v1.1.x, and all crystals spent _after_ this bugfix, all correctly increase your maximum energy.
- Ported Clone Larvae's buy-max-cocoons links.
- When "speeds: per swarmwarp" is set, energy is now correctly displayed per-second instead.
- Upgrade-available icons no longer change the positioning of the name they're next to.

#### 2019-07-30
- Reworked much of the UI on small screens. Experimental; very open to feedback!
  - All tabs should now fit along the top of the screen.
  - When a unit is selected, units in the same tab are not shown. Instead, there's now a link to show that list/deselect the unit.
  - The "more..." menu is now a link on small screens, instead of a dropdown that falls off the side of the page.
  - [You can turn it off, for now](?mobileui=0). Please say something if you hate the new UI, because it will soon be permanent and this link will stop working!

#### 2019-07-29
- Fixed incorrect Swarmwarp time display.
- Implemented the no-unit-selected tab view. Buying from this screen isn't yet possible.

#### 2019-07-23
- Fixed a bug that sometimes caused your input field to lose focus when typing the number of things to buy.

#### 2019-07-22
- We're approaching the end of beta testing, so I'm actually taking the changelog seriously again.
- Progress bars below 1% are now rendered more accurately.
- The Swarmwarp description now shows its time using minutes/hours/days/years, not just seconds.
- Progress bars for spells work properly once again.

#### 2019-07-17
- Empowering territory units now correctly changes their name and order.
- Fixed most rounding errors with `@` input and links.

#### 2019-07-14
- Ported crystal purchases.
- Ported nonlinear progress estimates. Estimated times for meat units should be much more accurate now.

#### 2019-06-19
- Ported most keyboard shortcuts.
- Welcome-back is now shown as a modal.
- Several minor bug fixes.

#### 2019-06-15
- Online saves for elm.swarmsim.com/v1.2.0 now use a new save slot. They will no longer overwrite your www.swarmsim.com/v1.1.x online save.

#### 2019-06-12
- If you have no saved game on elm.swarmsim.com, we'll automatically read your save from www.swarmsim.com.
- Ported the welcome-back screen, saying what your swarm produced while you were away.
- Ported the new player tutorial.

#### 2019-05-27
- Implemented and redesigned online saved games.
  - Login/register/reset-password now have their own pages and menu entries. They're no longer on the options screen.
  - Your online save is automatically imported when the page is loaded.
    - If we're not sure whether to use your local save or online save, we'll ask you and show a comparison of the two.
    - Previously, you had to visit the options screen and manually import your online save. The intent is to make playing on multiple devices less painful.
- Redesigned progress bars and cost displays.
  - Progress bars can be expanded/collapsed while viewing any unit, upgrade, or spell
  - This will replace "advanced unit data" from the original Swarm Simulator.
- Added an official API for automating Swarm Simulator with Javascript code.
  - Programmers: open your browser's Javascript console and inspect `swarmApi` or `window.swarmApi`.
- Ported many other features from v1.1.11, many small miscellaneous changes, and many bug fixes over the last two months.
  - I haven't maintained this changelog very well. Sorry.

#### 2019-04-04
- Fixed many achievement thresholds that were unintentionally too low.
- Many kittens fixes.
- Many unit/upgrade description fixes. Everything but crystals should now work.

#### 2019-03-29
- Undo now works.
- When buying units, `@` and `=` syntax now works.
- Kittens now work[.](?fools=on)
- Filled in most unit/upgrade descriptions. None are blank/TODO, though some are still incomplete (crystals, hatcheries, expansions, territory).
- The achievements page once again shows the larva production bonus.
- Added a new experimental UI for showing costs:
  - The cost is shown as text by default.
  - When expanded, it shows a progress bar for each resource.
  - The progress bar either shows your progress toward saving for the specified purchase, or the percentage of your savings the purchase will consume.
  - Each progress bar includes an exact-cost link when appropriate.
  - This will replace the old "show advanced unit data".
  - Feedback is welcome!
- Selecting territory shows a list of percentages. Pie chart coming soon.

#### 2019-03-19
- Achievement dates are now imported correctly from v1.1.11.
- The last unit you've selected in each tab is now saved when you leave the page.
- Achievement requirements should be displayed correctly, for example "1 trillion" not "1,000 billion".
- Costs should now be displayed/paid without rounding errors.
- If your saved game fails to load, the error page now allows you to reset your save or report a bug.
- If the game fails to start for some other reason, the error page is prettier.
- Importing a bad saved game should now always show the error below the input field, and never crash the game.

#### 2019-03-11
- Thousands of production upgrades (and maybe other cases of large exponents) no longer break the game. This fixes certain v1.1.11 saves that broke when imported.
- Hatcheries are once again correctly imported from v1.1.11.

#### 2019-03-07: v1.2.0-beta
- Public beta announced.
- Adding more features from v1.1.11:
  - The selected unit is now highlighted in the sidebar.
  - Added outline to progress bar text, for better visibility.
  - A "close" button is now visible for units and spells.
  - Added a theme selector to the options screen.
  - Fixed certain legacy saves that failed to import.

#### 2019-03-01: v1.2.0-alpha
- Rewrote all of Swarm Simulator Classic's code. ([Why?](https://gitlab.com/erosson/swarm-elm#but-everyone-knows-the-big-rewrite-is-a-terrible-idea))
  - Your old saved games from before the rewrite/`1.1.x` should work perfectly. Saves may be deleted before beta testing is over.
  - Notable improvements:
    - The game performs much better, especially for players who've unlocked all units.
      - My development machine (a 2015 laptop) now comfortably sees 60fps at endgame, while it struggled to get 4fps with the old version.
      - Try adding [`?debug=1`](?debug=1) to the URL to see your frames per second. FPS-display will be in the options screen soon.
    - Longstanding Safari and iOS browser incompatibility is now gone.
    - Memory leaks in modern versions of Chrome and other browsers is now gone.
    - The options screen now allows you to choose a language. You can only choose English or some test languages right now, but the potential for useful translations is there!
    - The developer is happier, and has better defenses against writing new ~~bugs~~ defects
  - v1.2.0 is currently being tested on `elm.swarmsim.com`. Once testing's complete, it will replace `www.swarmsim.com`.
    - All gameplay features from the original are working now - units, upgrades, spells, ascensions. Your late-game saves should work!
    - Various non-gameplay features - UI/layout, settings, text, mobile layout, online saves, crystal purchases, Kongregate integration - may still be missing or broken. These will be addressed before the end of beta testing. (I've been focused on gameplay features until now!)
    - Expect at least a month of beta testing.
- Gameplay changes:
  - Mutagen respecs have been redesigned. Instead of the old "free"-respec system and energy costs, a respec now costs 5,000 crystals.
    - The old mutagen-respec system was needlessly complicated for new players, and predated crystals. Crystals are an opportunity to simplify the respec system.
  - Hatcheries, all energy abilities, and mutagen-respecs are no longer "upgrades". Each one has a row in the unit sidebar. Their effects have not changed.
  - Nothing else. Really. Any other gameplay-related differences (not interface/layout differences) may be bugs.
- The buy button and input-field have changed.
  - There's now a dropdown with a few common options, instead of 3 buttons; and a more-visible "formatting help" modal.
  - This is experimental - feedback is welcome!
- Redesigned this changelog. The changes for versions older than `1.2.x` will be archived elsewhere soon.
- The game's version numbers will look different in the future.
  - The game's release and update process has been redesigned (continuous deployment). Most version numbers will be generated automatically.
