[Swarm Simulator Classic](https://elm.swarmsim.com/)
====================================================

Starting with only a few larvae and a small pile of meat, build a merciless swarm of trillions of giant alien bugs.

This is a rewrite of the original [Swarm Simulator](https://github.com/swarmsim/swarm) in [Elm](https://elm-lang.org).

## But everyone knows [The Big Rewrite is a terrible idea](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/)!

They're not wrong - rewriting a software project is almost always a tragic mistake. Don't suggest it at work! I wouldn't.

Here are some reasons I'm choosing to rewrite Swarmsim anyway:

* www.swarmsim.com is mostly a one-man show. I'm not imposing my awful strategic decisions on anyone else.
  * [Swarm Simulator: Evolution](https://store.steampowered.com/app/914320/Swarm_Simulator_Evolution/) is created by [a different developer](https://www.scarybee.com/) - we cooperate, but our projects are separate. His progress isn't slowed by my choices.
* The original is pretty broken - it's sprouted memory leaks as browsers have updated over the years, and it simply doesn't work in Safari. Normally the right answer is "dig in and fix the original", not "rewrite", but...
  * Updating the original is painful and fraught with peril. Automated tests are lackluster, no-static-typing makes refactoring difficult and error-prone, and I keep breaking things when I try.
  * I haven't been able to find the root of the problem, and I'm kind of sick of searching.
* I've been in a slump creatively lately. I've tried [a few new game ideas](https://www.warswarms.com), but for various reasons they haven't worked out so far. A rewrite gives me something less-creative-but-still-engaging to do until I'm out of that slump.
* I'm likely to reuse the rewritten code in future projects.
* I learn a lot more experimenting with these new tools than digging around in old ones.
* Swarmsim's well-understood. It's been rewritten once already (Evolution), and I wrote the original myself - no lost knowledge from fired coworkers will interfere with a rewrite.
* Swarmsim is my hobby - I'm not on any kind of schedule or budget. I'm optimizing for my own fun and learning, not for productivity or income - under those parameters, a rewrite's a little less crazy.

## Tools

* [Git repository](https://gitlab.com/erosson/swarm-elm)
* [The game itself](https://elm.swarmsim.com)
  * [...with debugging tools publicly enabled](https://elm.swarmsim.com?debug=1)
  * [Netlify deployment and continuous integration](https://app.netlify.com/sites/swarm-elm/overview)
* [analytics](https://analytics.google.com/analytics/web/#/report-home/a53523462w195625761p190819035)

