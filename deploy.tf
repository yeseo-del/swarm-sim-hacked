# I configure the infrastructure for a static website:
# * Sourced from a Gitlab repository
# * Hosted from Netlify
# * Viewed from a Cloudflare subdomain (this one's disabled until production-time via comments)
# * Continuously built/deployed on git-push
#
# Run me with `terraform apply`. Maybe do a `terraform plan` first, too.
#
# To use me in a new project, just copy this file and change the `locals` block.

terraform {
  backend "s3" {
    bucket = "terraform-backend.erosson.org"
    key    = "swarm1-elm"
    region = "us-east-1"
  }
}

provider "cloudflare" {
  version = "~> 1.2"
}

provider "gitlab" {
  version = "~> 1.0"
}

provider "netlify" {
  version = "~> 0.1"
}

locals {
  project          = "elm"
  hostdomain       = "swarmsim.com"
  fulldomain       = "${local.project}.${local.hostdomain}"
  cloudflare_zone_id = "2526e11f0b20a0e69b0fcfb1e5a21d21"
}

resource "gitlab_project" "git" {
  name             = "swarm-elm"
  # description      = "https://${local.fulldomain}"
  description      = "https://elm.swarmsim.com"
  visibility_level = "public"
  default_branch   = "master"

  provisioner "local-exec" {
    command = <<EOF
sh -eu
git remote remove origin || true
git remote add origin ${gitlab_project.git.ssh_url_to_repo}
git push -u origin master
EOF
  }
}

module "webhost" {
  source = "git::ssh://git@gitlab.com/erosson/terraform.git//netlify/gitlab"
  name = "${gitlab_project.git.name}"
  custom_domain = "${local.fulldomain}"

  repo {
    repo_branch = "master"
    command     = "yarn build:ci"
    dir         = "build/www"
    repo_path   = "erosson/${gitlab_project.git.name}"
  }
}

resource "cloudflare_record" "www" {
  domain  = "${local.hostdomain}"
  name    = "${local.project}"
  type    = "CNAME"
  value   = "${module.webhost.dns}"
  proxied = false
  #proxied = true # netlify does its own proxying, but this is required for auth, for now
}
## Require auth until we're closer to done
#resource "cloudflare_access_application" "www" {
#  zone_id = "${local.cloudflare_zone_id}"
#  name = "swarm1-elm"
#  domain = "${local.fulldomain}"
#  session_duration = "168h"
#}
#resource "cloudflare_access_policy" "www" {
#  application_id = "${cloudflare_access_application.www.id}"
#  zone_id = "${local.cloudflare_zone_id}"
#  name = "swarm1-elm policy"
#  decision = "allow"
#  include = {
#    email_domain = ["erosson.org", "swarmsim.com"]
#    email = ["yosef.berman@gmail.com"]
#  }
#}
